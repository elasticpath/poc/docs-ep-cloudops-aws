// See https://docusaurus.io/docs/site-config for all the possible site configuration options.

const args = process.argv.slice(2);
const siteUrl = args[0] ? "/" + args[0] + "/" : '/cloudops-aws/';

const siteConfig = {
  title: 'CloudOps for AWS', // Title for your website.
  tagline: 'Documentation site for CloudOps for AWS',
  url: 'https://documentation.elasticpath.com', // Your website URL
  baseUrl: siteUrl, // Base URL for your project */
  // For github.io type URLs, you would set the url and baseUrl like:
  //   url: 'https://facebook.github.io',
  //   baseUrl: '/test-site/',

  // Used for publishing and more
  projectName: 'docs-ep-cloudops-aws',
  organizationName: 'elasticpath',
  // For top-level user or org sites, the organization is still the same.
  // e.g., for the https://JoelMarcey.github.io site, it would be set like...
  //   organizationName: 'JoelMarcey'

  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    { href: siteUrl + 'compatibility.html', label: 'Compatibility' }
  ],

  /* path to images for header/footer */
  favicon: 'img/favicon/ep-logo.png',

  /* Colors for website */
  colors: {
    primaryColor: '#0033cc',
    secondaryColor: '#ea7317',
  },

  // This copyright info is used in /core/Footer.js and blog RSS/Atom feeds.
  copyright: `Copyright © ${new Date().getFullYear()} Elastic Path, Inc.`,

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'default',
  },

  // Add custom scripts here that would be placed in <script> tags.
  scripts: [
    'https://buttons.github.io/buttons.js'
  ],

  // On page navigation for the current documentation page.
  onPageNav: 'separate',
  docsSideNavCollapsible: true,
  // No .html extensions for paths.
  // cleanUrl: true,

  // Open Graph and Twitter card images.
  ogImage: 'img/ep-logo-stacked-black.png',
  twitterImage: 'img/ep-logo-stacked-black.png',
  twitterUsername: 'elasticpath',

  // Show documentation's last update time.
  enableUpdateTime: true,

  //Enable scrolling to top button
  scrollToTop: true,
  skipNextRelease: args[1] == "--skip-next-release" ? true : false
};

module.exports = siteConfig;
