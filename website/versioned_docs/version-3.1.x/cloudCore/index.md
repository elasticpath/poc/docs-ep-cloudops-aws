---
id: version-3.1.x-index
title: CloudCore Overview
sidebar_label: CloudCore Overview
original_id: index
---

## Overview

The CloudCore component contains the base infrastructure that the other CloudOps components need to operate. All other CloudOps components, including CloudTeam and CloudDeploy, depend on CloudCore being initialized before the components can be setup and used.

CloudCore creates the following infrastructure:

- A network with public and private subnets
- AMIs used to deploy EC2 instances
- A Jenkins server
- A configuration store
- A bastion instance
- Amazon Route53 configuration
- The Amazon S3 bucket used by all CloudOps components

For more information on initializing CloudCore, see [Initializing CloudCore](initialization.md).


## Workflow

When you initialize CloudCore  by running the `runBootstrap.sh` script, the script does the following:

- Retrieves AWS (Amazon Web Services) username that is provided in the `runBootstrap.sh` script
- Gets the variables, such as region, availability zones, and other parameters that you set in the `runBootstrap.sh` script
- Gets Amazon base AMIs (Amazon Machine Image)
- Updates Amazon S3 endpoints everywhere except in the `init.sh` file
- Creates an Amazon S3 bucket
- Syncs the files from the branch in the specified repository to Amazon S3
- Creates a default EC2 key
- Creates the bastion key
- Creates network stack
- Creates bastion instances
- Adds bastion’s elastic IP and security group to EP-CC-Network’s Public and Private security groups respectively
- Creates the Route53 hosted zone for the Elastic Path CloudOps Domain
- Creates the ECS (Elastic Container Services) Optimized and Jenkins Optimized AMIs
- Creates Base AMI’s
- Creates Consul Config Store Cluster
- Creates Jenkins Server

For more information, see
[VPC (Virtual Private Cloud) with Public and Private Subnets](https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Scenario2.html)

## Related topics

- [Bastion Instance](../architecture/build-op-env.md#bastion-instance)
- [Author and Live Architecture](../architecture/auth-live-env.md)
