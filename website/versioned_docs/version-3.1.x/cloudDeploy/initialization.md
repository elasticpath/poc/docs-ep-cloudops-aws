---
id: version-3.1.x-initialization
title: Initializing CloudDeploy
sidebar_label: Initializing CloudDeploy
original_id: initialization
---

This section provides instructions to initialize the CloudDeploy component. For generic instructions on initializing CloudOps components, see [Initializing CloudOps Components](../workflow-overview.md#initializing-components).

## Prerequisites

1. Ensure that the requirements listed in the [Requirements](../requirements.md) page are met
2. Ensure that the CloudCore component is successfully initialized. For more information on initializing CloudCore, see [Initializing CloudCore](../cloudCore/initialization.md)
3. If you use CloudTeam to create deployment packages, ensure that the CloudTeam *BuildPipeline* Jenkins job is completed successfully

## Procedure

1. Clone the *CloudDeploy* repository from the Git repository hosting service as described in the [Requirements](../requirements.md) page.

2. Check out the branch for this version of CloudDeploy that you want to initialize.

3. Run the following command to copy a private SSH key authorized to clone from the Git repository hosting service to the `Containers/bootstrap/` sub-directory: `cp /path/to/${PRIVATE_GIT_KEY} CloudDeploy/Containers/bootstrap/`

4. Edit the `runBootstrap.sh` script with the required parameters. For information on each `runBootstrap.sh` parameter for CloudDeploy, see [`runBootstrap.sh` settings below](#runbootstrapsh-settings).

5. Run the `runBootstrap.sh` script. You can monitor the progress of the initialization on the terminal window and in the AWS (Amazon Web Services) CloudFormation web console.

6. Validate the CloudDeploy initialization.  For instructions, see [Validating CloudDeploy Initialization](#validating-clouddeploy-initialization) below

### Initialization Details

The bootstrap container builds the *InitPipeline* job. The *InitPipeline* job builds the following jobs:

- `BuildBaseImage`
- `BuildActiveMQ`
- `BuildEPImage`
- `BuildMySQL`
- `CreateRdsSnapshotLive`
- `CreateRdsSnapshotAuthor`

With the completion of the `InitPipeline` job, the artifacts required for deploying Elastic Path Commerce are also generated.

## Validating CloudDeploy Initialization

After running the bootstrap container to initialize CloudDeploy, validate the following to ensure that CloudDeploy is setup as required.

### AWS Security Groups

Access to resources created by CloudOps is controlled through [AWS Security Groups](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_SecurityGroups.html). This includes EP Commerce environments deployed by CloudDeploy. By default, CloudOps only grants access to these resources to the IP address from which CloudCore was initialized. For more information about the security groups, see [Security in Elastic Path CloudOps for AWS](../architecture/infrastructure.md#security).

> **Warning**: Access to the Public security group must be restricted as it grants access to the [Config Store](../architecture/build-op-env.md#configuration-store). The Config Store contains sensitive information including EC2 keys and other credentials.

#### Granting additional access to EP Commerce environments

1. Identify the external IP address of the users who will need access to the EP Commerce environments
2. Update the **Public** security group as needed


### AWS CloudFormation Stacks

Ensure that the following CloudFormation stacks have completed successfully:

- `EP-CD-RDS-Subnet-Group`: Creates a subnet group for RDS (Relational Database Service) instances
- `EP-CD-ECS-Image-Repositories`: Creates the Elastic Container Registries (ECRs) required for deployment
- `EP-CD-Lambda-Function`: Creates the Lambda Functions required for Author and Live deployment

> **Note:** You may have additional CloudFormation stacks named like `EP-RDS-Snapshot-*`. These stacks are created as a result of a Jenkins initialization job `InitPipeline`. These stacks are displayed after CloudDeploy is initialized and are deleted after finishing the job.

### AWS S3 Bucket

Ensure that the `ep-cloudops-<account_id>` Amazon S3 bucket exists and contains:

- `CloudDeploy` folder with the following structure:

```text
CloudDeploy/
|
|---<branch>/
    |
    |---CloudFormation/
    |   |
    |   |---Base-AMI/
    |   |
    |   |---ECS-Repositories/
    |   |
    |   |---EP-Infrastructure/
    |   |   |
    |   |   |---Author-And-Live-Environment/
    |   |   |
    |   |   |---Shared-Templates/
    |   |   |
    |   |   |---Single-Instance-Environment/
    |   |
    |   |---Lambda-Functions/
    |   |
    |   |---RDS
    |       |
    |       |---ParameterGroups
    |---Config
    |   |
    |   |---cloudops-env-files
    |   |
    |   |---ep-sample-datapopulation
    |       |
    |       |---authoring
    |       |
    |       |---live
    |
    |
    |---Scripts/
        |
        |---UpdateEnvironment/
        |
        |---Lambda-Functions/
|
|---&lt;branch&gt;
    |
    |...
```

- A `Dependencies` folder with the following structure:

```text
Dependencies/
|
|---jdbc/
```

### Elastic Container Registries (ECRs)

Confirm the following ECRs are created:

- `cm`
- `centos7jre8`
- `batch`
- `search`
- `cortex`
- `activemq`
- `integration`
- `tomcat`
- `mysql-ep-data`

### Jenkins Jobs

Confirm that the following Jenkins jobs are added to the *CloudOps* view in the master Jenkins server:

- Build view
    - `BuildActiveMQ`
    - `BuildBaseImage`
    - `BuildEPImage`
    - `BuildMySQL`
    - `CleanDockerImages`
    - `CreateEcsAmi`
    - `CreateRdsSnapshotAuthor`
    - `CreateRdsSnapshotLive`
- Deployments view
    - `DeployAuthorAndLive`
    - `DeploySingleInstanceDevEnvironment`
    - `UpdateEnvironment_1_StageUpdates`
    - `UpdateEnvironment_2_ApplyUpdates`
- Other view
    - `InitPipeline`

## Reference Materials

### `runBootstrap.sh` Settings

You must provide the following details to initialize the CloudDeploy component:

| Field | Description |
|---|---|
| `epCloudDeployGitRepoUrl` | The SSH URL to the CloudDeploy repository. |
| `epDockerGitRepoUrl` | The SSH URL to the *CloudOps* Docker repository. |
| `epCloudDeployBranch` | The branch and release version to use for CloudDeploy initialization. This setting is optional. If left blank, the system sets the value to master. |
|`epDockerBranch`|The branch and version to use to build Docker images needed to deploy Elastic Path Commerce. This setting is optional. If left blank, the system sets the value to master.|
|`gitSSHKeyFileName`| The name of the private SSH key that is authorized to clone from both `epCloudDeployGitRepoUrl` and `epDockerGitRepoUrl`. The key must be located in the same directory as CloudDeploy’s bootstrap Dockerfile, `Containers/bootstrap/`, and must not be password protected. |
|`gitUsername`|This setting is optional if you don’t use AWS CodeCommit. If you use AWS CodeCommit as a git service for all CloudOps repositories, use the 'SSH Key ID’ as mentioned in the [AWS documentation page](https://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html).|
|`awsAccessKeyId`|The access key ID for the AWS account used to initialize CloudCore. You must use the same AWS account that you used to initialize CloudCore.|
|`awsSecretAccessKey`|The secret access key for the AWS account used to initialize CloudCore. You must use the same AWS account that you used to initialize CloudCore.|
|`awsRegion`|The region in which CloudCore is initialized. You must not change this setting to a different region. The default setting is `us-west-2`.|
|`epConfigStoreUrl`|The complete URL of the config store created in CloudCore, including `http://` or `https://` |
|`epDeploymentPackageUrl`|The Amazon S3 URI of the deployment package to use when you build initial Elastic Path Docker images. The format for the URL is `s3://<bucket-name>/<path-to-deployment-pkg>`.|
