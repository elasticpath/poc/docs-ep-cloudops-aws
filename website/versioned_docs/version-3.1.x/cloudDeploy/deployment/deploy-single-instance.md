---
id: version-3.1.x-deploy-single-instance
title: Deploying a Single Instance Development Environment
sidebar_label: Deploy Single Instance
original_id: deploy-single-instance
---

## Overview

You can deploy a single instance development environment when the *InitPipeline* job and its sub-jobs are successfully completed. The *InitPipeline* job pushes Docker images required for deployment into the Elastic Container Registry (ECR) in AWS (Amazon Web Services). The *InitPipeline* is run as part of the CloudDeploy initialization. For more information on the CloudDeploy initialization, see the [CloudDeploy Initialization](../initialization.md) page.

The following diagram shows the structure of a single instance development environment:

![Elastic Path Commerce related containers on one instance exposed through a load-balancer.](assets/version-3.1.x/ep4aws-test-architecture.png)

For more information on the single instance environment architecture, see [Single Instance Development Environment Architecture](../../architecture/single-instance-env.md).

## Procedure

The *DeploySingleInstanceDevEnvironment* job deploys a development instance in a private subnet created by CloudCore. This job is located in the Jenkins job folder *CloudOps*, in the Jenkins view *Deployments*.

1. Confirm that the *InitPipeline* job is completed successfully and all required Docker images are in ECR.

1. Run the `DeploySingleInstanceDevEnvironment` job. The default values for the parameters are stored in the Consul server that is created by CloudCore. For more information on the parameters, see [`DeploySingleInstanceDevEnvironment` job](index.md#deploysingleinstancedevenvironment).

1. Monitor the deployment progress in the CloudFormation AWS console.
The environment is created in approximately 10 minutes. When the CloudFormation stack completes successfully the [stack outputs](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-console-view-stack-data-resources.html) displays the output that includes information, such as the load-balancer endpoint to the Elastic Path services in the single instance environment. The following is an example output from the AWS CLI:

```bash
$ aws cloudformation describe-stacks --stack-name EP-Single-Dev --query 'Stacks[0].Outputs'
[
    {
        "OutputKey": "ElbEndPoint",
        "OutputValue": "dev1.ep-dev.example.epcloudops.com"
    },
    {
        "OutputKey": "ASG",
        "OutputValue": "EP-Single-Instance-Dev-Env-EP-CloudDeploy-master-1-AutoScalingCluster-1B6FYD77F0Y4F-AutoScalingGroup-1LBJ1FCWEAOSU"
    },
    {
        "OutputKey": "Cluster",
        "OutputValue": "EP-Single-Instance-Dev-Env-EP-CloudDeploy-master-1-AutoScalingCluster-1B6FYD77F0Y4F-Cluster-GBQ62PY6ELW7"
    }
]
```

> **Note:** The ELB (Elastic Load Balancer) endpoint works only if your DNS settings are set up correctly. For more information about DNS settings, see [CloudCore DNS Configuration](../cloudCore/configurations.md#dns-configuration). If DNS is not configured, you must reference the load-balancer’s unique DNS name to access the Elastic Path services on the single instance environment.


## Validating Single Instance Deployment

This section outlines how to verify a successful deployment and
connection.

![Example of a successfully built single instance CloudFormation stack](assets/version-3.1.x/complete-si-stack.png)

These checks are done after running the
[`DeploySingleInstanceDevEnvironment` Jenkins job](index.md#deploysingleinstancedevenvironment) and a single instance developer environment is created.

Credentials used to log in are located on the following page:

- [Default Credentials](../../references.md#default-credentials)

### Target Group health check

Check the health of each **Target Group** as follows:

1. Open the **AWS EC2 Console** and click **Target Groups**.

2. Confirm the presence of 5 target groups prefixed by `EP-Si-Targe`.

3. As shown in the following screenshot, select the **Health checks** tab and check the **Path** to find the app the target group points to.
    - ![Target Group App](assets/version-3.1.x/target-group-app.png)
    - Each target group should point to one of the 5 apps:
        - `cm`
        - `integration`
        - `cortex`
        - `batch`
        - `search`

4. Go to the **Monitoring** tab (on the same panel as the **Health checks** tab) and ensure that the number of instances on the **Healthy Hosts** graph is correct

### ActiveMQ transaction check

The following steps are to verify that *Elastic Path Commerce* is able to process transactions.

#### Connect to the ActiveMQ admin tool

1. Set up a SSH tunnel through the Bastion server to point your web browser at the ActiveMQ admin tool with the following terminal command:

```bash
ssh -i <path-to-bastion-private-key>/<bastion-private-key-name> \
ec2-user@<bastion-public-ip> \
-L <activemq-port>:<si-private-ip>:<activemq-port>
```

1. Log in from an internet browser to: `localhost:<activemq-port>/admin`\
1. Click the **queues** tab, and note the values in the columns **Messages Enqueued** and **Messages Dequeued** from the row queue named *Consumer.consumerRegistrationEmailHandler.VirtualTopic.ep.customers*

![ActiveMQ email queue](assets/version-3.1.x/activemq-email-columns.png)

#### Register a new user in Cortex

1. In *Cortex Studio* [Authenticate](https://developers.elasticpath.com/commerce/7.1/Cortex-API-Front-End-Development/Getting-Started/learningToUseTheAPITool#Authenticating) yourself as a public user
2. In the *Account Registration* section, enter the following information as shown in the following screenshot, then click **registeraction**:
    - **family-name**
    - **given-name**
    - **password**
    - **username**
    - ![Register a new user in Cortex Studio](assets/version-3.1.x/cortex-register-new-user.png)
3. In the ActiveMQ admin tool (refer to *Figure 3. ActiveMQ email queue*), in the columns **Messages Enqueued** and **Messages Dequeued** in the row queue named *Consumer.consumerRegistrationEmailHandler.VirtualTopic.ep.customers*,confirm that values have been incremented

### Search check

#### Searching for promotions and shipping items

1. Log in to the *Commerce Manager* as the administrator.

1. In the *Promotions/Shipping* tab, click **Search**. Confirm that the search result contains search result items

![Searching for promotions and shipping items](assets/version-3.1.x/cm-search-promotions.png)


#### Reindexing search for an index

1. Log in to the *Commerce Manager* as the administrator.

2. In the **Configuration** tab, click **Search Indexes**.

3. Click **Rebuild Index** for any index name. Confirm that the status goes from **Rebuild Scheduled** to **Rebuild in Progress** to **Complete**

![Rebuilding a search index from Commerce Manager](assets/version-3.1.x/cm-rebuild-search-index.png)
