---
id: version-3.1.x-deploy-auth-live
title: Deploying Authoring and Live Environments
sidebar_label: Deploy Authoring & Live
original_id: deploy-auth-live
---

## Overview

You can deploy an Author and Live environment when the *InitPipeline* job and its sub-jobs are successfully completed. The *InitPipeline* job pushes the Docker images required for deployment into the AWS (Amazon Web Services) Elastic Container Registry (ECR). It also creates AWS Relational Database Service (RDS) snapshots that contain pre-populated data required for an Author and Live environment. The *InitPipeline* job is run as part of [CloudDeploy initialization](../initialization.md).

The following diagram shows the architecture of an Author and Live environment with a focus on autoscaling and load-balancing:

![Autoscaling and load-balancing view of and Author and Live environment.](assets/version-3.1.x/authlive-loadbalancing-architecture.png)

For more information, see
[Author and Live Architecture Overview](../../architecture/auth-live-env.md).

## Procedure

The *DeployAuthorAndLive* job deploys a complete Elastic Path Commerce environment that contains an Authoring environment and a Live environment. This job is located in the Jenkins job folder *CloudOps*, in the Jenkins view *Deployments*.

1. Ensure that the `InitPipeline` job completed successfully.
    - All required Docker images must exist in ECR and at least two RDS snapshots must exist in RDS. By default, the Jenkins jobs deploy the Author and Live environment using the Docker images and data created from the deployment package provided during the CloudDeploy initialization
    - If you want to deploy with a different deployment package, you must rebuild the Docker images using the `BuildEpImage` job and re-create RDS snapshots using `CreateRdsSnapshotAuthor` and `CreateRdsSnapshotLive` jobs

2. Ensure that the configuration for the deployment is correct.
   - For more information, see [Elastic Path Commerce Configuration](commerce-configurations.md#elastic-path-commerce-configuration)

3. Run the `DeployAuthorAndLive` job.
    - The default parameters for this job are stored in the Consul server that is created by *CloudCore*. For more information about the parameters, see [*DeployAuthorAndLive* Jenkins job](index.md#deployauthorandlive)

4. Monitor the deployment progress in the CloudFormation AWS console.
    - The complete environment is created in approximately 40 minutes
    - When the root CloudFormation stack completes successfully, the [stack outputs](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-console-view-stack-data-resources.html) displays the output that includes information, such as the load-balancer endpoint to the Elastic Path services in the Author and Live environment
    - The following is an example output from the AWS CLI:

```bash
$ aws cloudformation describe-stacks --stack-name EP-Author-And-Live-Environment-1 --query 'Stacks[0].Outputs'
[
    {
        "OutputKey": "LiveCortexElbEndPoint",
        "OutputValue": "cortex.ep-preprod.test4.epcloudops.com"
    },
    {
        "OutputKey": "LiveIntegrationElbEndPoint",
        "OutputValue": "integration.ep-preprod.test4.epcloudops.com"
    },
...

]
```

> **Note:** The ELB (Elastic Load Balancer) endpoints only work if your DNS settings are set up correctly. For more information, see [CloudCore DNS Configuration](../../cloudCore/configurations.md#dns-configuration). If the DNS settings are not configured, you must reference the load-balancer’s unique DNS name to access the Elastic Path services.

1. Validate the deployment. For instructions, see [Validating an Author and Live Deployment](#validating-author-and-live-deployment) section below


## Validating Author and Live Deployment

After the root CloudFormation stack is completed successfully, you can perform the following validation steps to ensure that all apps are up and connected successfully:

![Successfully built Author and Live CloudFormation stack](assets/version-3.1.x/auth-live-success.png)

### Ensure all load balancer target groups are healthy

1. In the EC2 AWS Console, go to the **Target Group** section
2. For each Target Group, check the monitoring tab and ensure that the number of instances on the Healthy Hosts graph is correct

![Healthy ELBs](assets/version-3.1.x/healthy-target-groups.png)

*Figure 2. Healthy Target Groups.*

### Verify Cortex can send a message to ActiveMQ

#### Register a new user in Cortex

1. Go to Cortex Studio
2. [Authenticate](https://developers.elasticpath.com/commerce/7.1/Cortex-API-Front-End-Development/Getting-Started/learningToUseTheAPITool#Authenticating) yourself as a public user
3. Navigate to the *Account Registration* section
4. Enter the required information and use valid email format
5. Click *registeraction*

![Register a new user in Cortex Studio](assets/version-3.1.x/cortex-register-new-user.png)

*Figure 3. Registering a new user in Cortex Studio.*

#### Ensure a new message is added to an ActiveMQ queue

> Only one of two Live ActiveMQ nodes is active node. **You might have to try both IP addresses if the first one doesn’t work.**

1. Go to `http://<active live activemq ip address>:8161/admin/`
2. Log in with the credentials
3. Click the **queues** tab
4. Confirm the queue *Consumer.customerRegistrationEmailHandler.VirtualTopic.ep.customers* has a pending message

![ActiveMQ email queue](assets/version-3.1.x/activemq-email-queue.png)

*Figure 4. ActiveMQ email queue.*

### Ensure Commerce Manager uses search

#### Searching for promotions and shipping items

1. Go to `http://<live cm ip address>:8080/cm`
2. Login as the administrator
3. Navigate to the *Promotions/Shipping* tab
4. Click *Search*. Ensure that the search result contains search result items

![Searching with CM](assets/version-3.1.x/cm-search-promotions.png)

*Figure 5. Searching for promotions and shipping items.*

#### Reindexing search for an index

1. Go to `http://<live cm ip address>:8080/cm`
2. Login as the admin user
3. Navigate to the *Configuration* tab
4. Click *Search Indexes*
5. Click *Rebuild Index* for any index name
6. Confirm the status goes from *Rebuild Scheduled* to *Rebuild in Progress* to *Complete*

![Rebuild search index from CM](assets/version-3.1.x/cm-rebuild-search-index.png)

*Figure 6. Rebuilding a search index from Commerce Manager.*
