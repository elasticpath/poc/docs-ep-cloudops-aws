---
id: version-3.1.x-index
title: Introduction to CloudOps for AWS
sidebar_label: Introduction
original_id: index
---

Elastic Path CloudOps for AWS creates and deploys Elastic Path Commerce environments on AWS (Amazon Web Services) using the Docker images and AWS CloudFormation templates. Other AWS services are also used to optimize stability, reliability, performance, and operations in order to minimize Elastic Path Commerce implementation and operating costs.

## Components

Elastic Path CloudOps for AWS is modularized to three components to make the configuration and deployment flexible. The components are:

- **CloudCore**: Creates and configures the network infrastructure, build server, configuration management server, and storage
- **CloudTeam**: Creates deployment packages from the Elastic Path Commerce source code repository
- **CloudDeploy**: Creates Amazon RDS (Relational Database Services) Aurora snapshots and Docker images from the deployment package, and deploys them into an environment

Each component in the Elastic Path CloudOps for AWS is initialized only once. By default, these components set up all required servers and databases to deploy and run Elastic Path Commerce.

### CloudCore

CloudCore is the foundation of Elastic Path CloudOps for AWS. CloudCore sets up the network configuration, Jenkins instances, and other configuration settings. These settings are static and you need to configure these settings only once. With CloudCore, you need not change configurations for deployment. CloudCore ensures network security by accessing API HTTP(S) through an ELB (Elastic Load Balancer). For all SSH type access, CloudOps uses a *bastion* server.

### CloudTeam

CloudTeam provides additional capability to create a deployment package, which CloudDeploy utilizes, from the source code. CloudTeam component takes the Elastic Path Commerce source code, adds the source code distribution to the repository that you specify, automatically creates Continuous Integration (CI) jobs for the existing CI pipeline, and uses the existing Jenkins jobs to build the deployment package.

### CloudDeploy

CloudDeploy deploys the deployment package prepared by the CloudTeam component. This component runs the Jenkins jobs to build the necessary Docker images for deployment. With the Jenkins jobs, you can also redeploy new versions of the Elastic Path container without manual deployment.

CloudDeploy builds RDS Aurora snapshots and Docker images from the deployment package prepared by the CloudTeam component. It also deploys and updates environments using these snapshots and images.


## Features

You can initialize Elastic Path CloudOps for AWS from an empty AWS environment. With *Infrastructure-as-Code*, *Continuous Integration (CI)*, *Continuous Deployment (CD)*, and the *Jenkins management layer*, customers can get an Elastic Path Commerce production-like environment up and running.

- Fully automated deployments with ease
- Fault tolerant services, such as Amazon RDS Aurora for the data-tier and Elastic Load Balancers (ELB) for load balancing, provide redundancy and lower operating costs
- Auto-scaling capability. This capability ensures scaling up and down the service to meet the commerce need
- Continuous integration and delivery practices streamline development, testing and delivery to improve code accuracy and eliminate daily build bottlenecks
- The Jenkins CI and CD management layer helps to build Docker images quickly and orchestrate deployments and re-deployments with no downtime, easily and consistently using AWS ECS


## AWS Services used by CloudOps

Elastic Path CloudOps for AWS uses the following AWS services.

### CloudFormation

This is used for repeatable infrastructure management. It creates and manages a collection of related AWS resources. CloudFormation also provisions and updates the resources in a specific order.

### ECS

Elastic Container Services - Docker Container management service. It runs applications on a managed cluster of Amazon EC2 instances.

### ECR

Elastic Container Registry - Docker container registry. This stores and provides access to Docker images built and used by CloudOps.

### EC2

Elastic Cloud Computing - Virtual machines provides computing power in the cloud.

### Amazon S3

Stores files used by CloudOps infrastructure.

### CloudWatch

Stores logs from Elastic Path Commerce applications.

### VPC

Virtual Private Cloud - Controls virtual networking environment.

### RDS

Relational Database Service - Manages Aurora database clusters.

### ELB

Elastic Load Balancing - Automatically distributes incoming application traffic across multiple Amazon EC2 instance and provides fault tolerance and load balancing to route traffic for efficiency.

### Route53

Connects user requests to infrastructure running in AWS.

### ACM

Amazon Certificate Manager - Manages SSL certificates used with CloudOps infrastructure.

### AWS Lambda

Serverless compute service - Runs code in response to events.


## Limitations

Elastic Path CloudOps for AWS does not provide a production environment. However, an environment that can be transformed into a production environment is provided.

You can only deploy one Author-Live environment for one CloudOps instance.

You must create different AWS accounts to deploy CloudOps in different regions. For a specific account, you can deploy CloudOps only in the region that is selected for that account.
