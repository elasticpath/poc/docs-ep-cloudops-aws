---
id: version-3.1.x-references
title: Reference Materials of CloudOps for AWS
sidebar_label: Reference Materials
original_id: references
---

## Protocol

If the CloudOps components were initialized using a certificate ARN (Amazon Resource Name), prefix all hostnames with the protocol `https://`, if it was left blank use `http://`.

## Default Credentials

### Single-Instance Environment

| Applications | hostname | port | path | username | password |
|---|---|---|---|---|---|
| Cortex Studio | \<ElbEndPoint> | n/a | /studio | n/a | n/a |
| Commerce Manager | \<ElbEndPoint> | n/a | /cm/admin | admin | 111111 |
| ActiveMQ admin tool | \<si private ip> | 8161 | /admin | admin | admin |

## Finding the hostname parameters

### ELB (Elastic Load Balancer) endpoints

If the hostname is an ELB endpoint:

1. Go to the CloudFormation service console
2. Select the root stack of the desired deployment
3. Click the **Outputs** tab

![ELB endpoint of single instance](assets/version-3.1.x/si-elb-endpoint.png)

### IP Addresses

To determine the IP address of an instance refer to the AWS (Amazon Web Services) documentation [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-instance-addressing.html#using-instance-addressing-common).

