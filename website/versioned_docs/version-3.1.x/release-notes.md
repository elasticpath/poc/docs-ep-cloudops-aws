---
id: version-3.1.x-release-notes
title: Release Notes
original_id: release-notes
---

## `3.1.1`

### Change log

* `CLOUD-1060` - Fixed database connection loss that occurred due to 8 hours of idle time
* `CLOUD-1032` - Fixed the Integration server to correctly set Data Sync Tool configuration

## `3.1.0`

### Release Highlights

* Switched RDS (Relational Database Service) database engine to Aurora MySQL for Author and Live environments
* Added support for multiple Author and Live environments on a single AWS (Amazon Web Services) account
* Upgraded to Tomcat 9 for compatibility with _Elastic Path Commerce_ 7.4

### Change log

#### CloudCore

* Added version sets files to CloudCore for configuring default versions of third party tools

#### CloudDeploy

* Aurora MySQL 5.6 is now the RDS database engine for Author and Live environments
* Added support for multiple Author and Live environment deployments to a single AWS account
* Added Docker image namespace option for all _Elastic Path Commerce_ environments

#### Docker

* Removed cache configuration from the sample environments for _Elastic Path Commerce_ 7.4 compatibility
* Added support for Tomcat 9 and Solr 7.2.1 for _Elastic Path Commerce_ 7.4 compatibility

### Bug Fixes

* `CLOUD-681` - Improvements to logging to prevent recording sensitive parameters
* `CLOUD-623` - Removed unused public IPs from Jenkins worker nodes and Nexus instance
* `CLOUD-703` - Maven image now builds correctly when there are slashes in an image tag
* `CLOUD-747` - Removed unused database credentials from CloudFormation templates
* `CLOUD-748` - Reduced verbosity from deployment script outputs
* `CLOUD-788` - Updated the list of EC2 instances types that work with CloudOps
* `CLOUD-869` - Increase memory settings for building deployment packages to address intermittent out of memory issue
