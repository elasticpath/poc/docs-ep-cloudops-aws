---
id: version-3.3.x-glossary
title: Glossary
sidebar_label: Glossary
original_id: glossary
---

## A

### ActiveMQ

Open source JMS (Java Message Service) used by Elastic Path Commerce deployments.

### Amazon EC2

Elastic Compute Cloud, web service that provides secure and resizable compute capacity in the cloud.

### Amazon ECR

Elastic Container Registry, manages repositories for Docker images.

### Amazon ECS

Elastic Container Service, manages Docker containers in AWS.

### Amazon EFS

Elastic File System, used by CloudOps to provide a shared file storage for ActiveMQ clusters.

### Amazon ELB

Elastic Load Balancing, an AWS service to load balance application servers across multiple instances and multiple availability zones. It also works with AWS VPC to provide robust networking and security features. In conjunction with AS provides autoscaling on compute capability.

### Amazon S3

Amazon Simple Storage Service, provides storage capabilities in AWS.

### Amazon RDS

Relational Database Service, sets up, operates, and scales a relational database in the cloud.

### Amazon Route53

Amazon Route53, scalable Domain Name System (DNS).

### Amazon VPC

AWS Virtual Private Cloud, used to configure a private network and resources within the AWS infrastructure.

### AMI

Amazon Machine Images, provides information required to launch a pre-configured virtual server in the cloud.

### AS

Auto Scaling, automatically scales EC2 compute instances up or down to adjust for load.

### Author Environment

Elastic Path Commerce environment used to manage and preview products, promotions, and pricing, and to publish changes to a live Elastic Path Commerce environment.

### AWS

Amazon Web Services.

### AWS Lambda

A serverless computing platform that runs code in response to events.

## B

### Bastion

Special purpose server that is designed to be the primary access point from the Internet and acts as a proxy to other EC2 instances.

### Batch Server

Server that uses scheduled jobs to perform certain operations, such as calculating top selling products or product recommendations.

### Bootstrap Container

A Docker container that runs scripts and sends commands to AWS to initialize a CloudOps component.

## C

### Cleanup Container

A Docker container that runs scripts and sends commands to AWS to cleanup the infrastructure of a CloudOps component.

### CloudCore

The CloudOps component that sets up the network configuration, Jenkins instances, and configurations.

### CloudTeam

The CloudOps component that provides the capability to create a deployment package from Elastic Path Commerce source code.

### CloudDeploy

The CloudOps component that provides the capabilities to build artifacts required for Elastic Path Commerce deployment from a deployment package. It can deploy two styles of Elastic Path Commerce.

### CloudOps

Elastic Path CloudOps for AWS.

### CloudOps Component

Component that Provides a specific capability of CloudOps. The components are:

- CloudCore
- CloudTeam
- CloudDeploy

### Commerce Manager

Web application for managing Elastic Path Commerce settings and commerce capabilities.

### Config Store

A store of configuration values. CloudOps uses a Consul service to provide a config store.

### Container

Docker container. An isolated environment running the pre-built executables.

### Cortex

REST API for e-commerce that uses hypermedia links to associate related resources together rather than relying on resource URLs.

## D

### Deploy

Used as a term to inform users to run all required Elastic Path Commerce services in a specific infrastructure configuration.

### Deployment Package

A build artifact created from Elastic Path Commerce source code. The deployment package is required to build Elastic Path Docker images, and other resources needed to deploy Elastic Path Commerce.

### DNS

Domain name system, naming system for computers, services, or other resources connected to the internet or a private network.

### Docker

A container platform that builds, runs, and manages containers.

## E

### Elastic Path Source Code Distribution

Release Package, downloaded from the customer portal, that contains the Elastic Path source code.

## I

### Initialize

Term that indicates an operation that creates all of the infrastructure and artifacts of a CloudOps component using a bootstrap Docker container.

### Integration Server

A server that allows an Elastic Path Commerce deployment to integrate with external back-end systems.

## N

### Nexus

Maven repository to clone Elastic Path Commerce build dependencies.

## O

### Office IP

The public IP address of the VPN that is used to access Elastic Path Commerce services.

## R

### Route53

Amazon Route53, Domain Name System (DNS) from AWS.

## S

### Search Server

A server that provides searching capabilities to Elastic Path Commerce.

### Single Instance Environment

Small-scale test environment without separate authoring capabilities deployed to a single server.

### SSH

Secure Shell, a Unix-based command interface and protocol for securely getting access to remote computers.

### SSL

Secure Sockets Layer, the type of certificate used to secure network communications.

### SMTP

Simple Mail Transfer Protocol interface

## V

### VPN

Virtual Private Network, extends a private network across a public network, and enables users to send and receive data across shared or public networks.

