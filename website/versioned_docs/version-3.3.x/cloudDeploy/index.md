---
id: version-3.3.x-index
title: CloudDeploy Overview
sidebar_label: CloudDeploy Overview
original_id: index
---

With CloudDeploy, you can deploy Elastic Path Commerce onto AWS (Amazon Web Services) infrastructure. CloudDeploy also builds the Docker images for all services needed to deploy Elastic Path Commerce. CloudDeploy uses the network, Jenkins server, AMIs, and Amazon S3 bucket created by CloudCore. The only infrastructure CloudDeploy creates are:

- The ECR (Elastic Container Registry) repositories to store and manage the Docker images that it builds
- The infrastructure required for an Elastic Path Commerce deployment
- The Lambda functions that take the RDS (Relational Database Service) endpoints and the ActiveMQ endpoints from an Author and Live deployment and put them into the config store

For more information on initializing CloudDeploy, see [Initializing CloudDeploy](initialization.md).

## Deployment Process Overview

The following diagram shows an overview of the Elastic Path Commerce deployment process workflow:

![Simple deployment process](assets/version-3.3.x/ep4aws-simple-deploy.png)

The following diagram shows the complete workflow for deploying a test environment or production environment:

![Deployment model](assets/version-3.3.x/ep4aws-build-deploy.png)

### Generic Deployment Procedure

1. If the base Docker image is changed, run the *BuildBaseImage* Jenkins job.

2. Source a deployment package by building it using CloudTeam or using the [`extensions/packager`](https://developers.elasticpath.com/commerce/latest/Starting-Construction/Starting-Construction-Guide/Team-Infrastructure-Setup/Starting-Construction---Setup-CI-Server) module.

3. Run the *BuildEPImage* Jenkins job to build the Elastic Path Docker images using the deployment package.

4. To deploy a single instance developer environment and if the ActiveMQ image is changed, run the *BuildActiveMQ* Jenkins job.

5. Run the Jenkins job to deploy the required environment:
    - For a single instance environment: *DeploySingleInstanceDevEnvironment*
    - For an Author and Live environment: *DeployAuthorAndLive*

For more information on deploying a single instance development environment, see [Deploying a Single Instance Development Environment](deployment/deploy-single-instance.md).

For more information on deploying an Author and Live environment, see [Deploying an Author and Live Environment](deployment/deploy-auth-live.md).

## Data Population

With the Data Population tool, you can initialize or update existing databases with new data. In CloudOps, Jenkins jobs run the Data Population tool during Elastic Path Commerce deployment. For more information, see [Data Population Config](deployment/commerce-configurations.md#data-population-configuration).

## The Lambda Functions

The Lambda functions created as a part of the CloudDeploy deployment procedure are:

- `RDSEndpointFunction`
- `ActiveMqEndpointFunction`

The `RDSEndpointFunction` takes the RDS endpoints of the databases created by the `DeployAuthorAndLive` job and puts those values into the config store. The `ActiveMqEndpointFunction` takes the ActiveMQ endpoints created by the `DeployAuthorAndLive` job and puts those values into the config store. These functions are triggered after the RDS endpoints and
ActiveMQ endpoints are created during the the Author and Live deployment.

For more information on the Author and Live Deployment, see [Deploying Elastic Path Commerce with Jenkins Jobs](deployment/index.md#elastic-path-commerce-deployment).

## Related Topics

- [Deploying a Single Instance Development Environment](deployment/deploy-single-instance.md)
- [Deploying an Author and Live Environment](deployment/deploy-auth-live.md)
- [Building & Deploying Elastic Path Commerce](deployment/index.md)
