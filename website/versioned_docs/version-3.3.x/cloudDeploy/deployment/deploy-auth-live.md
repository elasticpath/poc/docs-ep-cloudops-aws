---
id: version-3.3.x-deploy-auth-live
title: Deploying Authoring and Live Environments
sidebar_label: Deploy Authoring & Live
original_id: deploy-auth-live
---

## Overview

After the Jenkins pipeline `InitPipeline` and its related sub-jobs complete successfully, the Author and Live environment is ready for deployment in an AWS (Amazon Web Services) account. The pipeline `InitPipeline` pushes Docker images required for deployment into an ECR (Elastic Container Registry) repository. The pipeline `InitPipeline` is run as part of [CloudDeploy initialization](../initialization.md).

The following diagram shows the architecture of an Author and Live environment with a focus on autoscaling and load-balancing:

![Autoscaling and load-balancing view of and Author and Live environment.](assets/version-3.3.x/authlive-loadbalancing-architecture.svg)

For more information, see
[Author and Live Architecture Overview](../../architecture/auth-live-env.md).

## Procedure

The Jenkins job `DeployAuthorAndLive` job deploys a complete Elastic Path Commerce environment that contains an Authoring environment and a Live environment. This job is located in the Jenkins job folder *CloudDeploy*, in the Jenkins view *Deployments*.

1. Ensure that the Jenkins pipeline `InitPipeline` completed successfully.

    All required Docker images must exist in ECR. By default, the Jenkins jobs deploy the Author and Live environment using the Docker images and data created from the deployment package provided during the CloudDeploy initialization. To deploy a different deployment package, rebuild the Docker images using the Jenkins job `BuildEpImage`.

2. Ensure that the configuration for the deployment is correct. For more information, see:
    - [Elastic Path Commerce Configuration](commerce-configurations.md#elastic-path-commerce-configuration)
    - [CloudOps Default Configuration Sets](../configurations.md#cloudops-configuration-sets)

3. Run the `DeployAuthorAndLive` job. The job performs the following steps before deploying the environment:
    - The default values for the job parameters are populated from the [Consul config store](../../architecture/build-op-env.md#configuration-store)
    - CloudFormation templates used in an Author and Live environment are uploaded into the Amazon S3 bucket `ep-cloudops-<account_id>/CloudDeploy/<Branch>/<StackName>`

    For more information about the parameters, see [`DeployAuthorAndLive`](index.md#deployauthorandlive).

4. Monitor the deployment progress in the CloudFormation AWS console.

    The complete environment is created in approximately 1 hour. When the root CloudFormation stack completes successfully, the [stack’s outputs](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-console-view-stack-data-resources.html) display useful information, such as the load-balancer endpoints to the Elastic Path services.

     > **Note:** The endpoints will only work after DNS settings are updated. This is usually done after initializing CloudCore. For more information, see [DNS Configuration](../../cloudCore/configurations.md#dns-configuration).

5. Validate the deployment. For instructions, see [Validating an Author and Live Deployment](#validating-author-and-live-deployment) section below


## Validating Author and Live Deployment

After the root CloudFormation stack is completed successfully, you can perform the following validation steps to ensure that all apps are up and connected successfully:

![Successfully built Author and Live CloudFormation stack](assets/version-3.3.x/auth-live-success.png)

### Reference Default CloudOps Endpoints and Credentials page

Use the [Default CloudOps Endpoints and Credentials](../../references.md#default-credentials) page for information on how to access all the services listed below.

Most services can be accessed easily using endpoints, however the ActiveMQ admin console also requires port-forwarding through the bastion server.

### Ensure all load balancer target groups are healthy

1. In the EC2 AWS Console, go to the **Target Group** section
2. For each Target Group, check the monitoring tab and ensure that the number of instances on the Healthy Hosts graph is correct

> **Note:** Target groups created by an Author and Live environment have their names prefixed by `EP-Au-`.

![Healthy ELB Target Groups](assets/version-3.3.x/healthy-target-groups.png)

*Figure 2. Healthy Target Groups.*

### Verify Live Cortex can send a message to ActiveMQ

#### Register a new user in Cortex

1. Go to the Live deployment’s Cortex Studio
2. [Authenticate](https://developers.elasticpath.com/commerce/7.1/Cortex-API-Front-End-Development/Getting-Started/learningToUseTheAPITool#Authenticating) yourself as a public user
3. Navigate to the *Account Registration* section
4. Enter the required information and use valid email format
5. Click *registeraction*

![Register a new user in Cortex Studio](assets/version-3.3.x/cortex-register-new-user.png)

*Figure 3. Registering a new user in Cortex Studio.*

#### Ensure a new message is added to an ActiveMQ queue

1. Access the Live ActiveMQ admin console. This requires bastion server credentials
2. Click the **Queues** tab
3. Confirm the queue *Consumer.customerRegistrationEmailHandler.VirtualTopic.ep.customers* has a pending message

![ActiveMQ email queue](assets/version-3.3.x/activemq-email-queue.png)

*Figure 4. ActiveMQ email queue.*

### Ensure Commerce Manager uses search

#### Searching for promotions and shipping items

1. Go to Commerce Manager
2. Login as the admin user
3. Navigate to the *Promotions/Shipping* tab
4. Click *Search*. Ensure that the search result contains search result items

![Searching with CM](assets/version-3.3.x/cm-search-promotions.png)

*Figure 5. Searching for promotions and shipping items.*

#### Reindexing search for an index

1. Go to Commerce Manager
2. Login as the admin user
3. Navigate to the *Configuration* tab
4. Click *Search Indexes*
5. Click *Rebuild Index* for any index name
6. Confirm the status goes from *Rebuild Scheduled* to *Rebuild in Progress* to *Complete*

![Rebuild search index from CM](assets/version-3.3.x/cm-rebuild-search-index.png)

*Figure 6. Rebuilding a search index from Commerce Manager.*
