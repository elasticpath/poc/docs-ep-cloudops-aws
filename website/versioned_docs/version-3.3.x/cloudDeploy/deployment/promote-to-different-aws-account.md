---
id: version-3.3.x-promote-to-different-aws-account
title: Promoting Docker Images to Different AWS Accounts
sidebar_label: Promoting Docker Images
original_id: promote-to-different-aws-account
---

This procedure describes how to promote Docker images to other AWS (Amazon Web Services) accounts, such as staging and production. Do not build Docker images using staging or production AWS accounts.

See the [best practices guides](../../best-practices.md) when dealing with multiple AWS accounts. Follow these steps to promote Docker images built in one source AWS account to another destination AWS account.

1. Build Docker images in the **source** AWS account.

2. Run the Jenkins job `AuthorizeAwsAccountToPullEcrImages` in the **source** AWS account.

    Set the Jenkins job parameter `AWS_ACCOUNT_ID` to the AWS account ID of the **destination** AWS account.

    This will authorize the destination AWS account to pull Docker images from the source AWS account’s ECR (Elastic Contariner Registry).

3. In the **destination** AWS account, run the Jenkins job `PullDockerImages`. Set the following Jenkins job parameters:
    - Set `SOURCE_IMAGE_TAG` to the Docker image tag to pull from the source account
    - Set `DEST_IMAGE_TAG` to the value the Docker images should be tagged in the destination account
    - Set `EP_IMAGE_REPOS` to a space-separated list of EP ECR repositories from which to promote images
    - Set `SOURCE_AWS_ACCOUNT_NUMBER` to the AWS account ID of the source account

    **Optionally** set `SOURCE_AWS_REGION` to the AWS region of the source account if the ECR repositories of the source account are in a different region from the destination account

    > **Note:** In order to pull images from all of the repositories specified by `EP_IMAGE_REPOS`, each repository must have an image tagged with value in `SOURCE_IMAGE_TAG`

The ECR repositories in the **destination** AWS account will now have the images from the **source** account. For more information on viewing ECR repositories, see [AWS ECR document](https://docs.aws.amazon.com/AmazonECR/latest/userguide/repository-info.html).
