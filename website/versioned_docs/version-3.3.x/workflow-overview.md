---
id: version-3.3.x-workflow-overview
title: CloudOps Setup and Elastic Path Commerce Deployment Workflow
sidebar_label: Workflow Overview
original_id: workflow-overview
---

## High Level Workflow

This section provides a high-level procedure to understand the various steps involved in deploying Elastic Path Commerce using Elastic Path CloudOps for AWS (Amazon Web Service):


1. Prepare the team environment for CloudOps. For more information, see the [Starting Construction Guide](https://developers.elasticpath.com/commerce/construction-home).

1. Ensure that all requirements that are described in the
[Requirements](requirements.md) section are met.

1. Initialize CloudCore. For more information, see [Initializing CloudCore](cloudCore/initialization.md).

    1. Ensure that the [CloudCore component is initialized properly](cloudCore/initialization.md#validating-initialization).

    1. Update the DNS name servers. For more information, see [DNS Configuration](cloudCore/configurations.md#dns)

1. Initialize CloudTeam. See [Initializing CloudTeam](cloudTeam/initialization.md) for more details.

    1. Ensure that the [CloudTeam component is initialized properly](cloudTeam/initialization.md#validating-initialization)

1. Initialize CloudDeploy. For more information, see [Initializing CloudDeploy](cloudDeploy/initialization.md).

    1. Ensure that the [CloudDeploy component is initialized properly](cloudDeploy/initialization.md#validating-initialization)

1. Run the Jenkins jobs for the environment that you are deploying.
    - For single instance, see [DeploySingleInstanceDevEnvironment](cloudDeploy/deployment/index.md#deploysingleinstancedevenvironment)
    - For Author and Live environment, see [DeployAuthorAndLive](cloudDeploy/deployment/index.md#deployauthorandlive)

### Initializing Components

The components of Elastic Path CloudOps for AWS, such as CloudCore, CloudTeam, and CloudDeploy, are initialized with a bootstrap Docker container that is built locally on the computer that is used to deploy the CloudOps component.

 The bootstrap container are run and built using the `runBootstrap.sh` script located in the *Containers* directory in the repository of the CloudOps component. The `runBootstrap.sh` script builds a bootstrap Docker container and runs the component’s initialization script, `init.sh`, inside the Docker container. Running the initialization script in a bootstrap container provides a consistent workspace and reduces the dependencies for initializing CloudOps components to only Docker and Bash.

#### Procedure

The following is a generic procedure to initialize a component:

1. Clone the component’s repository from the Git repository hosting service.

2. Check out the correct branch and release version of the CloudOps component.

3. Copy a private SSH key authorized to clone from your Git repository hosting service to the `Containers/bootstrap/` directory. The private SSH key must not be password protected.

4. In the cloned repository, navigate to the `Containers/` directory.

5. Edit the `runBootstrap.sh` script with the required parameters. The comments in the script provides details about each parameter.

6. Run the `runBootstrap.sh` script

#### Additional Instructions

For more information on the initialization requirements for each component, see the following initialization documentation for each component:

- [Initializing CloudCore](cloudCore/initialization.md)
- [Initializing CloudTeam](cloudTeam/initialization.md)
- [Initializing CloudDeploy](cloudDeploy/initialization.md)

### Cleaning Up the Components

You can run a set of interactive scripts to clean up the AWS resources created by the `init.sh` and `runBootstrap.sh` scripts.

> **DISCLAIMER**: Elastic Path recommends deleting resources manually to avoid any mistakes, especially if non-Elastic Path resources exist in the AWS account, because the script empties your account.

The `Containers/cleanUp/` directory in the repository for each component consists of the cleanup script for that component. The naming convention for the script is `runCleanOutCloudX.sh`, where `X` is the CloudOps component that the script cleans up. For example, the cleanup script for CloudCore is `runCleanOutCloudCore.sh`. The cleanup script builds a Docker container and runs a cleanup script in it.

#### Procedure

> **Note**: You must not delete the CloudCore component before deleting all other CloudOps components. You must clean up the CloudTeam and CloudDeploy components in any order before cleaning up CloudCore.

Do the following to clean up a component:

1. Clone the component’s repository from your Git repository hosting service.

2. Check out the correct branch and release version of the CloudOps component.

3. In the repository, navigate to the `Containers/cleanUp/` directory.

4. Edit the script `runCleanOutCloudX.sh` with the required parameters.
The comments in the script provides details about each parameter.

5. Run the `runCleanOutCloudX.sh` script. The script prompts you to confirm before cleaning up each resource

## Upgrading CloudOps

You must upgrade *Elastic Path CloudOps for AWS* when you upgrade *Elastic Path Commerce* to a version that is no longer compatible with your current version of CloudOps. For the latest compatibility matrix for CloudOps and Elastic Path Commerce, see the [Compatibility Matrix](../compatibility.html).

You can upgrade *Elastic Path CloudOps for AWS* by initializing the new version of CloudOps in a new AWS account and by deploying the new version of Elastic Path Commerce in the same account. When the new deployment of Elastic Path Commerce is ready, the old Elastic Path Commerce service is placed under maintenance to complete synchronization tasks, if any. After completion of the synchronization tasks, the new version of Elastic Path Commerce is enabled and the older version is disabled.
