---
id: version-3.3.x-release-notes
title: Release Notes
original_id: release-notes
---

## `3.3.3`

### Change log

#### CloudCore

* Upgraded Jenkins to the `2.176.1` long-term support release

#### CloudDeploy

* Added support to extended architecture (XA) transactions

## `3.3.2`

### Change log

#### CloudCore

* Upgraded Jenkins to the `2.164.2` long-term support release

#### CloudDeploy

* Renamed the Data Sync entrypoint script

## `3.3.1`

### Change log

#### CloudTeam

* Renamed Jenkins view to _CloudTeam_ from _Development_

#### CloudDeploy

* Set API Gateway throttling limits to a steady-state request rate of 10,000 requests per second and a burst limit of 5,000 requests
* Added Data Sync Tool web app to the Author environment
* Renamed Jenkins view to _CloudDeploy_ from _CloudOps_

### Bug Fixes

* `CLOUD-1226` - Resolved issue where ECS agent logs are written to incorrect log groups
* `CLOUD-1280` - Updated Tomcat context.xml to only exist in one location inside the Docker container
* `CLOUD-1282` - Adds support for deploying Author and Live environments with both RDS encryption and a read-replica
    * Fixes Known Issue from CloudOps for AWS 3.3.0
* `CLOUD-1303` - Resolved issue of how to set the Elastic Path Commerce version in the BuildDeploymentPackage job to work with Elastic Path Commerce 7.5.0
* `CLOUD-1316` - Fixed downloading ActiveMQ binaries while building the ActiveMQ Docker image
* `CLOUD-1327` - Added documentation about how password protected Git SSH keys are not supported by the CloudOps components
* `CLOUD-1326` - Fixed CloudCore bootstrap failure when `epCloudCoreBranch` parameter is unspecified


## `3.3.0`

### Release Highlights

* Replaced support for Oracle JDK (Java Development Kit) and JRE (Java Runtime Environment) with Zulu JDK
* Added support for provisioning and configuring Elastic Path Commerce with Amazon SES (Simple Email Service)
* Added SSH credentials for each repository (CloudCore, CloudTeam, CloudDeploy, Docker, and Elastic Path Commerce)
* New automated workflow for promoting Docker images between AWS (Amazon Web Services) accounts
* Improved Amazon Aurora configuration

### Change log

#### CloudCore

* Upgraded Tomcat and ActiveMQ versions used with Elastic Path Commerce 7.5
* Removed Docker from the Bastion server

#### CloudDeploy

* Upload CloudFormation templates to S3 bucket at deployment time
* Support deployment using custom CloudOps env files
* Provision ActiveMQ before data population is run
* Added ECS (Elastic Container Service) agent logs to CloudWatch
* Renamed the ECR (Elastic Container Registry) repository for the MySQL Docker image from `mysql-ep-data` to `mysql`

#### CloudDocs

* Documented process for connecting Java monitoring tools to Elastic Path applications using JMX ports
* Documented long-term implications of Git SSH keys used during CloudOps for AWS initialization
* Updated documentation on accessing Elastic Path applications deployed with CloudOps for AWS

### Bug Fixes

* `CLOUD-1176` - Increased Docker container memory limit from 4GB to 4.5GB to facilitate Java Heap space performance and stability improvements
* `PERF-89` - Improved autoscaling by modifying task scaling alarms to focus on ECS Cortex Service CPU Utilization of greater than 70%
* `CLOUD-1174` - Improved EC2 tagging

### Known Issues

* `CLOUD-1017` - No support for the Elastic Path Commerce 7.5 Data Sync Tool web application
* `CLOUD-1282` - Cannot deploy Author and Live with both RDS (Relational Database Service) encryption and a read-replica for the Live database
