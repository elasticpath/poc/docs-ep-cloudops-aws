---
id: version-3.3.x-generate-deployment-package
title: Generate Deployment Package Using CloudTeam
sidebar_label: Generate Deployment Package
original_id: generate-deployment-package
---

## Generating Deployment Package

If you update Elastic Path Commerce source code, and want to deploy a new environment or update an existing environment, you must generate a deployment package first.

## Building a Maven Docker Image

CloudTeam uses a Maven Docker container to build deployment packages in a clean, reproducible environment.

Before building the first deployment package, a Maven Docker image must have been built by running the Jenkins job `BuildMavenImage`. The job `BuildMavenImage` should have already been run as part of CloudTeam initialization through the Jenkins pipeline `BuildPipeline`.

### Creating a Deployment Package

After the Maven Docker image is built, run the Jenkins job `BuildDeploymentPackage`. This job builds a deployment package and uploads it to Amazon S3. From there, other jobs can use it to build Docker images.

The `BuildDeploymentPackage` job updates the Consul setting for `team/ep/deployment-package/uri` in Consul. The downstream jobs that use this parameter use the last-built deployment package by default.

> **Note:** When creating a CI pipeline, this might affect concurrent builds. To avoid concurrency issues, explicitly define which deployment package to use when building Docker images.

### Next Steps

For information on how to deploy a new environment using a deployment package, see [CloudDeploy](../cloudDeploy/index.md). To learn about updating an existing Author and Live environment using a deployment package, see [Updating an Author and Live Environment](../cloudDeploy/deployment/deploy-auth-live.md#updating-author-and-live-environment).

## Jenkins Jobs

The CloudTeam bootstrap container runs the Jenkins jobs to set up a Docker container with Maven and to generate an Elastic Path Commerce deployment package after completing CloudTeam initialization. The container runs the following jobs:

- `BuildPipeline`
- `BuildMavenImage`
- `BuildDeploymentPackage`

Before running the job, the container also:

- Uploads the Jenkins jobs to the Jenkins instance
- Sets the default parameters for these jobs in the config store
- Creates a Nexus instance
- Creates an ECR (Elastic Container Registry) repository for the Maven containers


### `BuildMavenImage` Job

This job creates a Maven Docker image. The `BuildDeploymentPackage` uses this image as an environment to run the `Build-Deployment-Package.sh` script. The required parameters for this job are:

| Parameter | Description |
|---|---|
|`CONFIG_STORE_URL` | The complete URL of the Config Store created in CloudCore, including the `http(s)://` prefix. |
|`EP_MVN_IMAGE_TAG` | The tag name to use on the image when pushed into their repository. The default tag name is, `EP-CloudTeam-<VERSION>`. |
|`EP_ORACLE_JDK_DOWNLOAD_URL` | A licensed Oracle JDK (Java Development Kit) download URL.|
|`EP_ORACLE_JDK_FOLDER_NAME`| The name of the folder where Oracle JDK is downloaded. |
|`EP_MAVEN_FOLDER_NAME`| The name of the folder where Maven binary is downloaded. |
|`CLOUDTEAM_GIT_REPO_URL`| The URL of the CloudTeam Git repository. |
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |

### `BuildDeploymentPackage` Job

The `BuildDeploymentPackage` job builds a deployment package using the source code from the Elastic Path Commerce repository, and then uploads it to Amazon S3. Use this deployment package to build the Docker images that are used to deploy Elastic Path Commerce. This job requires the following parameters:

| Parameter | Description |
|---|---|
|`CONFIG_STORE_URL` | The complete URL of the Config Store created in CloudCore, including the `http(s)://` prefix. |
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |
|`EP_MVN_IMAGE_TAG` | The tag name of the Maven image to use when creating the Maven container. |
|`EP_COMMERCE-GIT_REPO_URL`| The URL of the EP-Commerce repository where the source code distribution is available. |
|`EP_COMMERCE_GIT_REPO_BRANCH`| The branch in the EP-Commerce repository that you want to use.|
|`NEXUS_REPO_BASE_URI`| The URL of the Nexus repository created by the initialization of CloudTeam, including the `http(s)://` prefix. |
|`NEXUS_REPO_USER_NAME`| The user name for the Nexus repository created by the initialization of CloudTeam. By default, CloudOps uses the default Nexus credentials. |
|`NEXUS_REPO_PASSWORD`| The  password for the Nexus repository created by the initialization of CloudTeam. By default, CloudOps uses the default Nexus credentials. |
|`CLOUDTEAM_GIT_REPO_URL`| The URL of the CloudTeam Git repository. |
|`DEPLOYMENT_PACKAGE_NAME`| The name of the deployment package that is uploaded to Amazon S3, including .zip. |

### `BuildPipeline` Job

`BuildPipeline` job runs the `BuildMavenImage` and the `BuildDeploymentPackage` jobs.
