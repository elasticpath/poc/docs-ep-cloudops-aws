---
id: version-3.3.x-configurations
title: CloudCore Configurations
sidebar_label: CloudCore Configurations
original_id: configurations
---

## DNS Configuration

### Overview

*Elastic Path CloudOps for AWS (Amazon Web Services)* programmatically generates a *Route53* public hosted zone that by default uses a DNS delegated subdomain of `aws.epcloudops.com`.This domain is configurable and you can use it with any domain. On completion of the runBootstrap script, a set of Route53 nameservers are displayed on the screen. You can retrieve these nameservers from AWS through the hosted zone ID available in the outputs of the *EP-CC-Route53* CloudFormation stack. You can also set these nameservers as delegation records in the authoritative nameserver for the chosen domain.  For more information, see the following diagram:

![Route53 DNS - Default Route53 DNS Configuration through Sub-domain](assets/version-3.3.x/route53dnsdiagram.png)

You need not use a subdomain. You can specify any domain that client uses. If the client uses `example.com`, the nameservers are set at the registrar level instead of the DNS server for the root domain. For more information, see the following diagram:

![Route53 DNS - Parent Domain Route53 DNS Configuration](assets/version-3.3.x/route53dnsdiagram-2edusecase.png)

### Amazon Route53 Hosted Zones

*Elastis Path CloudOps for AWS* uses Route53 for publicly accessible services and for internal DNS resolvers. Public DNS response requires a manual update of the name server records with the relevant registrar. However, CloudOps also uses an Amazon Route53 Private Hosted Zone for instant internal DNS response inside VPC (Virtual Private Cloud). With the Amazon Route53 Private Hosted Zone, you can use DNS names to reach the various components of CloudOps from the moment the components are created from queries originating from inside of the VPC. CloudOps services that create DNS names register themselves into both the private and public hosted zones.

### DNS Names Created by CloudOps

Elastic Path’s internal services are accessible only through Application Load Balancers that is also known as version two of AWS’s Elastic Load Balancer. CloudOps uses the AWS DNS record type of `ALIAS` to associate a DNS name to specific ELBs. When a resource is not behind an ELB then a suitable DNS record type is used. The individual environment CloudFormation scripts are responsible for the creation of their DNS name. These names are used as subdomains of the chosen domain specified in the runBootstrap script.

This following are the current DNS names for the three default environments:

| Environment | Sub-domain | Server | DNS Name (excluding domain) |
| ----------- | ---------- | ------ | --------- |
| Config Store |  | Consul | `config` |
| Build & Operation | `ep-build` | Jenkins | `jenkins.ep-build` |
| |  | Nexus | `nexus.ep-build` |
| Development | `ep-dev`   | Single-Instance Dev Server | `dev1.ep-dev`\* |
| Pre-Production    | `ep-preprod` | Author | `author.ep-preprod` |
|             |              | Commerce Manager (Live) | `cm.ep-preprod` |
|             |              | Cortex (Live) | `cortex.ep-preprod` |
|             |              | Integration (Live) | `integration.ep-preprod` |
|             |              | Search Master (Live) | `search-master.ep-preprod` |
|             |              | ActiveMq Server (Live) | `liveactivemq.ep-preprod` |
|             |              | ActiveMq Server (Author) | `authoractivemq.ep-preprod` |

- *The ActiveMq Load Balancers are only routable inside VPC*

- *You can run multiple single-instance dev servers simultaneously,  such as dev1, dev2, and so on.*

> **Note**: You can configure DNS in the runBootstrap script, Jenkins jobs, or their respective CloudFormation scripts.

### More Details

#### Route53

Route53 is a non-region specific hosted DNS service that allows API-driven DNS management for both public authoritative and internal private zones. The functional difference between a public and private hosted zone is only when the DNS zone resolves outside of a specified VPC.

#### Private Hosted Zones

Elastic Path recommends to avoid using private hosted zones without a public zone due to the inflexible accessibility of private hosted zones. You must attach a private hosted zone to a VPC and the VPC does not respond outside of it. If the VPC is configured correctly to work with Route53, the attached private zone acts as the primary DNS resolver for all requests inside of the VPC. You must enable the `enableDnsHostnames` and `enableDnsSupport` VPC settings. Due to this, VPC acts as the authoritative source for any DNS record added to it, and that allows using internal DNS domains, such as `company.local`, or overriding a public DNS domain for internal use. However, resources outside VPC cannot resolve addresses in this domain.

#### Public Hosted Zones

A public hosted zone is not associated with a VPC and is used as the authoritative server for a publicly routable domain. For example, `epcloudops.com` is supported by a Route53 public hosted zone. Records in a public hosted zone are not required to be accessible to the public as the resources that are pointed to by records in the `epcloudops.com` hosted zone are in a restrictive security group. If you own the specific domain, this provides the same functionality as a private hosted zone while enabling the ease of use provided by globally resolvable DNS names.

#### DNS Delegation

DNS delegation is the process delegating the load of a subsection of one authoritative zone to another DNS service. For example, if someone who has the authoritative zone for `epcloudops.com` wants someone else to manage the subdomain `aws`, the authorized person must create an NS record for `aws.epcloudops.com` that points to the nameservers of that person. This NS record informs the DNS client where records for the specified subdomain can be found. This chain of delegation is a core feature of how DNS works and is available on all nameservers.

##### Example DNS delegation record

```text
Name: aws.epcloudops.com.
Type: NS
Value: ns-774.awsdns-32.net.
ns-1571.awsdns-04.co.uk.
ns-1118.awsdns-11.org.
ns-266.awsdns-33.com.
TTL: 300
```

> **Note:** The TTL might need to be adjusted downward if the delegated zone is hosted on a service that has random nameservers per hosted zone and the zone creation is driven by a CI type process causing it to be recreated frequently. A value of 300 (5 minutes) rather than the DNS standard 172800 (2 days) might be more appropriate for this use case.
>
> **Note:** If you use Route53 for the delegated zone and frequently recreate it, set the TTL to 300.


## HTTPS Configuration

### Overview

*Elastic Path CloudOps for AWS* uses Amazon Certificate Manager (ACM) to enable secure communication for all load-balancers that are created. You need not create certificates through ACM to provide HTTPS for CloudOps. If a certificate is obtained from another provider, you can upload it to ACM and then use it identically.  Certificates provided from ACM are free of cost and only require an email validation. The certificates are automatically renewed and are recommended as the simplest approach to enable secure communication for CloudOps.

### HTTPS Implementation

After uploading a certificate to ACM, set ARN (Amazon Resource Name) of the certificate in the `epCertificateArn` parameter in the the `runBootstrap.sh` script. If this parameter is empty, *Elastic Path CloudOps for AWS* enters the HTTP fallback mode.
