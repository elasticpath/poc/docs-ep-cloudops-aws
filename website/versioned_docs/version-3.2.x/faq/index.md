---
id: version-3.2.x-index
title: Frequently Asked Questions Using CloudOps for AWS
sidebar_label: FAQ On CloudOps for AWS
original_id: index
---

## CloudOps Initialization

### Failure In Initialization of CloudOps Component

*Elastic Path CloudOps for AWS* is implemented to run the initialization scripts only once per account. If you run into any issues, Elastic Path recommends manually deleting all AWS (Amazon Web Services) resources created by the initialization and then rerunning the initialization. This minimizes the risk of deleting important resources.

See the initialization validation documentation for the CloudOps component you want to cleanup to find which AWS resources need to be deleted:

- [Validating CloudCore Initialization](../cloudCore/initialization.md#validating-initialization)
- [Validating CloudTeam Initialization](../cloudTeam/initialization.md#validating-initialization)
- [Validating CloudDeploy Initialization](../cloudDeploy/initialization.md#validating-initialization)

You can also delete everything on your AWS account automatically using a cleanup script.

> **Disclaimer**: Elastic Path recommends deleting resources manually to avoid any mistakes, especially if non-Elastic Path resources exists in the AWS account. Use the scripts at your own risk with the assumption it may delete non-Elastic Path resources.

For generic instructions on how to cleanup CloudOps components using the cleanup scripts, see [Cleaning Up the Components](../workflow-overview.md#cleaning-up-the-components).


### How do I set up the correct public or private keys to use the bootstrap Docker containers and initialize the CloudOps components?

When initializing CloudOps components, you must have a copy of a private key that is authorized to clone from your CloudOps Git repositories.

## Configuration

### Where do I configure variables specific to my environments?

In AWS (Amazon Web Services) both single instance development environments, and Author and Live environments have their own `.env` configuration files that are stored with the CloudFormation templates. For example, the default single instance development environment configuration file is `single-instance-mysql-container.env`.

This is a simple key-value pair file that is sourced within each container, for that environment, creating environment variables that are selected by the various components, such as Tomcat’s `setenv.sh` script.

### Where are Elastic Path Assets?

Assets are also mounted on the EC2 host as follows:

```json
{
    "Name": "ep_assets",
    "Host": {
        "SourcePath": "/ep/assets"
    }
}
```

## Credentials

This section discusses where the default usernames and passwords are set.


### What is the CloudTeam Nexus Server root username and password?

The default username and password is discussed [Default Credentials](../references.md#default-credentials) page.

### What is the RDS instance root username and password?

The root username and password are set during the [`DeployAuthorAndLive` Jenkins Job](../cloudDeploy/deployment/index.md#deployauthorandlive)

Refer to [How do I reset the master user password for my Amazon RDS DB instance?](https://aws.amazon.com/premiumsupport/knowledge-center/reset-master-user-password-rds/) for instructions on how to reset the root user password for RDS (Relational Database Service) instances.

## Jenkins Jobs

### How to see the parameter values a Jenkins job is run with?

For each Jenkins job, the parameter fields that are left blank are populated with default values defined in the config store. The config store URL in the parameter description provides the default value for the parameter as in the following figure:

![BuildEPImage job with the config store highlighted.](assets/cloudops-jenkins-jobs-consul-url.png)

When you troubleshoot a build and want to check the set values for the parameters, the run-time value cannot be viewed from the *Parameters* tab as these values are customized for Jenkins jobs. To view the details, check the *Environment Variables* tab, as shown the following image:

![BuildEPImage job build #1, Environment Variables tab view.](assets/cloudops-jenkins-jobs-env-vars.png)

### The BuildEpImage job failed with a device mapper issue

Occasionally, a `docker build` command fails due to an underlying `device_mapper` storage driver issue.  The options to resolve this issue are:

1. Re-run the job
2. Change the storage driver. You can uncomment the overlay2 config in the `Create-AMI.sh` script, or use the config as an example to enable it

## Logs

### How do I view Elastic Path Commerce application logs?

Elastic Path applications are configured to send logs to `stdout` for [12 Factor compliance](https://12factor.net/logs). All CloudOps deployments of Elastic Path Commerce forward logs to [AWS CloudWatch](https://aws.amazon.com/cloudwatch/).

In the CloudWatch dashboard, select **Logs** in the left pane, to see the CloudFormation stack and the logs. Using this log, you can set up any metrics or alerts.

### How do I view MySQL Docker container data population logs?

The console output for the Jenkins job `BuildMySQL` contains data population logs.

### How do I view data population logs?

Logs from data population from an data population tool are forwarded to CloudWatch. To find the correct CloudWatch Log Group, use the **Resources** tab from the CloudFormation stack prefixed with one of **EP-Author-And-Live-Environment-*-DataPopTask**

### How do I view CloudFormation initialization logs for any CloudOps EC2 instance?

All CloudOps CloudFormation initialization logs are forwarded to CloudWatch.

To find the correct CloudWatch Log Group, use the **Resources** tab from the CloudFormation stack that created the EC2 instance. The CloudWatch Log Stream will be named something like:

- `{ec2-instance-id}/cfn-init.log`
- `{ec2-instance-id}/cfn-init-cmd.log`

### How do I view logs that are not in CloudWatch?

Access the EC2 instance that has the logs you need [using the bastion server](../architecture/build-op-env.md#bastion-instance).

