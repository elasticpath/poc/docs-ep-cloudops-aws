---
id: version-3.2.x-references
title: Reference Materials of CloudOps for AWS
sidebar_label: Reference Materials
original_id: references
---

## Protocol

If the CloudOps components were initialized using a certificate ARN (Amazon Resource Name), prefix all hostnames with the protocol `https://`, if it was left blank use `http://`.

## Default Credentials

### Nexus Server

| Hostname | Path | Full URI Example |
|----------|------|------------------|
| Nexus load-balancer DNS name | `/nexus` | `https://nexus.ep-build.cloudops.example.com/nexus` |

#### Default Nexus Users

| Role | Username | Password | Privileges |
|---|---|---|---|
| administration | `admin` | `admin123` | all privileges |
| deployment | `deployment` | `deployment123` | read and deploy to repositories |
| anonymous | n/a | n/a | read-only |


### Single-Instance Environment

| Application | Hostname | Path | Full URI Example | Username | Password |
|-------------|----------|------|------------------|:--------:|:--------:|
| Cortex Studio | Single-instance API gateway DNS name | `/studio/` | `https://dev1-cortex-gw.ep-dev.cloudops.example.com/studio/` | - | - |
| Cortex | Single-instance API gateway DNS name | `/cortex` | `https://dev1-cortex-gw.ep-dev.cloudops.example.com/cortex` | - | - |
| Commerce Manager | Single-instance application load-balancer DNS name | `/cm/admin` | `https://dev1.ep-dev.cloudops.example.com/cm/admin` | `admin` | `111111` |
| ActiveMQ admin tool | ActiveMQ network load-balancer DNS name | `/admin` | `https://dev1-activemq.ep-dev.cloudops.example.com/admin` | `admin` | `admin` |

> **Note**: The trailing slash in Studio’s endpoint is required when using an API gateway

## Finding Hostname Values

### AWS ELB (Elastic Load Balancer) and API endpoints

If the hostname is either a load-balancer or API gateway endpoint:

1. Go to the CloudFormation service console
2. Select the root CloudFormation stack of the desired deployment
3. Click the **Outputs** tab
4. Find the CloudFormation output key for the endpoint

![API gateway endpoint from a single-instance deployment.](assets/version-3.2.x/si-cf-endpoint-outputs.png)

### IP Addresses

To determine the IP address of an instance refer to the AWS (Amazon Web Services) documentation [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-instance-addressing.html#using-instance-addressing-common).


## Tags On EC2 Instances

EC2 instances deployed by CloudOps usually contain values for the following tags:

### `Name`

Basic reference name of the EC2 instance. Value shows up under the **Name** column in the AWS EC2 Console:

- `Cloud-Core-Bastion`
- `EP-Development-Server`

### `AccountRoleTag`

The role of the AWS account in which EC2 instances are deployed. CloudOps users often have separate AWS accounts, each separately initialized with CloudOps.

For example, CloudOps users use different AWS accounts to deploy EP Commerce for QA, EP Commerce for staging production and EP Commerce for production:

- `dev`
- `qa`
- `staging`
- `prod`

### `InstanceRole`

The specific role of the EC2 instance:

- `author`
- `live-cortex`
- `base-ami`
- `data-population`

### `CloudOpsEnvironment`

The CloudOps environment the EC2 instance is a part of. Can be part of either a single-instance, authoring or live environment:

- `single-instance`
- `author`
- `live`

### `CommerceEnvironment`

The EP Commerce environment folder with which the CloudOps environment was deployed. Must be updated manually if the `EP_COMMERCE_ENVNAME` env var passed to the docker containers is changed in the config store.

- `sample-author`
- `sample-live`
- `dev-1`
- `qa`

For more information on how EP Commerce environment folders are chosen, see the [Elastic Path Commerce Configuration](cloudDeploy/deployment/commerce-configurations.md#elastic-path-commerce-configuration) page.
