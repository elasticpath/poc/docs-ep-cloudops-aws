---
id: version-3.2.x-initialization
title: Initializing CloudTeam
sidebar_label: Initializing CloudTeam
original_id: initialization
---

This section provides instructions to initialize the CloudTeam component. For generic instructions on initializing CloudOps components, see [Initializing CloudOps Components](../workflow-overview.md#initializing-components).

## Prerequisites

1. Ensure that the requirements listed in the [Requirements](../requirements.md) section are met
2. Ensure that the CloudCore component is successfully initialized. For more information on initializing CloudCore, see [Initializing CloudCore](../cloudCore/initialization.md)

## Procedure

1. Clone the *CloudTeam* repository from the Git repository hosting service as described in the [Requirements](../requirements.md) section.

1. Check out the correct branch for the version of CloudTeam that you want to initialize.

1. Run the following command to copy a private SSH key authorized to clone from the Git repository hosting service to the `Containers/bootstrap/` sub-directory: `cp /path/to/${PRIVATE_GIT_KEY} path/to/cloud-team-aws/Containers/bootstrap/`

1. Edit the `runBootstrap.sh` script with the required parameters. For information about the parameters, refer to comments in the script. For additional information on each `runBootstrap.sh` parameter for CloudTeam, [see `runBootstrap.sh` settings below](#runbootstrapsh-settings).

1. Run the `runBootstrap.sh` script. You can monitor the initialization progress on the terminal window and on the AWS (Amazon Web Services) CloudFormation web console.

1. Validate the CloudTeam initialization. For instructions, see [Validating CloudTeam Initialization](#validating-initialization)

### Initialization Details

When you initialize the CloudTeam component, it performs the following tasks:

1. Uploads required files to Amazon S3
2. Creates an ECR (Elastic Container Registry) repository for the Maven build container
3. Creates a Nexus instance that hosts a Maven mirror of the Elastic Path public repository
4. Uploads the jobs into the Jenkins instance that is created by CloudCore
5. Starts running a CloudTeam pipeline that creates the deployment package

## Validating Initialization

After running the bootstrap container to initialize CloudTeam, validate the following to ensure that CloudTeam is setup as required.

### AWS Security Groups

Access to resources created by CloudOps is controlled through [AWS Security Groups](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_SecurityGroups.html). This includes the Nexus server. By default, CloudOps only grants access to these resources to the IP address from which CloudCore was initialized. For more information about the security groups, see [Security in Elastic Path CloudOps for AWS](../architecture/infrastructure.md#security).

> **Warning**: Access to the Public security group must be restricted as it grants access to the [Config Store](../architecture/build-op-env.md#configuration-store). The Config Store contains sensitive information including EC2 keys and other credentials.

#### Granting additional access to the Nexus server

1. Identify the external IP address of the users who will need access to the Nexus server
2. Update the **Public** security group as needed

### AWS CloudFormation Stacks

Ensure that the following CloudFormation stacks are completed successfully:

- `EP-CT-ECR`: Creates the Elastic Container Registries (ECRs) required for CloudTeam
- `EP-CT-Nexus`: Creates the Maven Repository Manager

### AWS S3 Bucket

Ensure that the `ep-cloudops-<account_id>` Amazon S3 bucket exists and contains a `CloudTeam` folder with the following structure:

```text
CloudTeam/
|
|---<branch>/
    |
    |---Containers/
    |   |
    |   |---bootstrap/
    |   |
    |   |---cleanUp/
    |   |
    |   |---maven/
    |
    |---Files/
    |
    |---Scripts/
```

### Elastic Container Registries (ECRs)

Confirm an ECR repository named `maven` is created.

### Jenkins Jobs

Confirm that the following Jenkins jobs are added to the *Development* view in the master Jenkins server:

- BuildDeploymentPackage
- BuildMavenImage
- BuildPipeline

## Reference Materials

### `runBootstrap.sh` Settings

You must provide the following details to initialize CloudTeam and generate a deployment package from Elastic Path Commerce source code distribution:

|  Field| Description  |
|---|---|
|`epCloudTeamGitRepoUrl`| The SSH URL of the CloudTeam repository.|
|`epCommerceGitRepoUrl`|The SSH URL of the Elastic Path Commerce repository.|
|`epCloudTeamBranch`| The CloudTeam branch and release version to use for CloudTeam initialization. This setting is optional. The default setting for this parameter is master. |
|`epCommerceBranch`| The Elastic Path Commerce branch and release version to build first deployment package with. This setting is optional. The default setting for this parameter is master. |
| `gitSSHKeyFileName` | The name of the private SSH key authorized to clone from the Git repository hosting server. Ensure that the private key is in the same folder as the bootstrap Dockerfile, `Constainers/bootstrap/`, and that the key is not password protected. |
| `gitUsername` | This setting is optional if you don’t use AWS CodeCommit. If you use AWS CodeCommit as a git service for all CloudOps repositories, use the 'SSH Key ID’ as mentioned in [this AWS documentation](https://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html).|
|`awsAccessKeyId`  |The access key ID for the same AWS account used to initialize CloudCore. |
|`awsSecretAccessKey`| The secret access key for the same AWS account used to initialize CloudCore. |
| `awsRegion`| The AWS region in which you initialized CloudCore. |
|`epConfigStoreUrl`|The complete URL of the config store created by CloudCore, including `http://` or `https://` prefix. |
|`epNexusUserName`| The username that CloudTeam can use to authenticate to the Elastic Path public Maven repository. **Note:** Your Elastic Path [Support Portal Account page](https://community.elasticpath.com/resource-center/) lists your Maven credentials. If you do not have these credentials, submit a ticket on the page. |
|`epNexusPassword`|The password that CloudTeam can use to authenticate to the Elastic Path public Maven repository. **Note:** Your Elastic Path [Support Portal Account page](https://community.elasticpath.com/resource-center/) lists your Maven credentials. If you do not have these credentials, submit a ticket on the page. |
|`oracleJdkDownloadUrl`| URL to download a licensed Oracle JDK (Java Development Kit). The file must be a `tar.gz` file. For more information on which version of Java is compatible with a specific version of Elastic Path Commerce, see [Elastic Path documentation](https://developers.elasticpath.com/commerce/7.2/Core-Commerce-Development/Setting-up-your-Developer-Environment/Installing-Build-Tools/1.-Java-JDK). |
|`jdkFolderName`| The name of the folder in the `tar.gz` file that contains the JDK.|
