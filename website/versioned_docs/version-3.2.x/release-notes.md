---
id: version-3.2.x-release-notes
title: Release Notes
original_id: release-notes
---

## `3.2.2`

### Change log

#### Bug Fixes

* `CLOUD-1184` - Added explicit depends on statement to avoid race conditions in the API Gateway
* `CLOUD-1179` - Only removing data population CloudFormation stack when deploying without RDS (Relational Database Service) snapshots. Fixes Known Issue from CloudOps for AWS (Amazon Web Services) 3.2.1
* `CLOUD-1150` - Added support for CORS (Cross Origin Resource Sharing) headers with Cortex and Studio
* `CLOUD-1192` - Increased access restrictions to prevent outside access to the Cortex OSGi console

## `3.2.1`

### Change log

#### CloudDeploy

* Replaced Autoscaling group in the single instance environment with a single EC2 instance

#### Docker

* Removed unused JDBC parameters from Tomcat 9 context file
* Provided a sample Jenkinsfile for testing a docker-compose deployment of the EP stack
* Provided additional configuration to the MySQL Docker image to limit memory usage

#### Bug Fixes

* `CLOUD-1043` - Removed Author and Live DataPopTask Cloudformation stacks after deploying an Author and Live environment. Fixes unintended database reset known issue in 3.2.0
* `CLOUD-1077` - Removed unused Config/ep-sample-datapopulation directory from CloudDeploy
* `CLOUD-1060` - Fixed database connection loss that occurred due to 8 hours of idle time
* `CLOUD-1055` - Bypassed MySql ELB (Elastic Load Balancer) when running data population to speed up and avoid connection timeout on large datasets
* `CLOUD-1078` - Added explicit depends on statements to avoid race conditions in the Author and Live environment

### Known Issues

* `DeployAuthorAndLive` Jenkins job will show a false negative when `AUTHOR_RDS_SNAPSHOT_ID` and `LIVE_RDS_SNAPSHOT_ID` parameters are specified

## `3.2.0`

### Release Highlights

* Added Amazon API Gateway in front of single-instance, and Author and Live environments
* Standardized data population and application configuration across all _Elastic Path Commerce_ environments
    * Data population is now done at environment run time instead of at build time
* All CloudFormation initialization and data population [logs](faq/index.md#logs) are now forwarded to CloudWatch
* Updated the default AMIs (Amazon Machine Image) to use Amazon Linux 2
    * **Note:** This change breaks compatibility with AMIs based off of Amazon Linux 1 images. Updating *CloudOps for AWS* to 3.2 requires an update to the AMIs based on the Amazon Linux images

### Change log

#### CloudCore

* Configured bastion instance to use ep-ec2 key by default for ssh
* Added option to choose which AMI to base CloudOps base AMIs off of

#### CloudDeploy

* Added Amazon API Gateway and Network Load Balancer in front of cortex for single-instance, and Author and Live environments
* Removed requirement for prebuilt RDS Snapshots or prepopulated MySql containers when standing up _Elastic Path Commerce_ environments
* Added load balancers to MySql and ActiveMq in the single instance environment
* Forwarded cfn-init logs from all ec2 instances to CloudWatch
* Moved log groups for single-instance, and Author and Live environments to root CloudFormation stacks so they are easily accessible
* Improved tagging for EC2 instance
* Removed requirement on prebuilt RDS Snapshots for Author and Live environments, and pre-populated MySql docker image for single instance environment
* Upgraded to Amazon Linux 2

#### Docker

* Based our mysql image off of the official docker hub version
* Containerized Data Population tool so data population process could be standardized across _Elastic Path Commerce_ environments
* Moved search host, email, and asset settings from database to environment variables so they can be set dynamically
* Added option to use docker registries other than just Amazon ECR (Elastic Container Registry)
* Removed custom MySql commerce environment from *Elastic Path* Docker repository

#### Bug Fixes

* `CLOUD-782` - Fixed `BuildDeploymentPackage` Jenkins job so it updated the default deployment package after it is run
* `CLOUD-878` - Removed out of date Create-Base-AMI CloudFormation template from CloudDeploy to fix the `CreateEcsAmi` job
* `CLOUD-848` - Fixed authentication issue when pushing artifacts to the Nexus repository
* `CLOUD-904` - Fixed `SyncS3GitRepo` job so it syncs templates to each branch instead of replicating a single branches templates
* `CLOUD-1032` - Fixed error configuring *Data Sync Tool* on integration server
* `SUP-28` - Fixed Nexus container to restart upon termination, or Nexus server restart

### Known Issues

* Update environment scripts can cause an unintended database reset if the root Author and Live stackname is used for the `APPLICATIONS_STACK_NAME`
* Single instance environment sometimes fails during data population for large datasets
