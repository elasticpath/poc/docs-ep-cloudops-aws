---
id: version-3.2.x-best-practices
title: Elastic Path CloudOps for AWS Best Practices
sidebar_label: Best Practices
original_id: best-practices
---

## Multiple Accounts for Different Environments

You must use different accounts for different environments when you deploy CloudOps. Separate accounts for each part of the pipeline increases security by reducing the chances of promoting Elastic Path application images in development into production accidentally.

When you deploy CloudOps in a development, QA, and pre-production environment pipeline, do the following:

1. Initialize each AWS (Amazon Web Services) account with CloudOps independently. For more information on initializing CloudOps, see [Initializing Components](workflow-overview.md#initializing-components)
2. Set up the following resources to share among the accounts to build the pipeline:
    - Source code
    - Elastic Path application images
    - Deployment package
    - Config

## Best Practices for Sharing the Source Code

If you use
[AWS CodeCommit](https://docs.aws.amazon.com/codecommit/latest/userguide/welcome.html) to host CloudOps, the relevant CloudOps repositories, such as cloudcore-for-aws and docker must be in the same AWS account. All other accounts must only reference these repositories when required. For more information on CodeCommit authentication, see [Authentication and Access Control for AWS CodeCommit](https://docs.aws.amazon.com/codecommit/latest/userguide/auth-and-access-control.html).

If you use a different service than AWS CodeCommit to host CloudOps, you must still not have more than one copy of each CloudOps repository.

> **Note**: Only one AWS account, such as the 'development’ or 'build’ AWS account, must build images. The images must then be promoted to other AWS account’s ECRs. You must not build Elastic Path application images with each AWS account.

### Elastic Path Application Images

Multiple AWS accounts must share the Docker images between their Amazon ECR (Elastic Container Registries). Each AWS account must only modify it’s own ECR only. For example, the production account must pull images into its ECR, and the QA account must pull images into its ECR.

You must create a new Jenkins job in each account’s Jenkins server to ensure this requirement. This job must pull images from another account’s ECR into its own. The following parameters are required for this job:

| Parameter | Purpose |
| --------- | ------- |
| An AWS account’s credentials  | The credential of the source account from where you want to pull the images. |
| ECR repository name | The ECR repository from which you want to pull the images. |
| ECR image tag  | The image that you want to pull.  |

The job then uses the [AWS CLI for ECR](https://docs.aws.amazon.com/AmazonECR/latest/userguide/ECR_AWSCLI.html) to pull the image from the source account and then push it to the destination AWS account.

 > **Note**: Ensure that the Jenkins job is in the Jenkins server of the destination AWS account.

#### Deployment Package

The deployment package contains the *Data Population Tool* required to create or update an RDS (Relational Database Service) instance. It is uploaded to the Amazon S3 bucket during the initial deployment of CloudOps. If you make any updates to the *Data Population Tool* in the build AWS account, the changes must be propagated to the AWS accounts in the pipeline.

The build or development AWS account is the first account in the pipeline, and you must use these accounts to build Elastic Path Application images.

#### Config

Config is unique to each deployment and must be separate for all AWS accounts. The config files include file such as `author.env`, `live.env`, and other files in the `AWS/Config/` folder.

