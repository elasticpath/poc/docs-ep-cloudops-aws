---
id: version-3.2.x-deploy-single-instance
title: Deploying a Single Instance Development Environment
sidebar_label: Deploy Single Instance
original_id: deploy-single-instance
---

## Overview

The following diagram shows the structure of a single instance development environment:

![Elastic Path Commerce related containers on one instance exposed through a load-balancer.](assets/version-3.2.x/ep4aws-test-architecture.png)

## Prerequisites

Before deploying a single-instance development environment, there are several Jenkins job that must be run first. These jobs build necessary Docker images and push them to the AWS (Amazon Web Services) Elastic Container Registry (ECR).

- [BuildActiveMQ](index.md#buildactivemq)
- [BuildBaseImage](index.md#buildbaseimage)
- [BuildEPImage](index.md#buildepimage)
- [BuildMySQL](index.md#buildmysql)


All of these jobs are run by the *InitPipeline* job during [CloudDeploy initialization](../initialization.md). For more information about what the jobs build, see the [Building Elastic Path Commerce Docker Images with CloudDeploy](index.md#docker-image-builds) section.

## Deployment Process

The *DeploySingleInstanceDevEnvironment* Jenkins job deploys an EC2 instance in a private subnet created by CloudCore. This job is located in the Jenkins job folder *CloudOps*, in the Jenkins view *Deployments*.

1. Confirm prerequisites are met.

2. Run the *DeploySingleInstanceDevEnvironment* job. The default values for the parameters are pulled from the [config store](../../architecture/build-op-env.md#configuration-store). For more information on the parameters, see [DeploySingleInstanceDevEnvironment](index.md#deploysingleinstancedevenvironment).

3. Monitor the deployment progress in the AWS CloudFormation console. The environment takes approximately 20 minutes to create. It creates several CloudFormation stacks and sub-stacks. When the root CloudFormation stack completes successfully, the [stack outputs](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-console-view-stack-data-resources.html) display useful information such as endpoints to the Elastic Path services and the AWS CloudWatch LogGroup name where logs can be found

```bash
$ aws cloudformation describe-stacks --stack-name EP-Single-Dev --query 'Stacks[0].Outputs'
[
    {
        "OutputKey": "SIApiGatewayEndpoint",
        "OutputValue": "https://m1x4oqcyze.execute-api.us-west-2.amazonaws.com/cortex"
    },
    {
        "OutputKey": "SIApiGwCustomEndpoint",
        "OutputValue": "https://dev1-cortex-gw.ep-dev.cloudops.example.com/cortex"
    },
    {
        "OutputKey": "SingleInstanceALB",
        "OutputValue": "dev1.ep-dev.cloudops.example.com"
    },
    {
        "OutputKey": "SIActiveMqELBDns",
        "OutputValue": "dev1-activemq.ep-dev.cloudops.example.com"
    },
    {
        "OutputKey": "SIMysqlELBDns",
        "OutputValue": "dev1-mysql.ep-dev.cloudops.example.com"
    },
    {
        "OutputKey": "LogGroup",
        "OutputValue": "EP-Single-Instance-Dev-Env-EP-CloudDeploy-master-1-LogGroup-1QSJJU6DV73LC"
    },
```

> **Note:** Endpoints outputted by CloudFormation stacks work only if [CloudCore DNS Configuration](../cloudCore/configurations.md#dns-configuration) are set up correctly. If DNS is not configured, one work-around is to reference load-balancer’s unique DNS names to access the Elastic Path services on the single-instance environment. In addition, the API gateway custom endpoint domains will not be enabled unless CloudOps was deployed with an HTTPS Certificate.

## Validating Single Instance Deployment

This section outlines how to verify a successful deployment and
connection.

![Example of a successfully built single instance CloudFormation stack](assets/version-3.2.x/complete-si-stack.png)

These checks are done after running the
[`DeploySingleInstanceDevEnvironment` Jenkins job](index.md#deploysingleinstancedevenvironment) and a single instance developer environment is created.

Addresses and credentials used to log in are located at [Default Credentials](../../references.md#default-credentials) page.

### Target Group health check

Check the health of each **Target Group** as follows:

1. Open the **AWS EC2 Console** and click **Target Groups**.

2. Confirm the presence of 5 target groups prefixed by `EP-Si-Targe`.

3. As shown in the following screenshot, select the **Health checks** tab and check the **Path** to find the app the target group points to.
    - ![Target Group App](assets/version-3.2.x/target-group-app.png)
    - Each target group should point to one of the 5 apps:
        - `cm`
        - `integration`
        - `cortex`
        - `batch`
        - `search`

4. Go to the **Monitoring** tab (on the same panel as the **Health checks** tab) and ensure that the number of instances on the **Healthy Hosts** graph is correct

### ActiveMQ transaction check

The following steps are to verify that *Elastic Path Commerce* is able to process transactions.

#### Connect to the ActiveMQ admin tool

1. Go to the ActiveMQ admin tool.

1. Click the **queues** tab, and note the values in the columns **Messages Enqueued** and **Messages Dequeued** from the row queue named *Consumer.consumerRegistrationEmailHandler.VirtualTopic.ep.customers*

![ActiveMQ email queue](assets/version-3.2.x/activemq-email-columns.png)

#### Register a new user in Cortex

1. In *Cortex Studio* [Authenticate](https://developers.elasticpath.com/commerce/7.1/Cortex-API-Front-End-Development/Getting-Started/learningToUseTheAPITool#Authenticating) yourself as a public user
2. In the *Account Registration* section, enter the following information as shown in the following screenshot, then click **registeraction**:
    - **family-name**
    - **given-name**
    - **password**
    - **username**
    - ![Register a new user in Cortex Studio](assets/version-3.2.x/cortex-register-new-user.png)
3. In the ActiveMQ admin tool (refer to *Figure 3. ActiveMQ email queue*), in the columns **Messages Enqueued** and **Messages Dequeued** in the row queue named *Consumer.consumerRegistrationEmailHandler.VirtualTopic.ep.customers*,confirm that values have been incremented

### Search check

#### Searching for promotions and shipping items

1. Log in to the *Commerce Manager* as the administrator.

1. In the *Promotions/Shipping* tab, click **Search**. Confirm that the search result contains search result items

![Searching for promotions and shipping items](assets/version-3.2.x/cm-search-promotions.png)


#### Reindexing search for an index

1. Log in to the *Commerce Manager* as the administrator.

2. In the **Configuration** tab, click **Search Indexes**.

3. Click **Rebuild Index** for any index name. Confirm that the status goes from **Rebuild Scheduled** to **Rebuild in Progress** to **Complete**

![Rebuilding a search index from Commerce Manager](assets/version-3.2.x/cm-rebuild-search-index.png)
