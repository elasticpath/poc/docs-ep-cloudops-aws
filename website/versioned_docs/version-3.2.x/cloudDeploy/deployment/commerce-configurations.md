---
id: version-3.2.x-commerce-configurations
title: Deployment and Elastic Path Commerce Configurations
sidebar_label: Commerce Configurations
original_id: commerce-configurations
---

## Elastic Path Commerce Configuration

### Overview

When building the EP application Docker images, sets of EP configuration files from the deployment package are copied to the `/ep/environments/` directory. When the EP images are deployed one of the sets of EP configuration files is selected by the `EP_COMMERCE_ENVNAME` environment variable. The selected set of configuration files is copied to the `/ep/conf/` directory of the Docker container.

> **Note:** We recommend tuning application cache timeouts when configuring environments. The cache timeouts should be quite short for Author and Single Instance environments. Timeouts for Live environments should balance performance with responsiveness to changes. See [Configuring Ehcache](https://developers.elasticpath.com/commerce/latest/Core-Commerce-Development/Cross-Platform-Technologies/EhCache) for more information.


### Customizing EP configuration

In order to use custom EP configuration you will need to:

1. Add a new environment folder containing your custom configuration files to your EP Commerce repository in the directory: `ep-commerce/extensions/database/ext-data/src/main/resources/environments`

    - > **Note:** The EP applications will source any configuration files that are missing in the new environment folder from the environment folder named `default`

2. Build the deployment package using CloudTeam’s `BuildDeploymentPackage` Jenkins job or using the extensions/packager module.

3. Run the `BuildEPImage` Jenkins job using the new deployment package. This will build the EP application Docker images and a containerized Data Population tool. All images will contain the environments from the deployment package and the containerized Data Population tool image will also contain data sets from the deployment package

### Selecting an EP configuration set

When deploying a single-instance or Author and Live CloudOps environment you may select from one of the EP environment configuration sets that were bundled in the EP Docker images. The EP environment configuration sets originate from the EP Commerce repo in the directory: `ep-commerce/extensions/database/ext-data/src/main/resources/environments`

#### Single-instance

Run the Jenkins job `DeploySingleInstanceDevEnvironment` with the parameter `EP_COMMERCE_ENVNAME` set to the name of the environment with which to deploy.

#### Author and Live

Run the Jenkins job `DeployAuthorAndLive` with the parameters `AUTHORING_EP_COMMERCE_ENVNAME` and `LIVE_EP_COMMERCE_ENVNAME` set to the name of the environments with which to deploy. For more information, see [Deploying Elastic Path Commerce with Jenkins Jobs](../deployment/index.md#elastic-path-commerce-deployments).

### Updating EP configuration

> **Note:** This is only possible with an existing Author and Live CloudOps deployment. A single-instance deployment cannot be automatically updated.

It is possible to update the configuration of an existing Author and Live deployment.

1. Update the EP Commerce configuration in the same EP Commerce configuration set that was used to deploy the Author and Live environment that will be updated. This will be the environment folder selected by the `EP_COMMERCE_ENVNAME` Jenkins parameter in the `DeployAuthorAndLive` Jenkins job.

1. Build a new deployment package using the Jenkins job `BuildDeploymentPackage` and the EP Commerce branch with the updated configuration.

1. Build new EP Docker images using the Jenkins job `BuildEPImage` and the new deployment package.

1. Follow the updating workflow outlined in the [Updating an Author and Live Environment](../deployment/deploy-auth-live.md#updating-author-and-live-environment) section


## Data Population Configuration

### Overview

The deployment package used to build EP Docker images also contains the [Data Population tool](https://developers.elasticpath.com/developer-tools) and an archive containing multiple sets of data, named `ext-data.jar`. The Data Population tool is used to populate a database with EP data and schema changes during deployment of EP Commerce. The tool is configured using one templated Java property file and two Java properties files.:

| Properties File | Purpose |
|---|---|
| `database.properties.mo-template` | Specifies database connection information.  |
| `data-population.properties` | Specifies which sets of data to populate and other required Data Population tool settings. |
| `filtering.properties` | Specifies values used when overriding property placeholders in data. |

These three configuration files are sourced from the EP Commerce code in the folder: `ep-commerce/extensions/database/ext-data/src/main/resources/environments`

The `environments/` folder contains sets of configuration files that are used in the same EP deployment. For example, you can find configuration you would use in a continuous-integration (CI) type deployment under the folder: `ep-commerce/extensions/database/ext-data/src/main/resources/environments/ci/`

There is always a `default` environment which provides default configuration files to fall back on if the configuration files do not exist in the selected environment.

### Data Population Customization

#### Data sets and filtering data configuration

The files `data-population.properties` and `filtering.properties` controls which data sets are populated and what filtering data values are applied. These files are EP Commerce configuration files and originate from the deployment package.

The files should be customized by the steps outlined in the section [Customizing EP Commerce configuration](#customizing-ep-configuration).

The files should be selected using the steps outlines in the section [Selecting an EP Commerce configuration set](#selecting-an-ep-configuration-set).

#### Database configuration

The templated Java property file `database.properties.mo-template` is de-templatized at EP Commerce deployment time with values for database connection information. Most values used to de-templatize the file come from CloudDeploy. For exceptions, see the [Exceptions](#exceptions) section below.

CloudOps uses a containerized version of the Data Population tool, [data-pop-tool](https://code.elasticpath.com/ep-cloudops/docker/tree/release/3.2.x/data-pop-tool). It de-templatizes `database.properties.mo-template` using env vars which come from [CloudOps configuration sets](../configurations.md#cloudops-configuration-sets).


##### Custom database configuration

For detailed steps on how to customize the configuration which is sourced from the CloudDeploy repo, see the page [Customizing CloudOps Configuration Sets](../configurations.md#customizing-configuration-sets).

##### Selecting database configuration

For detailed steps on how to select the configuration which is sourced from the CloudDeploy repo, see the page [CloudOps Configuration Sets](../configurations.md).

##### Exceptions

| Java Property | Source |
|---|---|
| `data.population.host` | Automatically set at EP Commerce deployment time with the value of the database hostname, which may not be known before deployment. |
| `data.population.reset_user` | Automatically set at EP Commerce deployment time with the value of the database root username, which may not be known before deployment. |
| `data.population.reset_password` | Automatically set at EP Commerce deployment time with the value of the database root password, which may not be known before deployment. |

