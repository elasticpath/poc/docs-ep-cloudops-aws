---
id: version-3.2.x-configurations
title: CloudDeploy Configurations
sidebar_label: CloudDeploy Configurations
original_id: configurations
---

## CloudOps Configuration Sets

### Overview

CloudOps uses `.env` files for managing environment variables passed to the EP application Docker containers. The environment variables specify configuration such as Java memory settings and database connection properties. All `.env` files can be found in the CloudDeploy repository in the folder `cloud-deploy-aws/Config/cloudops-env-files/`.

When deploying EP Commerce Jenkins jobs, upload the contents of an `.env` file to a [Consul config store](../architecture/build-op-env.md#configuration-store) using the prefix `deploy/ep/config/v1/envs/{cloudformation-stack-name}/{dot-env-file-name}/`.

The EP application Docker containers pull all the keys under the prefix from the [Consul config store](../architecture/build-op-env.md#configuration-store) and set the env vars.

Most env vars are consumed in the EP-specific Tomcat `setenv.sh` file which sets EP settings through JVM (Java Virtual Machine) system properties.

Some env vars are required and some env vars are optional. The EP apps may use default settings defined by EP Commerce if the optional env vars and JVM system properties are left blank in CloudOps.

### Customizing Configuration Sets

#### Single Instance Environment

To customize the values:

1. Check out the CloudDeploy code.

2. Update the file `cloud-deploy-aws/Config/cloudops-env-files/single-instance-mysql-container.env`

3. Commit and push your code.

4. In the Jenkins job `DeploySingleInstanceDevEnvironment`, set the Jenkins parameter `CLOUDDEPLOY_BRANCH` to the CloudDeploy branch containing the custom configuration

#### Authoring Environment

To customize the values:

1. Check out the CloudDeploy code.

2. Update the file `cloud-deploy-aws/Config/cloudops-env-files/author.env`

3. Commit and push your code.

4. In the Jenkins job `DeployAuthorAndLive`, set the Jenkins parameter `CLOUDDEPLOY_BRANCH` to the CloudDeploy branch containing the custom configuration

> **Note:** To use custom values for both `author.env` and `live.env` the changes must be in the same CloudDeploy branch.

#### Live Environment

To customize the values:

1. Check out the CloudDeploy code.

2. Update the file `cloud-deploy-aws/Config/cloudops-env-files/live.env`

3. Commit and push your code.

4. In the Jenkins job DeployAuthorAndLive, set the Jenkins parameter `CLOUDDEPLOY_BRANCH` to the CloudDeploy branch containing the custom configuration

> **Note:** To use custom values for both `author.env` and `live.env` the changes must be in the same CloudDeploy branch.

### Unsupported Configuration Sets

By default, the `.env` files that CloudOps uses are:

- `author.env`
- `live.env`
- `single-instance-mysql-container.env`

The `cloudops-env-files/` directory also contains additional unsupported `.env` files:

- `single-instance-aurora-rds.env`
- `single-instance-mysql-rds.env`
- `single-instance-mssql-rds.env`
- `single-instance-oracle-rds.env`

These files are used within Elastic Path for testing EP Commerce database compatibility. **These files are included as examples, but are unsupported.**
