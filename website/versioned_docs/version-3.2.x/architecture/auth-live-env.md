---
id: version-3.2.x-auth-live-env
title: Authoring and Live Instance Deployment
sidebar_label: Authoring & Live Deployment
original_id: auth-live-env
---

The complete Elastic Path Commerce stack includes an Authoring and a Live environment. The Live environment is configured for performance, reliability, and scalability. The Authoring environment is used to manage and preview commerce related items and publish changes to the Live environment using the *Data Sync Tool*.

In addition to the production use, you can use these environments for pre-production, performance, and robustness testing. For more information, see [Elastic Path CloudOps for AWS Best Practices](../best-practices.md).

The following architecture diagram highlights the load-balancing and autoscaling for Author and Live environment:

![Load-balancing and autoscaling focused view of an Author and Live environment.](assets/version-3.2.x/authlive-loadbalancing-architecture.png)

## Authoring Environment

The Elastic Path Authoring environment provides business users the ability to manage and preview products, promotions, and pricing. You can publish changes to the Live environment using the *Data Sync Tool*.

Since the Authoring environment is an internally focused environment, the load and reliability requirements are low. All Elastic Path applications are deployed onto a single EC2 instance. Other services used by this single instance, such as the database and the ActiveMQ server, are deployed separately.

## Live Environment

The Elastic Path Live environment is composed of the following:

- Cortex API to deliver the shopping services
- Commerce Manager to deliver customer support and order management capabilities
- Integration server to deliver the application and support for back-end integrations

## Elastic Path Commerce Application Nodes

The Elastic Path Commerce application nodes are composed of the Cortex, Commerce Manager, and the Integration servers, and their dependencies, which include a slave search instance and the necessary file assets.

The application nodes provide shopping services to front-end touch points and are scaled horizontally to meet transactional demand and to maintain reliability.

Most applications run on Docker containers on EC2 instances managed by ECS (Elastic Container Services), with the exception of ActiveMQ that runs directly on EC2 instances. The Docker containers are uniquely attributed to maximize the performance and functionality for a particular server.

For more information on how the nodes and services communicate together, see [Author and Live Network Architecture](infrastructure.md#network).

### Cortex Nodes

The Cortex nodes provide the Cortex API service along with Cortex Studio, a web application for easy interaction with the Cortex API. The node also contains a search slave. The Cortex nodes are the only Elastic Path application nodes that automatically scale beyond one node to adjust for load by default for *Elastic Path CloudOps for AWS (Amazon Web Services)*.

### Commerce Manager Node

The Commerce Manager node is for business users to administer Elastic Path Commerce and contains the Commerce Manager server and a search slave.

### Integration Node

Elastic Path Integration node provides a central point for back-end integrations and contains the integration server and a search slave.

### Other Nodes and Services

#### Admin Node

The Elastic Path Admin node is a singleton node that contains the search master server and the batch server.

#### ActiveMQ Cluster

The ActiveMQ cluster provides a redundant JMS (Java Message Service) to the Elastic Path applications through two ActiveMQ nodes, one active node and one fall-back node. The two nodes share storage through an AWS EFS (Elastic File System).

#### MySQL Database

A MySQL database is used for all Elastic Path Commerce data needs. Both the Author and the Live environment use AWS RDS (Relational Database Service) to provision the databases. Amazon RDS handles routine database tasks such as:

- Provisioning
- Patching
- Backup
- Recovery
- Failure detection
- Repair

By default Aurora replicates the database into other availability zones for failover scenarios.
