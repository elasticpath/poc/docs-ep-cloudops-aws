---
id: version-3.2.x-api-gateway
title: API Gateway for Single Instance, or Author and Live Instance Deployment
sidebar_label: API Gateway
original_id: api-gateway
---

- Cortex lives in a private subnet behind a network load balancer in both auth/live and si
- The cortex network load balancer is connected to an AWS (Amazon Web Services) managed VPC (Virtual Private Cloud) Link device
- The VPC Link allows the AWS API Gateway to connect to the cortex NLB (Network Load Balancer) via a private direct connection
- The API Gateway is deployed to two stages: one for cortex and one for cortex studio; they respond to `/cortex` and `/studio`
- Our API Gateway deployment returns an `OPTIONS` response that allows all domain origins access for CORS (Cross Origin Resource Sharing) purposes
- The API Gatway is only accessible via `HTTPS`
- If cloudops was deployed with a `HTTPS` certificate then the API Gateway is accessed via the custom DNS name set in the Jenkins job
- If cloudops was deployed without a `HTTPS` certificate then it is still accessed via `HTTPS`, however a `DNS` name provied by the API Gateway is used
- Only one of the above two DNS names can be used to access the API Gateway
