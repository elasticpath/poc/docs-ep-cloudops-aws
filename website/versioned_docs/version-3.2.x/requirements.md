---
id: version-3.2.x-requirements
title: Requirements
sidebar_label: Requirements
original_id: requirements
---

## Elastic Path Commerce Requirements

Before using Elastic Path CloudOps for AWS (Amnazon Web Services), ensure that you have an Elastic Path Commerce source code distribution. This package must be in a repository created in the  source control management system. For more information, see the [Git Repository Hosting Service](#git-repository-hosting-service) section.

Note that the source code distribution and deployment package are not the same. The deployment package is created from the source code distribution.

## Elastic Path CloudOps for AWS Requirements

Before initializing Elastic Path CloudOps for AWS, you must ensure that the following requirements are met.

### Git Repository Hosting Service

Before using Elastic Path CloudOps for AWS, you must clone all repositories comprising Elastic Path CloudOps for AWS to a Git repository hosting service, such as GitHub, Bitbucket, or CodeCommit.
To complete this requirement, you must have access to the CloudOps repositories hosted at [Elastic Path Source Code Repositories](https://code.elasticpath.com/).

### Access to DNS Configuration

To leverage the use of Route53 by CloudOps completely, you must have access to update name servers for the domain name you want to use with CloudOps.

If you want to use CloudOps with SSL, an SSL certificate for your domain name, `$DOMAIN_NAME`, and the following sub-domains are also required:

- `*.$DOMAIN_NAME.com`
- `*.ep-build.$DOMAIN_NAME.com`
- `*.ep-preprod.$DOMAIN_NAME.com`
- `*.ep-dev.$DOMAIN_NAME.com`

For example, if you want to use the domain name `cloudops.example.com`, you need an SSL certificate including the following names:

- `*.cloudops.example.com`
- `*.ep-build.cloudops.example.com`
- `*.ep-preprod.cloudops.example.com`
- `*.ep-dev.cloudops.example.com`

If you are using multiple Author and Live environments, you will need SSL certificates for each of of your environments. Following the previous example, if you have an Author and Live environments for qa and staging with the following base domain names

- `ep-preprod-qa`
- `ep-preprod-staging`

You need an SSL certificate including the following names:

- `*.ep-preprod-qa.cloudops.example.com`
- `*.ep-preprod-staging.cloudops.example.com`

### AWS Requirements

CloudOps leverages several AWS services. Before getting started with CloudOps, ensure that the following requirements are met, and you are proficient in the following services that CloudOps uses:

- An empty AWS Account created with credit card details included and administrator access. You must provide credit card details to use AWS services required by CloudOps
- An [AWS region supported by ECR (Elastic Container Registry)](https://aws.amazon.com/about-aws/global-infrastructure/regional-product-services/)
- An AWS EC2 vCPU quota of at least 120. The limit default is 1152. For new accounts, the limit may be lower. To increase the limit, request an increase from AWS. For more information about requesting an increase, see [Amazon EC2 Service Limits](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-resource-limits.html)
- Proficiency in the following AWS services:
    - Amazon CloudFormation
    - Amazon VPC (Virtual Private Cloud)
    - Amazon Route53
    - Amazon EC2
    - Amazon ECS (Elastic Container Service)
    - Amazon RDS (Relational Database Service)
    - Amazon S3
    - Amazon CloudWatch
    - AWS Certificate Manager
    - AWS Lambda

### Software and Other Requirements

The following are the system requirements to initialize CloudOps components:

- An SSH key authorized to the Git service hosting your copy of CloudOps repositories. For more information on creating SSH keys, see [this github documentation page](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)
- URL to download licensed JDK (Java Development Kit)
- Username and password to Elastic Path Nexus to download source code and create a mirror of the source code
- The AWS CLI (Command Line Interface) installed and configured correctly. For more information on configuring the AWS CLI, see [this AWS documentation page](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
- A Linux or Mac machine with the following programs installed:
    - Bash
    - Git
    - [Docker](https://docs.docker.com/install/) version 17 or higher
