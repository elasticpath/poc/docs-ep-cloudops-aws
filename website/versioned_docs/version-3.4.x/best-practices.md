---
id: version-3.4.x-best-practices
title: Elastic Path CloudOps for AWS Best Practices
sidebar_label: Best Practices
original_id: best-practices
---

## Multiple AWS Accounts for Different Release Stages

To reduce the chance of accidentally promoting untested and possibly unstable artifacts into production, use different AWS (Amazon Web Services) accounts for each phase of an Elastic Path Commerce release such as:

- QA
- Staging
- Production

Do the following to use CloudOps with multiple release stages:

1. Initialize each AWS account with CloudOps independently. For more information on initializing CloudOps, see the [Initializing Components](workflow-overview.html#initializing-components) section.

2. Share the following resources between AWS accounts:
    - Source code
    - Docker images

## Sharing Source Code

There must be only one copy of each Git repository used by CloudOps for AWS. This includes the following Elastic Path repositories:

- Elastic Path Commerce
- Docker
- CloudCore
- CloudTeam
- CloudDeploy

For example, if you use [AWS CodeCommit](https://docs.aws.amazon.com/codecommit/latest/userguide/welcome.html) to host Elastic Path Git repositories, the repositories must be in only one AWS account. All other accounts must only reference these repositories when required.

For more information on CodeCommit authentication, see the AWS document [Authentication and Access Control for AWS CodeCommit](https://docs.aws.amazon.com/codecommit/latest/userguide/auth-and-access-control.html).

## Promoting Elastic Path Docker Images

Build Docker images once only using the AWS account used for continuous integration (CI) or QA purposes. Promote those images to other AWS accounts, such as the staging and production accounts. For more information on how to promote Docker images, see [Promoting Docker Images](cloudDeploy/deployment/promote-to-different-aws-account.html)

## Elastic Path Deployment Package

The deployment package built from Elastic Path Commerce source code **does not** need to be promoted to other AWS accounts.

All required artifacts from the deployment package, including applications and data, are converted to Docker images.

## CloudOps Configuration

For each Elastic Path Commerce deployment, each AWS account must have their own unique set of CloudOps configuration files.

CloudOps configuration files are found in the CloudOps component CloudDeploy under the `cloud-deploy-aws/Config/cloudops-env-files` directory. Examples of configuration files contained in the `cloudops-env-files` directory include:

- `author.env`
- `live.env`
- `single-instance-mysql-container.env`
