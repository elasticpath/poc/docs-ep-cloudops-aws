---
id: version-3.4.x-references
title: Reference Materials of CloudOps for AWS
sidebar_label: Reference Materials
original_id: references
---

## Protocol And Domain Names

If the CloudOps components were initialized using a certificate ARN (Amazon Resource Name), prefix all hostnames with the protocol `https://`, if it was left blank use `http://`.

### Root Domain Name

The root domain name used for accessing most services is set during CloudCore initialization. The CloudCore bootstrap parameter `epCloudOpsDomain` configures this value.

> **Note:** This section uses an example value of `cloudops.example.com`.

### Finding Hostname Values in CloudFormation Stack Outputs

The endpoints for services are found in the **Outputs** section of a CloudFormation stack. To find the value:

1. Refer to this document to find the CloudFormation stack name and output name for the service you are accessing
2. Go to the CloudFormation service console
3. Select the root CloudFormation stack that created the service you are accessing
4. Click the **Outputs** tab
5. Find the CloudFormation output from step 1.

    ![API gateway endpoint from a single-instance deployment.](assets/version-3.4.x/si-cf-endpoint-outputs.png)

### HTTP Protocol

The HTTP/HTTPS protocol that will be used to access services depends on the use of a certificate, and whether or not the API Gateway is used.

The HTTPS protocol will be used used if one of the following is true:

- CloudCore was initialized using an ACM (Amazon Certificate Manager) SSL/TLS certificate
- An API Gateway is used

> **Note:** This document assumes the use of a valid certificate during CloudCore initialization.

The HTTP protocol will be used if both of the following are true:

- The API Gateway is not used
- A certificate is not used

### SSL/TLS certificates and AWS API Gateway

In AWS (Amazon Web Services) the API Gateway requires a valid certificate in order to use the custom domain name provided at CloudCore initialization time. Otherwise, services behind an API Gateway can only be accessed using an auto-generated URL created by the API Gateway.

> **Note:** CloudFormation outputs marked with two asterisks (\*\*) are only available if a valid certificate was used during CloudCore initialization.
>
> If a valid certificate was *not* passed during CloudCore initialization, use CloudFormation outputs that end with `ApiGatewayEndpoint` instead of the asterisked outputs.


## Default Credentials

### Nexus Server

| CloudFormation stack | CloudFormation output with hostname | Path | Example |
|----------------------|---------------------|------|---------|
| EP-CT-Nexus | NexusDnsName | `/nexus` | `https://nexus.ep-build.cloudops.example.com/nexus` |


#### Default Nexus Users

| Role | Username | Password | Privileges |
|------|----------|----------|------------|
| Administration | `admin` | `admin123` | All privileges |
| Deployment | `deployment` | `deployment123` | Read and deploy to repositories |
| Anonymous | n/a | n/a | Read-only |

### Jenkins

| CloudFormation stack | CloudFormation output with hostname | Example |
|----------------------|---------------------|---------|
| EP-CC-Jenkins-Server | JenkinsDnsName | `https://jenkins.ep-build.cloudops.example.com` |

#### Default Jenkins Users

| Username | Password |
|----------|----------|
| `admin` | `El4stic123` |

### Configuration Store

CloudOps for AWS uses [Consul’s key-value feature](https://www.consul.io/docs/agent/kv.html) as a configuration store.

| CloudFormation stack | CloudFormation output with hostname | Example |
|----------------------|-------------------------------------|---------|
| EP-CC-Config-Store | ConsulDnsName | `https://config.cloudops.example.com` |

### Single Instance Services

The CloudFormation stack name for a single-instance deployment is configurable:

- `EP-Single-Instance-Dev-Env-EP-*`: the default name prefix

To set another value, refer to the [Jenkins job `DeploySingleInstance`](cloudDeploy/deployment/index.html#deploysingleinstancedevenvironment)

> **Note:** When using an API gateway, the trailing slash is required for the _Studio_ endpoint.

| Application | CloudFormation output with hostname | Path | Example | Default Username | Default Password |
|-------------|-------------------------------------|------|---------|:----------------:|:----------------:|
| Cortex Studio | SIApiGwCustomEndpointStudio** | `/studio/` | `https://dev1-cortex-gw.ep-dev.cloudops.example.com/studio/` | - | - |
| Cortex | SIApiGwCustomEndpointCortex** | `/cortex` | `https://dev1-cortex-gw.ep-dev.cloudops.example.com/cortex` | - | - |
| Commerce Manager | SingleInstanceALB | `/cm/admin` | `https://dev1.ep-dev.cloudops.example.com/cm/admin` | `admin` | `111111` |
| ActiveMQ admin tool | SIActiveMqELBDns | `/admin` | `https://dev1-activemq.ep-dev.cloudops.example.com/admin` | `admin` | `admin` |

### TCP Based Services

The database and ActiveMQ services are TCP based and can be accessed using the following information.

| Service Type | Service Name | CloudFormation output with hostname | Port |
|---|---|---|---|
| Database | MySQL | `SIMysqlELBDns` | `3306` |
| JMS (Java Messaging Service) | ActiveMQ | `SIActiveMqELBDns` | `61616` |

### Author Services

The CloudFormation stack name for an Author and Live deployment is configurable.

- `EP-Author-And-Live-Environment-*`: the default name prefix

To set another value, refer to the [Jenkins job `DeployAuthorAndLive`](cloudDeploy/deployment/index.html#deployauthorandlive)

> **Note:** When using an API gateway, the trailing slash is required for the _Studio_ endpoint.

| Application | CloudFormation output with hostname | Path | Example | Default Username | Default Password |
|-------------|-------------------------------------|------|---------|:----------------:|:----------------:|
| Cortex Studio | AuthorApiGwCustomEndpointStudio** | `/studio/` | `https://author-cortex-gw.al.cloudops.example.com/studio/` | - | - |
| Cortex | AuthorApiGwCustomEndpointCortex** | `/cortex` | `https://author-cortex-gw.al.cloudops.example.com/cortex` | - | - |
| Commerce Manager | AuthorElbEndPoint | `/cm/admin` | `https://author.al.cloudops.example.com/cm/admin` | `admin` | `111111` |

### Live Services

The CloudFormation stack name for an Author and Live deployment is configurable.

- `EP-Author-And-Live-Environment-*`: the default name prefix

To set another value, refer to the [Jenkins job `DeployAuthorAndLive`](cloudDeploy/deployment/index.html#deployauthorandlive)

> **Note:** When using an API gateway, the trailing slash is required for the _Studio_ endpoint.

| Application | CloudFormation output with hostname | Path | Example | Default Username | Default Password |
|-------------|-------------------------------------|------|---------|:----------------:|:----------------:|
| Cortex Studio | LiveApiGwCustomEndpointStudio** | `/studio/` | `https://live-cortex-gw.al.cloudops.example.com/studio/` | - | - |
| Cortex | LiveApiGwCustomEndpointCortex** | `/cortex` | `https://live-cortex-gw.al.cloudops.example.com/cortex` | - | - |
| Commerce Manager | LiveCmElbEndPoint | `/cm/admin` | `https://cm.al.cloudops.example.com/cm/admin` | `admin` | `111111` |

### Accessing Services In Private Subnets

Services whose endpoints are in the private subnets must be accessed using the CloudOps bastion server. For more information on accessing the bastion server, see [how to access the bastion server](faq/deployments.html##access-ec2-instances-in-private-subnets-using-ssh-and-bastion-server) page. **Ensure you have the public IP address of the bastion server and the private SSH key authorized to access the bastion server before proceeding.**

### Accessing Live ActiveMQ Admin Console

This task will show how to access the ActiveMQ admin console that is part of a Live-type EP Commerce deployment. Follow these steps to display the ActiveMQ admin console in a browser.

1. Find the CloudFormation stack that created the Live ActiveMQ cluster load-balancer.

    The default name is `EP-Author-And-Live-Environment-*-LiveActivemqCluster-*`.

2. Select the **Resources** tab and look for the **ActiveMqLoadBalancer** resource.

3. Click the Physical ID link.

4. Go to the **Instances** tab and click the Instance ID of the `InService` EC2 instance.

    ![Live ActiveMQ cluster instances.](assets/version-3.4.x/live-activemq-loadbalancer-instances.png)

5. Using the private IP address of the ActiveMQ EC2 instance and the public IP address of the bastion server, port-forward the ActiveMQ admin console to your local machine

```bash
ssh -i <path-to-bastion-private-key>/<bastion-private-key-name> \
    -L <local-port>:<activemq-private-ip>:8161 \
    ec2-user@<bastion-public-ip>
```

Now, access the ActiveMQ admin console using the port-forwarded port. For example, if you port-forwarded the ActiveMQ admin console’s port `8161` to your local machine’s port `8162` use the address `http://localhost:8162/admin`.

You should now see the ActiveMQ admin console in your browser.

### Accessing Author ActiveMQ Admin console

This task will show how to access the ActiveMQ admin console that is part of an Authoring-type EP Commerce deployment. Follow these steps to display the ActiveMQ admin console in a browser.

1. Find the CloudFormation stack that created the Author ActiveMQ cluster load-balancer.

    The default name is `EP-Author-And-Live-Environment-*-AuthorActivemqCluster-*`.

2. Follow steps 2 - 6 from the [_Accessing the Live ActiveMQ admin console_ procedure](#accessing-live-activemq-admin-console)

You should now see the ActiveMQ admin console in your browser.

### Accessing OSGi Console of Cortex Instance

This task will show how to access the OSGi console that is part of any Cortex instance. Follow these steps to display the OSGi console in a browser.

1. Find the private IP address of the EC2 instance hosting the OSGi console’s Cortex instance.

2. Port-forward the Cortex port `8080` to a port on your local machine using the bastion server

```bash
ssh -i <path-to-bastion-private-key>/<bastion-private-key-name> \
    -L <local-port>:<ec2-private-ip>:8080 \
    ec2-user@<bastion-public-ip>
```

Now, access the OSGi console using the port-forwarded port. For example, if you port-forwarded Cortex’s port `8080` to your local machine’s port `4567` use the address `http://localhost:4567/cortex/system/console`.

You should now see the OSGi console in your browser.

## Port Mappings

For each Elastic Path Container that runs in a CloudOps environment, we expose the following three ports on their host EC2 instance:

- HTTP Host Port
    - Making API calls against the container
    - Health checks
    - Access to the Tomcat and OSGi consoles
- Debug Host Port
    - Remote tomcat debug port
- JMX Host Port
    - Remote JMX port for connecting java profilers to e.g. jvisualvm or jconsole

### Container Ports exposed on their Host EC2 instance

The following table lists the ports that are exposed by each container. Column 1 is sorted by the  **EC2 instance Role** that hosts each **Container** that is listed in column 2.

| EC2 Instance Role  | Container   | HTTP Host Port | Debug Host Port | JMX Host Port |
| -----------        | ----------  | :----------:   | :---------:     | :---------:   |
| single    | cortex      | 8080           | 1080            | 8880          |
| single    | cm          | 8081           | 1081            | 8881          |
| single    | search      | 8082           | 1082            | 8882          |
| single    | integration | 8083           | 1083            | 8883          |
| single    | batch       | 8084           | 1084            | 8884          |
| author             | cortex      | 8080           | 1080            | 8880          |
| author             | cm          | 8081           | 1081            | 8881          |
| author             | search      | 8082           | 1082            | 8882          |
| author             | integration | 8083           | 1083            | 8883          |
| author             | batch       | 8084           | 1084            | 8884          |
| author             | data-sync   | 8086           | 1085            | 8885          |
| cortex        | cortex      | 8080           | 1081            | 8888          |
| cortex        | search      | 8082           | 1082            | 8889          |
| cm            | cm          | 8080           | 1081            | 8888          |
| cm            | search      | 8082           | 1082            | 8882          |
| integration   | integration | 8080           | 1081            | 8888          |
| integration   | search      | 8082           | 1082            | 8889          |
| admin | search      | 8082           | 1081            | 8888          |
| admin | batch       | 8080           | 1082            | 8889          |


## Tags On EC2 Instances

EC2 instances deployed by CloudOps usually contain values for the following tags:

### `Name`

Basic reference name of the EC2 instance. Value shows up under the **Name** column in the AWS EC2 Console:

- `Cloud-Core-Bastion`
- `EP-Development-Server`

### `AccountRoleTag`

The role of the AWS account in which EC2 instances are deployed. CloudOps users often have separate AWS accounts, each separately initialized with CloudOps.

For example, CloudOps users use different AWS accounts to deploy EP Commerce for QA, EP Commerce for staging production and EP Commerce for production:

- `dev`
- `qa`
- `staging`
- `prod`

### `InstanceRole`

The specific role of the EC2 instance:

- `author`
- `cortex`
- `base-ami`
- `single`

### `CloudOpsEnvironment`

The CloudOps environment the EC2 instance is a part of. Can be part of either a single-instance, authoring or live environment:

- `single-instance`
- `author`
- `live`

### `CommerceEnvironment`

The Elastic Path Commerce environment folder with which the CloudOps environment was deployed. Must be updated manually if the `EP_COMMERCE_ENVNAME` env var passed to the docker containers is changed in the config store.

- `sample-author`
- `sample-live`

For more information on how Elastic Path Commerce environment folders are chosen, see the [Elastic Path Commerce Configuration](cloudDeploy/deployment/commerce-configurations.html#elastic-path-commerce-configuration) page.
