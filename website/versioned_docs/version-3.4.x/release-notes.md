---
id: version-3.4.x-release-notes
title: Release Notes
original_id: release-notes
---

## `3.4.2`

### Change Log

#### CloudCore

* `CLOUD-1649` - Version locked all Jenkins plugins
* `CLOUD-1643` - Added a new Jenkins job `ProductionSecurityGroups` to secure public, private, and bastion [EC2 Security Groups](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html)

## `3.4.1`

### Change Log

#### CloudCore

* `CLOUD-1660` - Locked the Jenkins Durable Task plugin to version 1.30 to avoid issues introduced in the latest version of the plugin

### Bug Fixes

* `SUP-250` - Fixed the issue of SQS Purchases queues not receiving messages from cross-account Simple Notification Service (SNS) subscriptions

## `3.4.0`

### Release Highlights

#### CI Pipeline for Development Teams

Added a Continuous Integration (CI) pipeline for development teams that builds Elastic Path Commerce from source, deploys to a single-instance server and runs system tests.

#### Simple Queue Service (SQS) Integration

Configured SQS queues for message-based integration with external systems for purchases, shipments and inventory.

#### Security and Compliance

* Added the ability to inject Commerce configuration at runtime from an encrypted Git repository
* Enabled encryption of Aurora production databases
* Improved access security for Consul, JMX, Tomcat and Jenkins:
    * Secured access to Consul configuration using Access Control Lists (ACLs) keys provided by a Vault cluster
    * Enabled JMX user and password authentication
    * Removed default Tomcat web applications, such as Manager and Host Manager
    * Enabled use of an API token for Jenkins API calls
* Disabled Tomcat logging of system properties

#### Availability

Added ability to deploy an RDS read-replica for an Author database and changed the default to deploy read-replicas for an Author and Live deployment.

#### Jenkins

* Added a Jenkins job to update single-instance deployments
* Added a Jenkins job to teardown environments
* Converted Jenkins jobs to use Jenkinsfiles to simplify job upgrades and definitions

### Upgrade Notes

#### Redeployment Required

This is a major release with significant changes to the core infrastructure. As a result a full redeployment of the infrastructure is required.

#### Jenkins Job Customizations

Any previous customizations to Jenkins jobs must be applied to the appropriate Jenkinsfiles.

#### Jenkins Job Name Changes

The `BuildDeploymentPackage` Jenkins job was renamed to `BuildCommerce` as the job now has additional options to deploy build artifacts to Nexus and build Commerce with tests.


### Change Log

#### CloudCore

* `CLOUD-1431` - Decreased the Jenkins master node to a `t3.small` instance size
* `CLOUD-1456` - Introduced two new Jenkins build node types; one small and one large
* `CLOUD-1451` - Converted Jenkins jobs into Jenkinsfiles
* `CLOUD-1445` - Secured Consul using Access Control Lists (ACLs) keys provided by a Vault cluster
* `CLOUD-1545` - Modified Jenkins installation to use the latest Long Term Stable (LTS) version instead of a locked version
* `CLOUD-1549` - Generated API token to use for authentication in Jenkins API calls
* `CLOUD-1606` - Locked the Jenkins EC2 plugin to version 1.45 to avoid issues introduced in the latest version of the plugin

#### CloudTeam

* `CLOUD-1451` - Converted Jenkins jobs into Jenkinsfiles
* `CLOUD-1450` - Added the Spring Plugins Maven repository to Nexus
* `CLOUD-1549` - Converted Jenkins API calls to authenticate with the Jenkins API token
* `CLOUD-1455` - Renamed the `BuildDeploymentPackage` Jenkins job to `BuildCommerce`. Added new functionality to the job, such as the ability to push build artifacts to Nexus and building Commerce with tests
* `CLOUD-1454` - Added new Jenkins pipeline jobs `CommercePipeline`, `CommerceScheduledPipeline`, `RunCortexSystemTests`, and `RunOtherCommerceTests`
* `CLOUD-1595` - Increased the maximum heap size used by the Maven Docker image to avoid out of memory issues, and speed up build times

#### CloudDeploy

* `CLOUD-838` - Added ActiveMQ logs to the Author and Live CloudWatch log groups
* `CLOUD-1332` - Changed the default Author and Live subdomains from `ep-preprod` to `al`
* `CLOUD-1320` - Enabled JMX user and password authentication for all Authoring and Live deployments
* `CLOUD-1265` - Added job for updating single-instance deployments
* `CLOUD-1452` - Added functionality to use Simple Queue Service (SQS) for message-based integration with external systems
* `CLOUD-1451` - Converted Jenkins jobs into Jenkinsfiles
* `CLOUD-1309` - Added ability to deploy an RDS read-replica for an Author database and changed the default to deploy read-replicas for an Author and Live deployment
* `CLOUD-1359` - Enabled detailed monitoring on single-instance EC2 instances
* `CLOUD-1502` - Changed `EP_SMTP_HOST` and `EP_SMTP_PORT` configuration in single-instance and Author and Live deployments
* `CLOUD-1549` - Converted Jenkins API calls to authenticate with the Jenkins API token
* `CLOUD-1455` - Output the private IP of the single-instance to the `DeploySingleInstance` Jenkins job logs
* `PM-998` - Added ability to inject commerce configuration after application Docker images are built
* `SUP-151` - Increased timeouts for Author and Live and single-instance deployments
* `CLOUD-1454` - Added new Jenkins pipeline job `TeardownEnvironments`
* `PM-1389` - Added an `/ep/app/` directory for the batch and integration servers
* `OPS-2` - Added a Jenkins parameter for adding a description to builds of several Jenkins jobs
* `CLOUD-1592` - Stabilized deployments by adjusting memory resourcing and increasing EC2 instance sizes of single-instance and Author deployments

#### Docker

* `CLOUD-1321` - Removed default Tomcat web applications, such as Manager and Host Manager
* `CLOUD-1361` - Disabled Tomcat logging of system properties
* `CLOUD-1320` - Added support for enabling JMX user and password authentication
* `CLOUD-1263` - Moved container healthchecks from Docker containers to the Elastic Load Balancers (ELB)
* `CLOUD-1231` - Added a step in the Docker entrypoint scripts to wait for dependent services to start
* `CLOUD-1497` - Added redundant key servers to verify signatures of packages when you download GPG public keys to increase build stability
* `CLOUD-1502` - Changed `EP_SMTP_HOST` and `EP_SMTP_PORT` configuration in `common.env` and `common.live.env`
* `PB-6191` - Improved performance of database validation query
* `PERF-157` - Added environment properties to configure search indexer queue sizes to handle large catalogs
* `PM-998` - Added ability to inject commerce configuration after application Docker images are built
* `PM-1389` - Mounted Amazon Elastic File System volume for the batch and integration servers

### Bug Fixes

* `CLOUD-1317` - Fixed an Identity and Access Management (IAM) permission issue preventing Elastic Container Service (ECS) metrics from being uploaded to CloudWatch
* `CLOUD-1402` - Fixed database failover logic to use the correct Aurora cluster endpoint
* `SUP-118` - Added better error logging when CloudTeam or CloudDeploy initialization fails due to DNS name servers not being updated
* `SUP-121` - Removed duplicate Git repository URL Consul key
* `SUP-138` - Fixed intermittent bootstrap issue where Jenkins jobs would run before initialization was complete
* `SUP-125` - Fixed validation when checking for unset environment variables
* `SUP-126` - Fixed validation when checking the Canonical Name (CNAME) record in the Jenkins job `Verify-Domain-for-SES`
* `SUP-167` - Fixed how the script `runCleanOutCloudCore.sh` copies the Git SSH key. This addresses an issue where the key is deleted when it is in the same directory as the cleanup container Dockerfile
* `SUP-191` - Adjusted the memory sizes for the batch and search applications
* `SUP-122` - Added additional parameter validation to the Author and Live deployment update Jenkins job

### Known Issues

* `CLOUD-1564` - All Jenkins jobs named `bootstrap` ignore the branch parameter
* `CLOUD-1524` - Parallel runs of the Jenkins job `BuildEPImage` cause the first run to fail
* `CLOUD-1612` - Parallel runs of the Jenkins job `CommercePipeline` conflict and fail
