---
id: version-3.4.x-commerce-configurations
title: Deployment and Elastic Path Commerce Configurations
sidebar_label: Commerce Configurations
original_id: commerce-configurations
---

## Elastic Path Commerce Configuration

### Overview

When building the Elastic Path application Docker images, sets of configuration files from the deployment package are copied to the `/ep/environments/` directory on the Docker image. When you deploy the Docker images, one of the sets of Elastic Path Commerce configuration files is selected by the `EP_COMMERCE_ENVNAME` environment variable. The selected set of configuration files is copied to the `/ep/conf/` directory of the Docker container.

> **Note:** We recommend tuning application cache timeouts when configuring environments. The cache timeouts should be quite short for Author and Single Instance environments. Timeouts for Live environments should balance performance with responsiveness to changes. See [Configuring Ehcache](https://developers.elasticpath.com/commerce/latest/Core-Commerce-Development/Cross-Platform-Technologies/EhCache) for more information.


### Customizing Elastic Path Commerce configuration

In order to use custom Elastic Path Commerce Configuration you will need to:

1. Add a new environment folder containing your custom configuration files to your Elastic Path Commerce repository in the directory: `ep-commerce/extensions/database/ext-data/src/main/resources/environments`

    > **Note:** The Elastic Path applications will source any configuration files that are missing in the new environment folder from the environment folder named `default`

2. Build the deployment package using CloudTeam’s `BuildCommerce` Jenkins job or using the extensions/packager module.

3. Run the `BuildEPImage` Jenkins job using the new deployment package.

    This will build the Elastic Path application Docker images and a containerized Data Population tool. All images will contain the environments from the deployment package and the containerized Data Population tool image will also contain data sets from the deployment package

### Selecting an Elastic Path Configuration Set

When deploying a single-instance or Author and Live CloudOps environment you may select from one of the Elastic Path Commerce environment configuration sets that were bundled in the Elastic Path Commerce repo in the directory: `ep-commerce/extensions/database/ext-data/src/main/resources/environments`

#### Single-instance

Run the Jenkins job `DeploySingleInstance` with the parameter `EP_COMMERCE_ENVNAME` set to the name of the environment with which to deploy.

#### Author and Live

Run the Jenkins job `DeployAuthorAndLive` with the parameters `AUTHORING_EP_COMMERCE_ENVNAME` and `LIVE_EP_COMMERCE_ENVNAME` set to the name of the environments with which to deploy. For more information, see [Deploying Elastic Path Commerce with Jenkins Jobs](../deployment/index.html#elastic-path-commerce-deployments).

### Updating Elastic Path Commerce Configuration

> **Note:** This is only possible with an existing Author and Live CloudOps deployment. A single-instance deployment cannot be automatically updated.

To update the Elastic Path Commerce configuration of an existing Author and Live deployment:

1. Update the Elastic Path Commerce configuration set that was used in the original Author and Live deployment.

    This will be the environment folder selected by the `EP_COMMERCE_ENVNAME` Jenkins parameter in the Jenkins job `DeployAuthorAndLive`.

2. Build a new deployment package using the Jenkins job `BuildCommerce` and the Elastic Path Commerce branch with the updated configuration.

3. Build new Elastic Path Docker images using the Jenkins job `BuildEPImage` and the new deployment package.

4. Follow the updating workflow outlined in the [Updating an Author and Live Environment](../deployment/deploy-auth-live.html#updating-author-and-live-environment) section

### Injecting Commerce Configuration at Runtime

In cases where configuration cannot be committed into the Elastic Path Commerce codebase, it is possible to inject data-population and commerce configuration at runtime of the Docker containers using an encrypted Git repository. This requires additional setup, for detailed steps on how to setup up injecting configuration see the [Injecting Commerce Configuration at Runtime](./injecting-config.html) section.

## Data Population Configuration

### Overview

The deployment package used to build Elastic Path Docker images also contains the [Data Population tool](https://developers.elasticpath.com/developer-tools) and an archive containing multiple sets of data, named `ext-data.jar`. The Data Population tool is used to populate a database and JMS (Java Messaging Service) with Elastic Path data and schema changes during deployment of Elastic Path Commerce. The tool is configured using two Java property file templates and two regular Java properties files:

| Properties File | Purpose |
|---|---|
| `database.properties.mo‑template` | Specifies database connection information.  |
| `default‑jms.properties.mo‑template` | Specifies JMS connection information.  |
| `data‑population.properties` | Specifies which sets of data to populate and other required Data Population tool settings. |
| `filtering.properties` | Specifies values used when overriding property placeholders in data. |

By default, these four configuration files are sourced from the Elastic Path deployment. For example, you can find configuration you would use in a continuous-integration (CI) type deployment under the folder: `ep-commerce/extensions/database/ext-data/src/main/resources/environments/ci/`.

There is always a `default` environment which provides default configuration files to fall back on if the configuration files do not exist in the selected environment.

If the file `default‑jms.properties.mo‑template` is not found in the Elastic Path Commerce `environments/` folders, the containerized Data Population tool that CloudOps for AWS (Amazon Web Services) uses provides a default file from the following path in the Elastic Path Docker repository: `docker/data-pop-tool/templates/default-jms.properties.mo-template`.

### Data Population Customization

#### Data sets and filtering data configuration

The files `data-population.properties` and `filtering.properties` control which data sets are populated and what filtering data values are applied. These files are Elastic Path Commerce configuration files and originate from the deployment package.

Customize these files according to [Customizing Elastic Path Commerce configuration](#customizing-ep-configuration).

Select these files according to [Selecting an Elastic Path Commerce configuration set](#selecting-an-ep-configuration-set).

### Database and JMS configuration

When Elastic Path Commerce is deployed, the Java property file templates `database.properties.mo-template` and `default‑jms.properties.mo‑template` are populated with values for database and JMS connection information. Most values used to de-templatize the file come from [CloudOps Configuration Sets](../configurations.html#selecting-configuration-sets) in CloudDeploy. For exceptions, see the [Exceptions](#exceptions) section below.

Customize the values used by the Java property file templates according to [Customizing CloudOps Configuration Sets](../configurations.html#customizing-configuration-sets).

Select the values used by the Java property file templates according to [Selecting CloudOps Configuration Sets](../configurations.html#selecting-configuration-sets).

#### Exceptions

The following Java properties are set during Elastic Path Commerce deployment since the values are unknown beforehand:

| Java Property | Source |
|---|---|
| `data.population.host` | The database hostname |
| `data.population.jms.url` | The JMS connection string |
| `data.population.reset_user` | Username of the root database user |
| `data.population.reset_password` | Password of the root database user |
