---
id: version-3.4.x-index
title: CloudTeam Overview
sidebar_label: CloudTeam Overview
original_id: index
---

In *Elastic Path CloudOps for AWS* (Amazon Web Services), CloudTeam builds a deployment package from Elastic Path Commerce source code. CloudTeam provides this capability through the Jenkins server created by CloudCore by adding new jobs in the Jenkins folder *CloudTeam*.

The services added by CloudTeam include:

- New Jenkins jobs for building deployment packages
- A Nexus server for mirroring the public Elastic Path Maven server
- An ECR (Elastic Container Registry) repository to hold the Docker images needed for building the deployment packages
