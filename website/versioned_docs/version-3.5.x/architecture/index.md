---
id: version-3.5.x-index
title: Architecture of CloudOps for AWS
sidebar_label: Architecture Overview
original_id: index
---

## CloudOps for AWS

The CloudFormation template hierarchy of *Elastic Path CloudOps for AWS (Amazon Web Services)* provides the option to deploy a complete development infrastructure or to deploy a specific environment to a specific VPC (Virtual Private Cloud).

*Elastic Path CloudOps for AWS* provides CloudFormation templates to build the following environments:

- **Build & Operations Environment**: Sets up a build and operations environment. This includes the infrastructure that supports building and deploying Elastic Path Commerce, such as the Jenkins server, the Nexus server, and the Consul and Vault cluster
- **Single-Instance Environment**: Sets up a small-scale test Elastic Path Commerce environment
- **Author and Live Environments**: Sets up a complete Elastic Path Commerce stack with the Author and Live environments
