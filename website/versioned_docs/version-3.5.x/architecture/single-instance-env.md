---
id: version-3.5.x-single-instance-env
title: Single Instance Deployment
sidebar_label: Single Instance Deployment
original_id: single-instance-env
---

The single instance environment is a small scale test environment without separate authoring capabilities deployed on a single server. The Jenkins job [DeploySingleInstance](../cloudDeploy/deployment/index.html#deploysingleinstancedevenvironment) along with the CloudFormation template `Create-Single-Instance-Server.yaml` deploys the following:

![Elastic Path Commerce related containers on one instance exposed through a load-balancer.](assets/version-3.5.x/ep4aws-test-architecture.png)

## Single Instance Components

The CloudFormation template sets up a single node with the following components that can be accessed through a load-balancer:

- Cortex with all supporting Elastic Path and third-party applications
- Commerce Manager server
- Search Master server
- Batch server
- Integration server
- ActiveMQ
- MySQL database

The single instance environment is able to use AWS (Amazon Web Services) SQS (Simple Queue Service) for message-based integration with external systems. The single instance deployment configures SQS queues for handling purchases, shipments and inventory.

For more information about AWS SQS, see [What is Amazon SQS](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/welcome.html).

## Related Topics

- [Deploying a Single Instance Development Environment](../cloudDeploy/deployment/deploy-single-instance.html)
- [Building & Deploying Elastic Path Commerce Docker Images with Jenkins Jobs](../cloudDeploy/deployment/index.html)
