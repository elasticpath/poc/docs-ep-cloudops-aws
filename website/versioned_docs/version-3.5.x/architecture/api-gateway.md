---
id: version-3.5.x-api-gateway
title: Elastic Path AWS API Gateway Architecture
sidebar_label: API Gateway
original_id: api-gateway
---

## Architecture Overview

Cortex and Cortex Studio are located within a private subnet behind a network load balancer, in single-instance, Author and Live deployments. Additionally:

- The network load balancer is connected to an AWS (Amazon Web Services) VPC (Virtual Private Cloud) link device
- The AWS API Gateway connects to the network load balancer through a private direct connection by using the VPC link
- The API Gateway is deployed for Cortex and Cortex Studio and the API Gateway responds to paths ending in both `/cortex` and `/studio/`

## Access

The API Gateway is secured by only being accessible through HTTPS. The API Gateway endpoint is different depending on whether CloudCore was initialized with a valid SSL/TLS certificate.

- If CloudCore was initialized **with** a certificate, a **custom** API Gateway endpoint is set using the Jenkins job `DeployAuthorAndLive`
- If CloudCore was initialized **without** a certificate, the API Gateway endpoint is **auto-generated** by AWS

> **Note:** An auto-generated endpoint is created in both cases. It cannot be used to access Cortex or Cortex Studio if a custom endpoint is also present.

For more information on how to access Elastic Path Commerce applications, see the section [Default CloudOps Endpoints and Credentials](../references.html#default-credentials).

## Throttling API Requests

The Amazon API Gateway has throttling limits imposed by AWS on an account-level. By default, the steady-state request rate is 10,000 requests per second and the burst limit is 5,000 requests. This throttling applies across all APIs within the account. For *CloudOps for AWS* the single-instance, Author and Live environments are configured with the same throttling limits as what is imposed by the account-level limits by AWS. It is possible to request a limit increase to the steady-state request rate.

- For more information about limits, and what limits can be increased, see [Amazon API Gateway Limits and Known Issues](https://docs.aws.amazon.com/apigateway/latest/developerguide/limits.html)
- For more information about throttling limits set by Amazon, see [Throttle API Requests for Better Throughput](https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-request-throttling.html)
