---
id: version-3.5.x-build-op-env
title: Build and Operations Environment
sidebar_label: Build & Operations Environment
original_id: build-op-env
---

CloudOps includes supporting infrastructure that serves as a *Build and Operations* environment. The *Build and Operations* environment includes services that enable CloudOps features, such as the Jenkins server that has all jobs that builds and deploys Elastic Path Commerce. These services are:

| Service | Purpose |
| ------- | ------- |
| Jenkins | Triggers build and operational tasks, such as building deployment packages, building Docker images, or deploying Elastic Path commerce environments. |
| Config store | Stores configuration used by CloudOps to build and deploy Elastic Path Commerce. CloudOps uses a [Consul](https://learn.hashicorp.com/consul/getting-started/kv.html) server to store the configurations. |
| Nexus | Mirrors the public Elastic Path Maven repository. |
| Bastion server | Enables direct SSH access to application servers in the private subnets. |


The following diagram illustrates how and where this supporting infrastructure is deployed in AWS (Amazon Web Services). It also illustrates how the supporting infrastructure can be accessed with load-balancers:

![CloudOps Build and Operations environment with a focus on load-balancing and autoscaling.](assets/version-3.5.x/cloudops-build-ops-env.png)

## Jenkins

Triggers build and operational tasks, such as:

- Building deployment packages
- Building Docker images
- Deploying Elastic Path commerce environments

### Jenkins Plugins

The Jenkins server uses third-party plugins. These plugins are installed with fixed versions and are upgraded to the latest version without known issues with every CloudOps release.

For a complete list of the plugins installed in the Jenkins server, see [Create-Jenkins.yaml](https://code.elasticpath.com/ep-cloudops/cloud-core-aws/blob/master/CloudFormation/Create-Jenkins.yaml#L240).

## Nexus

Mirrors the public Elastic Path Maven repository. This Maven repository is used when building deployment packages with [CloudTeam](../cloudTeam/index.html).


## Configuration Store

Stores configuration used by CloudOps to build and deploy Elastic Path Commerce. CloudOps uses a [Consul](https://learn.hashicorp.com/consul/getting-started/kv.html) cluster to store these configurations.

For example, the following image displays the default version of ActiveMQ that the CloudDeploy Jenkins jobs uses to build Docker images and deploy Elastic Path Commerce:

![Consul showing the default ActiveMQ version.](assets/version-3.5.x/consul_activemq_version.png)

*Figure 1. Consul cluster that displays the default ActiveMQ version.*

CloudOps uses [Consul’s key-value API](https://www.consul.io/api/kv.html) to retrieve the values.

## Vault

Consul access is restricted to connections made from within the VPC (Virtual Private Cloud) network, or whitelisted IPs in the public security group. Access is further restricted through the use of ACL (Access Control Lists) keys that are provided by a [Vault](https://www.hashicorp.com/products/vault/secrets-management) cluster.

You can configure the vault cluster to provide ACL keys when accessing an IAM (Identity and Access Management) role in your AWS account. This is done using the [AWS Authentication Method](https://www.vaultproject.io/docs/auth/aws.html) for vault. If CloudCore is initialized with an `initialVaultUsername` and `initialVaultPassword` set in the runBootstrap script, then the [Userpass Authentication Method](https://www.vaultproject.io/docs/auth/userpass.html) is also enabled.

### Configuration Store Architecture

The Consul and Vault cluster is created during [CloudCore initialization](../cloudCore/initialization.html). CloudCore creates a cluster with three nodes by default, each running their own copy of consul and vault.

### Jenkins Job Parameters

Most Jenkins jobs use the Consul cluster to store the default values for the parameters. For more information, see the [How to see the parameter values a Jenkins jobs is run with](../faq/index.html#how-to-see-the-parameter-values-a-jenkins-job-is-run-with) section.

### Related Topics

- [Building & Deploying Elastic Path Commerce with Jenkins Jobs](../cloudDeploy/deployment/index.html)
- [ActiveMQ Overview](infrastructure.html#activemq)


## Bastion Instance

The Bastion instance is a server that provides secure access to instances on the public and private subnets within a VPC (Virtual Private Cloud). This page focuses on the architecture and deployment of the Bastion instance. For more information on using the Bastion server to access EC2 instances in private subnets, see the [How to access EC2 instances in private subnets using SSH and the Bastion server](../faq/deployments.html#access-ec2-instances-in-private-subnets-using-ssh-and-bastion-server).

### Bastion Architecture

The CloudFormation template creates an auto scaling group that maintains a group size of one Bastion instance. The default value for the instance is t2.micro and is created with a security group that allows access from one IP address and two security groups. The Bastion instance has its own key-pair that is separate from the key-pair assigned to the other instances created in CloudCore. It also has an Elastic IP assigned. CloudWatch has the logs of the terminal commands that run on the instance.

You can run the template as a part of CloudCore, or on its own. If the template is run as a standalone template, it requires VPC, security group, and subnet values to be filled either as separate fields or as the network stack name field. This is needed if a template is used to create them. You must add the Bastion security group into the security groups that Bastion needs access to.

### Prerequisites

If you run the template as a part of CloudCore, the prerequisites are implemented by default.

If run on its own, the Bastion template requires a VPC, security groups, and subnets to be set up. You must also upload a key-pair.

### Deployment

If run as a part of CloudCore, you need not deploy it separately. It is deployed by default with CloudCore.

To deploy Bastion on its own, you must upload the CloudFormation template and `bastion_bootstrap.sh` script to an Amazon S3 bucket and update the URL to download the script in the template to match. After deployment, you must add the Bastion security group into the inbound rules of any security groups the Bastion instance requires access to and copy the key-pair assigned to the other instances into the Bastion instance.

#### Troubleshooting

To resolve any issues during deployment of the Bastion instance, see the logs in `/var/logs` if the EC2 instance is up. For additional information, see the `cfn-init.log` file. If the instance is not active and up, check the **Events** tab of the CloudFormation stack.
