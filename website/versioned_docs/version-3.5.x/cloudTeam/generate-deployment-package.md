---
id: version-3.5.x-generate-deployment-package
title: Generating Deployment Package Using CloudTeam
sidebar_label: Generate Deployment Package
original_id: generate-deployment-package
---

Deploy a new environment or update an existing environment with updated Elastic Path Commerce source code. To do so, you must first generate a deployment package from the updated Elastic Path Commerce source code.

## Building a Maven Docker Image

CloudTeam uses a Maven Docker container to build deployment packages in a clean, reproducible environment.

1. Run the Jenkins job `BuildMavenImage`.

    > **Note**: The job `BuildMavenImage` should have already been run as part of CloudTeam initialization through the Jenkins pipeline `BuildPipeline`.

## Creating a Deployment Package

Before you can create a deployment package, ensure that the Maven Docker image is built.

1. Run the Jenkins job `BuildCommerce`.

    This job builds a deployment package and uploads it to Amazon S3. From there, other jobs can use it to build Docker images.

    The `BuildCommerce` job updates the Consul setting for `team/ep/deployment-package/uri` in Consul. The downstream jobs that use this parameter use the last-built deployment package by default.

    > **Note:** When creating a CI pipeline, this might affect concurrent builds. To avoid concurrency issues, explicitly define which deployment package to use when building Docker images.

### Next steps

For more information about how to deploy a new environment using a deployment package, see [CloudDeploy](../cloudDeploy/index.html). For more information about updating an existing Author and Live environment using a deployment package, see the [Updating an Author and Live Environment](../cloudDeploy/deployment/deploy-auth-live.html#updating-author-and-live-environment) section.

## Jenkins Jobs

 The CloudTeam bootstrap container runs the Jenkins jobs to set up a Docker container with Maven and to generate an Elastic Path Commerce deployment package after completing CloudTeam initialization. The container runs a seed job, `bootstrap`. The `bootstrap` job generates and runs the following jobs:

- `BuildPipeline`
- `BuildMavenImage`
- `BuildCommerce`

Before running the `bootstrap` job, the container also:

- Sets the default parameters for these jobs in the config store
- Creates a Nexus instance
- Creates an ECR (Elastic Container Registry) repository for the Maven containers

For more information about the parameter values, see the [How to see the parameter values a Jenkins jobs is run with?](../faq/index.html#how-to-see-the-parameter-values-a-jenkins-job-is-run-with) section.

### `bootstrap` job

This job populates the Jenkins server with the other jobs and triggers the BuildPipeline job. The required parameters for this job are:

| Parameter | Description |
|---|---|
|`CLOUDTEAM_GIT_REPO_URL`| The URL of the CloudTeam Git repository. |
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |

### `BuildMavenImage` job

This job creates a Maven Docker image. The `BuildCommerce` job uses this image as an environment to build Commerce. The required parameters for this job are:

| Parameter | Description |
|---|---|
|`EP_MVN_IMAGE_TAG` | The tag name to use on the image when pushed into their repository. The default tag name is, `EP-CloudTeam-<VERSION>`. |
|`EP_ORACLE_JDK_DOWNLOAD_URL` | A licensed Oracle JDK (Java Development Kit) download URL.|
|`EP_ORACLE_JDK_FOLDER_NAME`| The name of the folder where Oracle JDK is downloaded. |
|`EP_MAVEN_FOLDER_NAME`| The name of the folder where Maven binary is downloaded. |
|`CLOUDTEAM_GIT_REPO_URL`| The URL of the CloudTeam Git repository. |
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |

### `BuildCommerce` job

The `BuildCommerce` job builds Commerce using the source code from the Elastic Path Commerce repository. By selecting the `BUILD_DEPLOYMENT_PACKAGE` parameter, the job also builds a deployment package and uploads it to Amazon S3. The required parameters for this job are:

| Parameter | Description |
|---|---|
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |
|`EP_MVN_IMAGE_TAG` | The tag name of the Maven image to use when creating the Maven container. |
|`EP_COMMERCE-GIT_REPO_URL`| The URL of the Elastic Path Commerce repository where the source code distribution is available. |
|`EP_COMMERCE_GIT_REPO_BRANCH`| The branch in the Elastic Path Commerce repository that you want to use.|
|`NEXUS_REPO_BASE_URI`| The URL of the Nexus repository created by the initialization of CloudTeam, including the `http(s)://` prefix. |
|`NEXUS_REPO_USER_NAME`| The user name for the Nexus repository created by the initialization of CloudTeam. By default, CloudOps uses the default Nexus credentials. |
|`NEXUS_REPO_PASSWORD`| The password for the Nexus repository created by the initialization of CloudTeam. By default, CloudOps uses the default Nexus credentials. |
|`CLOUDTEAM_GIT_REPO_URL`| The URL of the CloudTeam Git repository. |
|`DEPLOYMENT_PACKAGE_NAME`| The name of the deployment package that is uploaded to Amazon S3, including .zip. |
|`SKIP_TESTS`| Builds Commerce with the Maven build option `-DskipAllTests`. |
|`RUN_MAVEN_DEPLOY`| Pushes artifacts to Nexus with the Maven build option `deploy` instead of running `install`. |
|`BUILD_DEPLOYMENT_PACKAGE` | Builds a deployment package after building Commerce. |

### `BuildPipeline` job

`BuildPipeline` job runs the `BuildMavenImage` and the `BuildCommerce` jobs.
