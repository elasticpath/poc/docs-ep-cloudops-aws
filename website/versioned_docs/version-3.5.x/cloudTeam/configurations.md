---
id: version-3.5.x-configurations
title: CloudTeam Configurations
sidebar_label: CloudTeam Configurations
original_id: configurations
---

## Nexus Instance Configuration

### Overview

CloudTeam creates a Sonatype Nexus Repository instance that contains a mirror of the Elastic Path public Maven repository. Nexus runs on a standalone EC2 instance that is created during the initialization of the CloudTeam component. The Nexus application runs through a container that is provided by Sonatype. The repository data directory is persisted to the local drive of the EC2 instance. The repository config for Nexus is stored on the EC2 instance at `/opt/nexus2data/conf/nexus.xml`.

### Nexus Customization

The default Nexus config file, `nexus.xml`, is replaced by the one distributed in CloudTeam. This replacement file configures Nexus into a self sufficient Maven repository to build Elastic Path Commerce from source. Two modifications are made to this file during the deployment of Nexus:

- The user name and password required to access the Elastic Path public repository are replaced with the values set when the CloudTeam is initialized
- The `nexus.xml` file is configured as a mirror

Information on how Elastic Path uses a Nexus server can be found in [Setting Up Maven Repository](https://developers.elasticpath.com/commerce/latest/Starting-Construction/Starting-Construction-Guide/Team-Infrastructure-Setup/Set-Up-Maven-Repository) section in the [Starting Construction Guide](https://developers.elasticpath.com/commerce/construction-home).

For information on how to access the Nexus server created by CloudOps, see [Default CloudOps Endpoints and Credentials](../references.html#default-credentials).

