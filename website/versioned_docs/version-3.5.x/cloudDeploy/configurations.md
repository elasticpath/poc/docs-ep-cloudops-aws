---
id: version-3.5.x-configurations
title: CloudDeploy Configurations
sidebar_label: CloudDeploy Configurations
original_id: configurations
---

## CloudOps Configuration Sets

### Overview

CloudOps uses `.env` files for managing environment variables passed to the Elastic Path application Docker containers. For example, configuration settings includes Java memory settings and database connection properties.

All `.env` files can be found in the CloudDeploy repository in the folder `cloud-deploy-aws/Config/cloudops-env-files/`.

When deploying Elastic Path Commerce, Jenkins jobs uploads the contents of an `.env` file to a [Consul config store](../architecture/build-op-env.html#configuration-store) using the prefix `deploy/ep/config/v1/envs/{cloudformation-stack-name}/{dot-env-file-name}/`. The Elastic Path application Docker containers download environment variable values from the prefix in the config store.

Most environment variables are consumed in the Elastic Path-specific Tomcat `setenv.sh` file that sets Elastic Path Commerce settings through JVM (Java Virtual Machine) system properties.

In the event that some optional CloudOps environment variables are left blank, Elastic Path Commerce will provide JVM system properties with default settings.

### Customizing Configuration Sets

#### Single Instance Environment

To customize the values:

1. Check out the CloudDeploy code.

2. Update the file `cloud-deploy-aws/Config/cloudops-env-files/single-instance-mysql-container.env`

3. Commit and push your code.

4. In the Jenkins job `DeploySingleInstance`, set the Jenkins parameter `CLOUDDEPLOY_BRANCH` to the CloudDeploy branch containing the custom configuration

#### Author Environment

To customize the values:

1. Check out the CloudDeploy code.

2. Update the file `cloud-deploy-aws/Config/cloudops-env-files/author.env`.

3. To use your own configuration file based on `author.env` such as `example_new_author.env`, update the Jenkins parameter `AUTHOR_EP_CLOUDOPS_ENVNAME` with this new filename.

4. Commit and push your code.

5. In the Jenkins job `DeployAuthorAndLive`, set the parameter `CLOUDDEPLOY_BRANCH` to point to the CloudDeploy Git branch name that contains the custom configuration file

> **Note:** To use custom values for both `author.env` and `live.env` the changes must be in the same CloudDeploy branch.

#### Live Environment

To customize the values:

1. Check out the CloudDeploy code.

2. Update the file `cloud-deploy-aws/Config/cloudops-env-files/live.env`.

3. To use your own configuration file based on `live.env` such as `example_new_live.env`, update the Jenkins parameter `LIVE_EP_CLOUDOPS_ENVNAME` with this new filename.

4. Commit and push your code.

5. In the Jenkins job `DeployAuthorAndLive`, set the parameter `CLOUDDEPLOY_BRANCH` to point to the CloudDeploy Git branch name that contains the custom configuration file

> **Note:** To use custom values for both `author.env` and `live.env` the changes must be in the same CloudDeploy branch.

### Unsupported Configuration Sets

By default, the `.env` files that CloudOps uses are:

- `author.env`
- `live.env`
- `single-instance-mysql-container.env`

The `cloudops-env-files/` directory also contains additional unsupported `.env` files:

- `single-instance-aurora-rds.env`
- `single-instance-mysql-rds.env`
- `single-instance-mssql-rds.env`
- `single-instance-oracle-rds.env`

These files are used within Elastic Path for testing Elastic Path Commerce database compatibility. **These files are included as examples, but are unsupported.**
