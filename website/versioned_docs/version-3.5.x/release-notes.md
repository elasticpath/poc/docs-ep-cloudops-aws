---
id: version-3.5.x-release-notes
title: Release Notes
original_id: release-notes
---

## `3.5`

### Release Highlights

* Added support to deploy horizontal database scaling with Elastic Path Commerce version 7.6

### Horizontal database scaling

Horizontal database scaling allows an additional increase in transaction throughput by directing read requests to a cluster of read-only replica databases rather than to the master database.

**Note**: The simplest and most cost effective way to increase transaction throughput is to scale the database *vertically* and add additional Cortex instances. Horizontal database scaling should be considered only after you have reached the limits of vertical database scalability.

For more information, see the following resources:

* For upgrade instructions, see the Elastic Path Commerce version 7.6 [Release Notes](https://documentation.elasticpath.com/commerce/docs/release-notes.html).
* For a description of horizontal database scaling and implementation instructions, see the Elastic Path Commerce [Deployment](https://documentation.elasticpath.com/commerce/docs/deployment/index.html) documentation.

### Change Log

#### Docker

* `CLOUD-1426` - Configure the Search master and slave Java Virtual Machine (JVM) heap size independently using the `EP_CONTAINER_MEM_SEARCH` and `EP_CONTAINER_MEM_SEARCH_SLAVE` environment files.
* `CLOUD-1617` - The Batch server no longer waits for a Search server to be ready to start
* `CLOUD-1589` - Migrate Docker base image from CentOS 7 to Amazon Linux 2
* `CLOUD-1552` - Added support for horizontal database scaling

#### CloudCore

* `CLOUD-1643` - Added the new `ProductionSecurityGroups` Jenkins job to secure public, private, and bastion [EC2 Security Groups](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html)
* `CLOUD-1649` - Version locked the Jenkins plugins

#### CloudTeam

* `CLOUD-1578` - Refined the CI pipeline by running the Core I-Tests and JUnit Tests in parallel, renaming the job `RunOtherCommerceTests` to `RunSelectedCommerceTests` and changing the job to execute tests based on a job parameter, adding Junit test reports to the test steps
* `CLOUD-1581` - Upgraded Maven from 3.5.2 to 3.6.2 and set Maven remote repository connection retry to 10

#### CloudDeploy

* `CLOUD-1617` - The Live Batch server crashing no longer causes the Live Search Master server to restart
* `CLOUD-1678` - Increased the maximum number of instances that can be launched in the Cortex ECS cluster from six to 12
* `CLOUD-1552` - Added option to deploy an Author and Live environment with horizontal database scaling

### Bug Fixes

* `CLOUD-1524` - Fixed known issue with parallel runs of the Jenkins job `BuildEPImage`

### Known Issues

* `CLOUD-1564` - All Jenkins jobs named `bootstrap` ignore the branch parameter
