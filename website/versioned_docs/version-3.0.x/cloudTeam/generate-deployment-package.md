---
id: version-3.0.x-generate-deployment-package
title: Generate Deployment Package Using CloudTeam
sidebar_label: Generate Deployment Package
original_id: generate-deployment-package
---

## Generating Deployment Package

If you update Elastic Path Commerce source code and want to deploy a new environment or update an existing environment, you must generate a deployment package first.

### Creating a Maven Image

CloudTeam uses a Maven Docker container to build deployment packages in a clean reproducible environment. Before you build the first deployment package, you must create a Maven Docker image by running the `BuildMavenImage` job. `BuildMavenImage` must be already run once as part of `BuildPipeline` when CloudTeam was initialized.

### Creating a Deployment Package

Once the Maven Docker image is built, you can run the `BuildDeploymentPackage` job. This builds a deployment package and pushes it to Amazon S3 where other jobs can use it to build the Docker images, setup environment configuration, create RDS database snapshots, or update existing RDS databases. When this job is run, it updates the Consul setting for `team/ep/deployment-package/uri` in Consul. The downstream jobs that use this parameter use the last built deployment package by default. When you create a CI pipeline, this might affect concurrent builds.

### Next Steps

For information on how to deploy a new environment using a deployment package, see [CloudDeploy](../cloudDeploy/index.md). To learn about updating an existing Author and Live environment using a deployment package, see [Updating an Author and Live Environment](../cloudDeploy/deployment/deploy-auth-live.md#updating-author-and-live-environment).

## Jenkins Jobs

The CloudTeam bootstrap container runs the Jenkins jobs to set up a Docker container with Maven and to generate an Elastic Path Commerce deployment package after completing CloudTeam initialization. The container runs the following jobs:

- `BuildPipeline`
- `BuildMavenImage`
- `BuildDeploymentPackage`

Before running the job, the container also:

- Uploads the Jenkins jobs to the Jenkins instance
- Sets the default parameters for these jobs in consul
- Creates a Nexus instance
- Creates an ECR (Elastic Container Registry) repository for the Maven containers

### `BuildMavenImage` Job

This job creates a Maven Docker image. The `BuildDeploymentPackage` uses this image as an environment to run the `Build-Deployment-Package.sh` script. The required parameters for this job are:

| Parameter | Description |
|---|---|
|`CONFIG_STORE_URL` | The complete URL of the Config Store created in CloudCore, including `http://`. |
|`EP_MVN_IMAGE_TAG` | The tag name to use on the image when pushed into their repository. The default tag name is, `EP-CloudTeam-<VERSION>`. |
|`EP_ORACLE_JDK_DOWNLOAD_URL` | A licensed Oracle JDK (Java Development Kit) download URL.|
|`EP_ORACLE_JDK_FOLDER_NAME`| The name of the folder where Oracle JDK is downloaded. |
|`EP_MAVEN_FOLDER_NAME`| The name of the folder where Maven binary is downloaded. |
|`CLOUDTEAM_GIT_REPO_URL`| The URL of the CloudTeam git repository. |
| `CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |

### `BuildDeploymentPackage` Job

The `BuildDeploymentPackage` job builds a deployment package using the source code from the Elastic Path Commerce repository, and then uploads it to Amazon S3. You can use this deployment package for the Docker Image builder and RDS (Relational Database Service) Snapshot jobs to build the Docker images and RDS snapshots, which are used to deploy environments. This job requires the following parameters:

| Parameter | Description |
|---|---|
|`CONFIG_STORE_URL` | The complete URL of the Config Store created in CloudCore, including `http://`. |
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |
|`EP_MVN_IMAGE_TAG` | The tag name of the Maven image to use when creating the Maven container. |
|`EP_COMMERCE-GIT_REPO_URL`| The URL of the EP-Commerce repository where the source code distribution is available. |
|`EP_COMMERCE_GIT_REPO_BRANCH`| The branch in the EP-Commerce repository that you want to use.|
|`NEXUS_REPO_BASE_URI`| The URL of the Nexus repository to be created in CloudTeam, including http(s)://. |
|`NEXUS_REPO_USER_NAME`| The user name for the Nexus repository to be created in CloudTeam. |
|`NEXUS_REPO_PASSWORD`| The  password for the Nexus repository to be created in CloudTeam. |
|`CLOUDTEAM_GIT_REPO_URL`| The URL of the CloudTeam git repository. |
|`DEPLOYMENT_PACKAGE_NAME`| The name of the deployment package that is uploaded to Amazon S3, including .zip. |

### `BuildPipeline` Job

`BuildPipeline` job runs the `BuildMavenImage` and the `BuildDeploymentPackage` jobs.
