---
id: version-3.0.x-release-notes
title: Release Notes
original_id: release-notes
---

## `3.0.2`

### CloudCore

* `CLOUD-800` - Upgraded Jenkins to the latest LTS (Long Term Support) release, 2.138.2

#### Bug Fixes

* `CLOUD-799` - Fixed issue blocking Jenkins from provisioning worker nodes
    * This issue was introduced by a breaking change in version 1.41 of the [Amazon EC2 Jenkins plugin](https://plugins.jenkins.io/ec2)

## `3.0.1`

### CloudCore & CloudDeploy

#### Bug Fixes

* `CLOUD-526` - Fixed CloudDeploy Jenkins jobs failing when initializing CloudOps in the us-east-1 AWS (Amazon Web Services) region

### Docker

#### Bug Fixes

* `CLOUD-1032` - Fixed the Integration server to correctly set Data Sync Tool configuration

## `3.0.0`

### Release Highlights

* This release introduces CloudTeam, giving *CloudOps for AWS* the ability to take
Elastic Path Commerce source code and build the deployment package in a Jenkins job
* *CloudOps for AWS* has been overhauled and now consists of three components:
    * CloudCore - creates the base infrastructure the other CloudOps components rely on
    * CloudTeam - (mentioned above)
    * CloudDeploy - deploys and updates environments using the deployment package prepared by CloudTeam
* Documentation on the CloudOps components has moved to its own repository called cloud-docs-aws

### Known Issues

* The Jenkins job `SyncS3GitRepo` updates all branches in the Amazon S3 bucket to the branch that you use when deploying CloudDeploy. This job should instead update each branch in S3 to its corresponding branch from the Git repository hosting service

### Change log

* **CloudCore**:
    * Improved network infrastructure by using a VPC (Virtual Private Cloud) with public and private subnets, and a NAT (Network Address Translation) gateway
    * Improved the capacity of the Jenkins server with Jenkins slaves
    * Added a configuration management server to store Jenkins job parameters and deployment parameters
    * Added a Bastion server for all SSH-type access to deployed instances
* **CloudTeam**:
    * Added Continuous Integration jobs to Jenkins to build deployment packages from Elastic Path Commerce source code
    * Added a Nexus server to mirror the public Elastic Path Maven repository
    * Added an ECR (Elastic Container Registry) repository for Docker images created by CloudTeam
    * Included support for Elastic Path Commerce 7.3
* **CloudDeploy**:
    * Removed obsolete ECR repository
    * Cleaned up CloudOps consumption of EP env config and properties
    * Improved Author and Live architecture to use library-style structure
    * Included support for Elastic Path Commerce 7.3
* **Bug Fixes**:
    * `CLOUD-472` - Fixed Amazon EFS (Elastic File System) mounting to automatically remount on instance reboot
    * `CLOUD-535` - Fixed *CloudOps for AWS* to deploy into any region
    * `CLOUD-550` - Improved clean up script and separated it into the CloudOps components

### Upgrade Notes

#### Changed handling of commerce configuration

To support Elastic Path Commerce 7.3 and its changes to configuration, we have cleaned up how CloudOps consumes config files and removed the zip files that lived in the Config directory to be built into the Docker images. When upgrading make sure that the Docker version you use is compatible with *CloudOps for AWS* 3.0.

#### Converted *CloudOps for AWS* into components

With CloudOps separated into components it will be difficult to diff this release to previous releases. To upgrade *CloudOps for AWS* we recommend initializing the new version in a new AWS account and disabling the old version once a deployment of Elastic Path Commerce is ready.

For help on upgrading to *CloudOps for AWS* 3.0, refer to the documentation on [Upgrading CloudOps](workflow-overview.md#upgrading-cloudops).
