---
id: version-3.0.x-configurations
title: CloudDeploy Configurations
sidebar_label: CloudDeploy Configurations
original_id: configurations
---

## CloudOps Default Configuration Sets

CloudOps uses two sets of default configurations. Elastic Path Commerce sample Data Population configuration for creating RDS (Relational Database Service) snapshots and CloudOps environment files for managing environment specific configuration such as Java memory settings. Both configurations are stored in the **Config** directory in the *CloudOps for AWS* (Amazon Web Services) source code distribution.

The Jenkins jobs are configured to use these configuration sets by default when creating RDS snapshots or deploying environments.  If you want to make any changes to an environment, you must only change the default `.env` files. However, you must not modify the Elastic Path Commerce sample Data Population configuration. Instead, Elastic Path recommends adding new environments to the Elastic Path Commerce repository. For information on using a custom Elastic Path Commerce environment with CloudOps, see [Changing Environment Name](#changing-environment-names) below.

### Unsupported Environment Files

The default environment files that *CloudOps for AWS* uses are `author.env`, `live.env`, and `single-instance-mysql-container.env`. The `cloudops-env-files` directory consists of additional single instance environment files. These files are used within Elastic Path for testing Elastic Path Commerce database compatibility. These files are included as examples, but are unsupported. Example Layout:

```text
Config
├── cloudops-env-files
│   ├── author.env
│   ├── live.env
│   ├── single-instance-aurora-rds.env
│   ├── single-instance-mssql-rds.env
│   ├── single-instance-mysql-container.env
│   ├── single-instance-mysql-rds.env
│   └── single-instance-oracle-rds.env
└── ep-sample-datapopulation
    ├── authoring
    │   ├── data-population.properties
    │   ├── database.properties
    │   └── filtering.properties
    └── live
        ├── data-population.properties
        ├── database.properties
        └── filtering.properties

```

## Changing Environment Names

The names of the Elastic Path Commerce environments are set to the default names that work with the existing Jenkins jobs and CloudFormation templates. However, you can set Elastic Path Commerce environment names to arbitrary values if the deployment Jenkins jobs and CloudFormation templates are updated as required.

### Procedure

1. Build a new deployment package by doing the following:
    - Add the new environment to the environments folder in Elastic Path Commerce source
    - Run the `BuildDeploymentPackage` job

2. Build EP images from the new deployment package by running the `BuildEPImage` job.

3. Build a MySql image from the new deployment package by running the `BuildMySQL` job (required for deploying a Single Instance environment).

4. Create RDS snapshots populated with data from the new Elastic Path Commerce environment. Run the following Jenkins jobs with the parameter `TARGET_ENVIRONMENT` set to the name of the new Elastic Path Commerce environment (required for deploying an Author Live environment):
    - `CreateRdsSnapshotAuthor`
    - `CreateRdsSnapshotLive`

5. Deploy an environment by running either of the following Jenkins Jobs
    - `DeployAuthorAndLive`: Deploys an Authoring and Live environment
        - Set the `AUTHORING_EP_COMMERCE_ENVNAME` and `LIVE_EP_COMMERCE_ENVNAME` to use your new environment names
    - `DeploySingleInstanceDevEnvironment`: Deploys a Single Instance environment
        - Set the `EP_COMMERCE_ENVNAME` to use your new environment name

## Updating or Replacing Properties Files

You must do the following to update a CloudOps environment if you change configurations in the *Elastic Path Commerce* repository:

1. Build a new Elastic Path Commerce deployment package using the `BuildDeploymentPackage` job
2. Rebuild the Elastic Path container images using the new deployment package and the respective Jenkins jobs
3. Run the environment update Jenkins jobs, `UpdateEnvironment_1_StageUpdates` and `UpdateEnvironment_2_ApplyUpdates`, to perform an ECS (Elastic Container Services) redeployment. For more information, see [Updating an Author and Live Environment](deployment/deploy-auth-live.md#updating-author-and-live-environment)

