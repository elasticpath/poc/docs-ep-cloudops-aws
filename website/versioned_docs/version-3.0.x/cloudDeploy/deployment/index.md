---
id: version-3.0.x-index
title: Overview of Deploying Elastic Path Commerce
sidebar_label: Deployment Overview
original_id: index
---

After initializing CloudDeploy, the new jobs in the Jenkins server created by CloudCore enables using the functionalities of CloudDeploy. With these jobs, you can build Docker images. These images are required for deploying Elastic Path Commerce and deploy Elastic Path Commerce in the single instance development environment and an Author and Live environment. You can create a production-like environment with the Author and Live environment. You can view these jobs in the Jenkins job folder *CloudOps*.

> **Note:** Before using any CloudOps component, ensure that you are familiar with [Jenkins](https://jenkins.io/). For more information on knowledge requirements for using *Elastic Path CloudOps for AWS* (Amazon Web Services), see [CloudOps Requirements](../../requirements.md).

For information on how to access the Jenkins server, see the Jenkins Server section in  [Validating CloudCore Initialization](../../cloudCore/initialization.md#validating-initialization).

## Jenkins Jobs

You can categorize the CloudDeploy Jenkins jobs into the following categories:

| Job Category | Purpose     |
| ------------ | ----------- |
| Docker Image Builds | Builds Docker images required for deploying Elastic Path Commerce. These images include a base image with CentOS, a Java JRE (Java Runtime Environment) and Tomcat installed, images for each Elastic Path web application, a MySQL database image populated with our test data, and an ActiveMQ image for development purposes. |
| RDS Snapshot Creation | Creates pre-populated RDS (Relational Database Service) snapshots required for Author and Live deployments. |
| Elastic Path Commerce Deployments | Deploys single instance environments for development and test purposes, and pre-production Author and Live environments for a full-fledged production-like environment. |
| Environment Updates | Updates Docker images and databases on existing Author and Live deployments. For more information, see [Updating an Author and Live Environment](../deployment/deploy-auth-live.md#updating-author-and-live-environment) for more details.|
| Initialization | Builds initial Docker images and RDS snapshots required for deployment. |
| Maintenance | Keeps the resources, such as AMIs and the Amazon S3 bucket for CloudOps, up-to-date. |

## Docker Image Builds

The following jobs build the Docker images needed to deploy Elastic Path Commerce:

- [BuildActiveMQ](#buildactivemq)
- [BuildBaseImage](#buildbaseimage)
- [BuildEPImage](#buildepimage)
- [BuildMySQL](#buildmysql)

These jobs use the *Docker* Elastic Path repository to build Docker images. After the images are successfully built, the Jenkins jobs push the images to their respective repositories in ECR (Elastic Container Registry).

For more information on how to see the parameter values that a job was run with, see the [How to see the parameter values a Jenkins jobs is run with?](../../faq/index.md#how-to-see-the-parameter-values-a-jenkins-job-is-run-with) section.

### BuildActiveMQ

This job builds the ActiveMQ Docker image used in the single instance development environment.

| Parameter |Description |
|---|---|
| `EP_ACTIVEMQ_VERSION` | The version of ActiveMQ that the job uses to build the Docker image. |
| `ACTIVEMQ_IMAGE_TAG` | The name the job tags the image in ECR. |
| `DOCKER_BRANCH` | The branch and release version of the *Docker* Elastic Path repository to use for building the image. |
| `DOCKER_GIT_REPO_URL` | The URL to the Docker Elastic Path repository. |
| `CONSUL_URL` | The URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store the default values for some parameters. |

### BuildBaseImage

This job builds the base Docker image that the *BuildEpImage* job uses to build the Elastic Path application Docker images.

This job first builds a CentOS image that has a Java JRE installed. The job uses this image to build the final base image, which has Tomcat installed.

| Parameter |Description  |
|---|---|
| `EP_TOMCAT_VERSION` | The version of Tomcat the job uses to build the Docker image. |
| `EP_TOMCAT_MAJOR_VERSION` | The major version number of `EP_TOMCAT_VERSION`. |
| `EP_JAVA_DOWNLOAD_URL` | A link to download Java JRE. |
| `EP_IMAGE_TAG` | This parameter is deprecated and not used. |
| `DOCKER_BRANCH` | The branch and release version of the Docker Elastic Path repository to use for building the image. |
| `DOCKER_GIT_REPO_URL` | The URL to the Docker Elastic Path repository. |
| `CONSUL_URL` | The URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store the default values for some parameters. |

### BuildEPImage

This job builds the Elastic Path Commerce web app Docker images such as:

    - Cortex server
    - Integration server
    - Commerce Manager server
    - Search server
    - Batch server

| Parameter |  Description |
| --------- | ------------ |
| `DEPLOYMENT_PACKAGE_S3_URI` | The S3 location of the Elastic Path deployment package zip file that the job uses when building the Docker images. The deployment package contains the compiled Java code from all of the Elastic Path applications, along with other important files. |
| `EP_IMAGE_BUILDER_CONFIG` | The name of the config file the build script uses when building the Elastic Path images. The config file specifies which Elastic Path web apps to build and what Dockerfile template to use when building them. |
| `EP_IMAGE_TAG` | The name the job tags build images in ECR. |
| `DOCKER_BRANCH` | The branch and release version of the Docker Elastic Path repository to use for building the images. |
| `DOCKER_GIT_REPO_URL`| The URL to the Docker Elastic Path repository. |
| `CONSUL_URL`| The URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store default values for some parameters. |

### BuildMySQL

This job builds the MySQL Docker image used in the single instance development environment.

| Parameter  |  Description|
|---|---|
| `DEPLOYMENT_PACKAGE_S3_URI` | The S3 location of the Elastic Path deployment package zip file that the job uses when building the Docker image. The deployment package contains the Data Population tool which the build script uses to initialize the database. |
| `MYSQL_IMAGE_TAG` | The name the job tags the image in ECR. |
| `DOCKER_BRANCH` | The branch and release version of the Docker Elastic Path repository to use for building the image. |
| `DOCKER_GIT_REPO_URL` | The URL to the Docker Elastic Path repository. |
| `CONSUL_URL` | The URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store the default values for some parameters. |


## Elastic Path Commerce Deployments

The following jobs support deploying and updating Elastic Path Commerce environments:

- [CreateEcsAmi](#createecsami)
- [SyncS3GitRepo](#syncs3gitrepo)
- [DeploySingleInstanceDevEnvironment](#deploysingleinstancedevenvironment)
- [CreateRdsSnapshotAuthor](#createrdssnapshotauthor)
- [CreateRdsSnapshotLive](#createrdssnapshotlive)
- [DeployAuthorAndLive](#deployauthorandlive)
- [UpdateEnvironment_1_StageUpdates](#updateenvironment_1_stageupdates)
- [UpdateEnvironment_2_ApplyUpdates](#updateenvironment_2_applyupdates)

For more information on how to see the parameters that a job is run with, see [How to see the parameter values a Jenkins jobs is run with?](../../faq/index.md#how-to-see-the-parameter-values-a-jenkins-job-is-run-with).

### CreateEcsAmi

This job creates an Elastic Path customized version of the AWS ECS (Elastic Container Service) optimized AMI (Amazon Machine Image).

| Parameter  | Description |
|---|---|
| `STACK_NAME` | The name of the stack to create in CloudFormation. |
| `CLOUDDEPLOY_BRANCH` | The CloudDeploy branch and release version folder in the Amazon S3 bucket from which the job gets CloudFormation templates. |
| `OPT_ECS_AMI_NAME` | The name of the ECS-optimized AMI that AWS publishes. This AMI is used as a base for the new customized AMI. |
|`KEY_NAME`| The name of the SSH key to use when launching any EC2 instances. |
|`INSTANCE_TYPE`| The EC2 instance type to use when launching any EC2 instances.  |
|`NETWORK_STACK`| The name of the CloudOps network CloudFormation stack, which is used to source networking information, such as subnets and security groups.|
|`EC2_NAME`| The name of the EC2 that the CloudFormation stack, `STACK_NAME`, creates. |
|`ECS_OPT_AMI_NAME`| The name of the new ECS-optimized AMI to be created. |
| `CONSUL_URL` | The URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store default values for some parameters. |

### SyncS3GitRepo

The `SyncS3GitRepo` job syncs CloudDeploy files in the Amazon S3 bucket for CloudOps with the Git repository that is specified in the job parameter.

The files updated include CloudFormation templates, scripts, and other important files related to building and deploying with CloudDeploy.

| Parameter |Description  |
|---|---|
|`CLOUDDEPLOY_GIT_REPO_URL`  | The Git repository from which the Amazon S3 resources are updated.|
|`CLOUDDEPLOY_BRANCH` | The CloudDeploy branch and release version with which to update the Amazon S3 resources.|
| `CONSUL_URL` | The URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store the default values for some parameters. |

### DeploySingleInstanceDevEnvironment

This job deploys a single instance development environment. For more information on the single instance environment, see [Deploying Single Instance Environment](deploy-single-instance.md).

| Parameter |Description  |
|---|---|
| `STACK_NAME` | Specifies the name of the stack to create in CloudFormation. |
| `ECS_AMI_ID` | Specifies the ID of the ECS-optimized AMI to use when this job launches any EC2 instances that run containers. |
| `CLOUDDEPLOY_BRANCH` | The CloudDeploy branch and release version folder in Amazon S3 from which the job gets CloudFormation templates. |
| `EP_IMAGE_TAG` | Specifies the Docker image to use for all Elastic Path Commerce related containers when deploying the single instance environment. |
|`MYSQL_IMAGE_TAG`   | Specifies the Docker image to use for the MySQL container in the single instance environment.  |
|`ACTIVEMQ_IMAGE_TAG`   | Specifies the Docker image to use for the ActiveMQ container in the single instance environment. |
|`KEY_NAME`   | Specifies the name of the SSH key to use for any launched EC2 instances.  |
| `NETWORK_STACK` | Specifies the name of the CloudOps network CloudFormation stack, which is used to source networking information, such as subnets and security groups. |
| `ROUTE_53_STACK_NAME` | Specifies the name of the CloudOps Route53 related CloudFormation stack. This stack is used to source DNS information like Hosted Zone IDs. |
|`EcsSubnetOne`   | Specifies the subnet in which you want to deploy ECS infrastructure.  |
|`EcsSubnetTwo`   | Specifies an additional subnet in which you want to deploy ECS infrastructure. |
|`LoadBalancerSubnetOne`   | Specifies a subnet in which you want to deploy load-balancers.  |
|`LoadBalancerSubnetTwo`   | Specifies an additional subnet in which you want to deploy load-balancers.  |
|`EP_CERTIFICATE_ARN`   | Specifies the ARN (Amazon Resource Name) for the HTTPS certificate in AWS Certificate Manager used to initialize CloudCore. This setting is optional, but the single instance environment does not use SSL if this parameter is not set. |
|`DNS_NAME`   | Specifies the DNS name to point to Cortex.  |
|`ENV`   | Adds this prefix to resources, this parameter is used as a prefix to create unique names for resources.  |
|`EnableUITests`   |  Sets the `org.eclipse.rap.rwt.enableUITests` property in the Elastic Path application containers. |
|`LOAD_BALANCER_SCHEME`   | Specifies whether to make the load-balancer pointing to Cortex internet-facing or internal.  |
|`DATABASE_TYPE` | Specifies the type of database the environment uses. |
|`EP_CLOUDOPS_ENVNAME` | Specifies the CloudOps environment configuration file to use when deploying the environment. For more info, see [CloudOps Default Configuration Sets](../configurations.md#cloudops-default-configuration-sets). |
|`EP_COMMERCE_ENVNAME` | Specifies the application configuration file in the Elastic Path Commerce deployment package to use when deploying the environment. For more info, see [Application Configuration Files](commerce-configurations.md#application-configuration-files). |
| `CONSUL_URL` | Specifies the URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store the default values for some parameters. |
|`CLOUDDEPLOY_GIT_REPO_URL`  | The Git repository from which the Amazon S3 resources are updated.|

### CreateRdsSnapshotAuthor

This job creates an RDS snapshot to use when deploying an Author and Live environment. This job creates the snapshot used by the Author environment and the *CreateRdsSnapshotLive* job creates the snapshot used by the Live environment.

To create the snapshot, an EC2 instance and an RDS instance are launched. The EC2 instance runs the Data Population tool against the RDS instance. After a snapshot of the RDS instance is taken, both the EC2 and RDS instances are torn down.

| Parameter |Description  |
|---|---|
|`AMAZON_LINUX_AMI_ID` | The ID of the AMI to use when launching the EC2 instance that populates database data.|
|`KEY_NAME`  | The name of the SSH key to use for any launched EC2 instances. |
|`RDS_INSTANCE_TYPE`  | The size of the RDS instance with which you want to create the RDS snapshot.  |
|`AUTHOR_RDS_SNAPSHOT_ID`| The ID to assign to the new Author snapshot.|
| `STACK_NAME` | The name of the stack to create in CloudFormation. |
|`TARGET_ENVIRONMENT`| The target environment for the database that this job creates. The database deployment configuration is located in a directory with the same name.|
| `CLOUDDEPLOY_BRANCH` | The CloudDeploy branch and release version folder in Amazon S3 from which the job gets CloudFormation templates. |
|`ENGINE`| The database engine to use for RDS snapshot that the job creates.|
|`EP_DB_STORAGE`| The size of the new RDS snapshot in GB.|
|`EP_DB_IOPS`| The number of IOPS for the new RDS snapshot.|
|`EP_DB_MASTER_PASSWORD`| The password of the database root user.|
|`EP_DB_MASTER_USER`| The username of the database root user.|
|`DEPLOYMENT_PACKAGE_S3_URI`|The Amazon S3 URI to the deployment package containing the correct version of the Data Population tool.|
| `NETWORK_STACK` | The name of the CloudOps network CloudFormation stack, which is used to source networking information, such as subnets and security groups. |
|`EP_RDS_VERSION`|The RDS engine version for the new RDS snapshot.|
|`EP_RDS_PARAMETER_GROUP`|The AWS database parameter group to use when creating the new RDS snapshot.|
|`DB_SUBNET_GROUP_ID`| The AWS subnet group to use when creating the new RDS snapshot.|
| `CONSUL_URL` | The URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store the default values for some parameters. |

### CreateRdsSnapshotLive

This job creates an RDS snapshot to use when deploying an Author and Live environment. This job creates the snapshot used by the Live environment and the *CreateRdsSnapshotAuthor* creates the snapshot used by the Author environment.

For the parameter details, see the [`CreateRdsSnapshotAuthor`](#createrdssnapshotauthor) job.

### DeployAuthorAndLive

This job deploys an Author and Live environment. For more information on deploying Author and Live environments, see the [Deploying Authoring and Live Environments](deploy-auth-live.md) page.

| Parameter | Description |
|---|---|
| `STACK_NAME` | The name of the stack to create in CloudFormation. |
| `ACTIVEMQ` | This parameter is obsolete, you need not set this value. |
|`AMAZON_LINUX_AMI_ID`  | The ID of the AMI that want to use when you launch any EC2 instance that does not run any container. |
| `ECS_AMI_ID` | The ID of the ECS-optimized AMI that you want to use when this job launches any EC2 instance that runs containers. |
| `CLOUDDEPLOY_BRANCH` | The CloudDeploy branch and release version folder in Amazon S3 from which the job gets CloudFormation templates. |
|`AUTHOR_RDS_SNAPSHOT_ID`|The RDS snapshot ID to use for the database in Author environment.|
|`EPRDS`|The size of the RDS instances to create for both Author and Live Elastic Path databases.|
| `EP_IMAGE_TAG` |The Docker image to use for all Elastic Path Commerce related containers when you deploy the Author and Live environment. |
|`KEY_NAME`|The name of the SSH key to use for any launched EC2 instances.|
|`LIVE_RDS_SNAPSHOT_ID`|The RDS snapshot ID to use for the database in Live environment.|
|`OFFICE_IP_ADDRESS`|The public IP address of the office to add to the public security group ingress rules.|
| `NETWORK_STACK` | The name of the CloudOps network CloudFormation stack, which is used to source networking information, such as subnets and security groups. |
| `ROUTE_53_STACK_NAME` | The name of the CloudOps Route53 related CloudFormation stack. This stack is used to source DNS information, such as Hosted Zone IDs. |
|`EP_CERTIFICATE_ARN`|The ARN for the HTTPS certificate in AWS Certificate Manager used to initialize CloudCore. This setting is optional, but note that the Author and Live environment does not use SSL if this field is left blank.|
|`EP_ACTIVEMQ_VERSION`|The version of ActiveMQ to be used.|
|`EP_RDS_VERSION`|The RDS engine version for both Author and Live Elastic Path databases. |
|`EP_RDS_PARAMETER_GROUP`|The AWS database parameter group for both Author and Live Elastic Path databases.|
|`EnableUITests`|This parameter sets the `org.eclipse.rap.rwt.enableUITests` property in all Elastic Path application containers. |
| `CONSUL_URL` | The URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store the default values for some parameters. |
|`ENV`| This parameter is used to make each Author and Live CloudFormation stack name unique.|
|`LAMBDA_STACK_NAME` | The name of the CloudOps Lambda CloudFormation stack, which is the Lambda Function that puts the Author and Live RDS endpoints into the config store. |
|`CLOUDDEPLOY_GIT_REPO_URL`  | The Git repository from which the Amazon S3 resources are updated.|
|`AUTHORING_EP_COMMERCE_ENVNAME` | Specifies the application configuration file in the Elastic Path Commerce deployment package to use when deploying the Authoring environment. For more info, see [Application Configuration Files](commerce-configurations.md#application-configuration-files). |
|`LIVE_EP_COMMERCE_ENVNAME` | Specifies the application configuration file in the Elastic Path Commerce deployment package to use when deploying the Live environment. For more info, see [Application Configuration Files](commerce-configurations.md#application-configuration-files). |
|`AUTHORING_EP_CLOUDOPS_ENVNAME` | Specifies the CloudOps Authoring environment configuration file to use in the Authoring environment. For more info, see [CloudOps Default Configuration Sets](../configurations.md#cloudops-default-configuration-sets). |
|`LIVE_EP_CLOUDOPS_ENVNAME`| Specifies the CloudOps Live environment configuration file to use in the Live environment. For more info, see [CloudOps Default Configuration Sets](../configurations.md#cloudops-default-configuration-sets). |
|`CONFIG_PREFIX` | The key to store the Author and Live RDS endpoint values in consul. |

### UpdateEnvironment_1_StageUpdates

This job generates a script that you can use to update an Author and Live environment. For more information on updating an Author and Live environment, see [Updating Environment](deploy-auth-live.md#updating-author-and-live-environment).

> **Note:** This Jenkins job is to update a deployment that is not under active load. Contact [Elastic Path Support](https://community.elasticpath.com/product-support/) for more information about updating a deployment that is under active load.

| Parameter |Description  |
|---|---|
|`APPLICATIONS_STACK_NAME`|Specifies the name of the application stack that you want to update.|
|`NEW_IMAGE_TAG`  |Specifies the Docker image tag to which you want to update your stack.  |  |
|`WITH_DB_UPDATES` | Specifies that updating the environment’s database is enabled. If you enable this setting, you must also set the `RDS_STACK_NAME` and `DATA_POP_COMMAND` values.|
|`RDS_STACK_NAME`|Specifies the CloudFormation stack that creates the RDS instance that you want to update.|
|`DATA_POP_COMMAND`|Specifies the data population command to be run when updating the database. The options are `update_db` and `reset_db`|
|`STAGING_DIRECTORY_S3_URI`|Specifies the folder in Amazon S3 where you can stage the update script that this job generates.|
|`ZERO_DOWN_TIME`|Specifies that the update is run in "Zero Downtime" mode. For more information on zero downtime mode, see [Updating Environment](deploy-auth-live.md#updating-author-and-live-environment).|
|`DEPLOYMENT_PACKAGE_S3_URI`|Specifies the Amazon S3 URI to the deployment package. You can use this URI to update your database.|
|`AMAZON_LINUX_AMI_ID`|Specifies the AMI used to launch the EC2 instance that runs data population.|
|`KEY_NAME`|Specifies the name of the SSH key to use for any launched EC2 instances.|
| `CLOUDDEPLOY_BRANCH` | Specifies the CloudDeploy branch and release version folder in Amazon S3 from which the job gets CloudFormation templates and update related scripts. |
|`CLOUDDEPLOY_GIT_REPO_URL`|Specifies the URL to the CloudDeploy repository to use when sourcing the update related scripts.|
| `CONSUL_URL` | Specifies the URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store the default values for some parameters. |

### UpdateEnvironment_2_ApplyUpdates

This job runs the update script created by the UpdateEnvironment_1_StageUpdates job. For more information on updating an Author and Live environment, see [Updating Environment](deploy-auth-live.md#updating-author-and-live-environment).

> **Note:** This Jenkins job is to update a deployment that is not under active load. Contact [Elastic Path Support](https://community.elasticpath.com/product-support/) for more information about updating a deployment that is under active load.

| Parameter | Description |
|---|---|
|`STAGING_DIRECTORY_S3_URI` | The same Amazon S3 folder used in the `UpdateEnvironment_1_StageUpdates` job to stage the update script. |
| `CONSUL_URL` | The URL to the Consul server that CloudCore creates. Jenkins jobs use Consul to store the default values for some parameters. |
|`STAGING_FILE` | The name of the update script located in the folder specified by `STAGING_DIRECTORY_S3_URI`. |

## Related Topics

- [CloudDeploy Configurations](../configurations.md)
