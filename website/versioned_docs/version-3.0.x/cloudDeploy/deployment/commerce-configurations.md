---
id: version-3.0.x-commerce-configurations
title: Deployment and Elastic Path Commerce Configurations
sidebar_label: Commerce Configurations
original_id: commerce-configurations
---

## Elastic Path Commerce Configuration

### Application Configuration Files

Application configuration files from the deployment package and the **EpImageBuilder/config/ep-sample-config/** directory of the *Elastic Path* Docker repository are copied into the `/ep/environments` directory of the Elastic Path Docker images at build time. An *Elastic Path Commerce* environment is selected at run time by the `EP_COMMERCE_ENVNAME` environment variable, and it’s configuration is loaded into the **/ep/conf/** directory of the Docker container.

### Selecting a Elastic Path Commerce Environments

Do the following to select the  *Elastic Path Commerce* environment that you want to deploy:

- For Single instance environment:
    - Set the `EP_COMMERCE_ENVNAME` parameter in the [`DeploySingleInstanceDevEnvironment` job](../deployment/index.md#deploysingleinstancedevenvironment) when deploying
- For an Author-Live environment:
    - Set the `AUTHORING_EP_COMMERCE_ENVNAME`, and `LIVE_EP_COMMERCE_ENVNAME` parameters in the [`DeployAuthorAndLive` job](../deployment/index.md#deployauthorandlive) when deploying

## Tomcat Context Configuration

The Tomcat context configuration is partially set by the Tomcat initialization script `setenv.sh`. *CloudOps* uses a custom `setenv.sh` script, which is stored in the *Elastic Path* Docker repository, for Tomcat context configuration. The configuration values used by the Tomcat context are used for several different activities, such as connecting to databases, connecting to ActiveMQ, and defining JVM (Java Virtual Machine) memory settings.

The `setenv.sh` script depends on certain environment variables, `env vars`, to be set. In *CloudOps*, these `env vars` are set using the [Docker entry point scripts](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#entrypoint). The entry point scripts pull values from the [Consul config store](../../architecture/build-op-env.md#configuration-store) created by CloudCore and set the required `env vars`.

When deploying an *Elastic Path Commerce* environment using the deployment jobs created by *CloudDeploy*, you can provide the `EP_CLOUDOPS_ENVNAME` parameter to specify which `.env` to use. An Author and Live deployment requires two parameters to be set, `AUTHORING_EP_CLOUDOPS_ENVNAME` and `LIVE_EP_CLOUDOPS_ENVNAME`). The jobs finds the corresponding `.env` from the **Config/cloudops-env-files/** directory in the *CloudDeploy* repository that is cloned at the beginning of the job. The job then uploads the `.env` file’s contents to the [Consul config store](../../architecture/build-op-env.md#configuration-store) using the `deploy/ep/config/v1/envs/${StackName}/{EP_CLOUDOPS_ENVNAME}/` prefix.

By default, the deployment jobs use the default `.env` files uploaded during the *CloudDeploy* initialization. You can upload new or custom `.env` files. Elastic Path recommends modifying the default `.env` files instead of using custom `.env` files.  If you use custom `.env` files, you must modify the CloudFormation templates for your deployment type.

### Excerpts from a sample default '.env’ file

```bash
EP_DB_PORT=3306
EP_DB_NAME=ElasticPathDB
EP_DB_PASS=epDbPassword
EP_DB_USER=epDbUser

...

EP_JMS_TYPE=org.apache.activemq.pool.PooledConnectionFactory

...

EP_CONTAINER_MEM_CORTEX=2048

...

EP_TESTS_ENABLE_UI=false
```

### Author and Live Environment

The following files contain the default Tomcat context configuration `env vars` for the Author and Live deployment:

- `live.env`
- `author.env`

### Single Instance Development Environment

The `single-instance-mysql-container.env` file contains the default Tomcat context configuration for the single instance development deployment.

The `cloudops-env-files` directory consists of additional single instance environment files. These files are used within Elastic Path for testing Elastic Path Commerce database compatibility. These files are included as examples, but are unsupported.

- `single-instance-mysql-rds.env`
- `single-instance-oracle-rds.env`
- `single-instance-mssql-rds.env`
- `single-instance-aurora-rds.env`

## Data Population Configuration

### Overview

The deployment package provided to CloudDeploy contains the *Data Population Tool* and an ext-data jar. The ext-data jar contains folders with Elastic Path Commerce environment specific configuration. CloudDeploy has two additional environment folders containing data population environment files for authoring and live environments. After running the bootstrap script `runBootstrap.sh`, these environment folders are uploaded to the Amazon S3 bucket `ep-cloudops-<account_id>/CloudDeploy/<branch>/Config/ep-sample-datapopulation`.

The data population step is run as a part of the `CreateRdsSnapshotLive` and `CreateRdsSnapshotAuthor` Jenkins jobs. The jobs combine the environment folders in `ep-sample-datapopulation` from the S3 bucket and the `ext-data-*.jar` from the external deployment package to populate the databases with data corresponding to the environment specified by the `TARGET_ENVIRONMENT` parameter. A snapshot is taken of these databases to use during the deployment.

### Data Population Customization

In order to use a different set of data population configuration files you will need to

1. Add your new environment folder to your EP Commerce repository in the `<EP-Commerce>/extensions/database/ext-data/src/main/resources/environments` directory.

2. Build the deployment package using CloudTeam’s `BuildDeploymentPackage` job or using the extensions/packager module.

3. Run the `BuildEPImage` Jenkins job using the new deployment package.

4. Run the `CreateRdsSnapshotAuthor` and `CreateRdsSnapshotLive` jobs with the desired folder name in the `TARGET_ENVIRONMENT` parameter and have unique `AUTHOR_RDS_SNAPSHOT_ID` and `LIVE_RDS_SNAPSHOT_ID` parameters.

5. Run the `DeployAuthorAndLive` job using the same `AUTHOR_RDS_SNAPSHOT_ID` and `LIVE_RDS_SNAPSHOT_ID` parameters from the previous step to create a new Author and Live environment

For more information, see [Deploying Elastic Path Commerce with Jenkins Jobs](../deployment/index.md#elastic-path-commerce-deployments).

