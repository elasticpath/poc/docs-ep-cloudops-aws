---
id: version-3.0.x-index
title: Frequently Asked Questions Using CloudOps for AWS
sidebar_label: FAQ On CloudOps for AWS
original_id: index
---

## CloudOps Initialization

### Failure In Initialization of CloudOps Component

*Elastic Path CloudOps for AWS* is implemented to run the initialization scripts only once per account. If you run into any issues, Elastic Path recommends manually deleting all AWS (Amazon Web Services) resources created by the initialization and then rerunning the initialization. This minimizes the risk of deleting important resources.

See the initialization validation documentation for the CloudOps component you want to cleanup to find which AWS resources need to be deleted:

- [Validating CloudCore Initialization](../cloudCore/initialization.md#validating-initialization)
- [Validating CloudTeam Initialization](../cloudTeam/initialization.md#validating-initialization)
- [Validating CloudDeploy Initialization](../cloudDeploy/initialization.md#validating-initialization)

You can also delete everything on your AWS account automatically using a cleanup script.

> **Disclaimer**: Elastic Path recommends deleting resources manually to avoid any mistakes, especially if non-Elastic Path resources exists in the AWS account. Use the scripts at your own risk with the assumption it may delete non-Elastic Path resources.

For generic instructions on how to cleanup CloudOps components using the cleanup scripts, see [Cleaning Up the Components](../workflow-overview.md#cleaning-up-the-components).


### How do I set up the correct public or private keys to use the bootstrap Docker containers and initialize the CloudOps components?

When initializing CloudOps components, you must have a copy of a private key that is authorized to clone from your CloudOps Git repositories.

## Configuration

### Where do I configure variables specific to my environments?

In AWS (Amazon Web Services) both single instance development environments, and Author and Live environments have their own `.env` configuration files that are stored with the CloudFormation templates. For example, the default single instance development environment configuration file is `single-instance-mysql-container.env`.

This is a simple key-value pair file that is sourced within each container, for that environment, creating environment variables that are selected by the various components, such as Tomcat’s `setenv.sh` script.

### Where are Elastic Path Assets?

Assets are also mounted on the EC2 host as follows:

```json
{
    "Name": "ep_assets",
    "Host": {
        "SourcePath": "/ep/assets"
    }
}
```

## Credentials

This section discusses where the default usernames and passwords are set.


### What is the CloudTeam Nexus Server root username and password?

The default username and password is discussed [Default Credentials](../references.md#default-credentials) page.


## Jenkins Jobs

### Parameter Values of Jenkins Jobs

For each Jenkins job, the parameter fields that are left blank are populated with default values defined in Consul. The Consul URL in the parameter description provides the default value for the parameter as in the following figure:

![BuildEPImage job with Consul URLs highlighted.](assets/version-3.0.x/cloudops-jenkins-jobs-consul-url.png)

When you troubleshoot a build and want to check the set values for the parameters, the run-time value cannot be viewed from the *Parameters* tab as these values are customized for Jenkins jobs. To view the details, check the *Environment Variables* tab, as shown the following image:

![BuildEPImage job build #1, Environment Variables tab view.](assets/version-3.0.x/cloudops-jenkins-jobs-env-vars.png)

### The BuildEpImage job failed with a device mapper issue

Occasionally, a `docker build` command fails due to an underlying `device_mapper` storage driver issue.  The options to resolve this issue are:

1. Re-run the job
2. Change the storage driver. You can uncomment the overlay2 config in the `Create-AMI.sh` script, or use the config as an example to enable it

## Logs

### How can I get Elastic Path Commerce application logs?

Elastic Path applications are configured to send logs to stdout, for 12 Factor compliance. All CloudOps deployments of Elastic Path Commerce forward the logs to [AWS CloudWatch](https://aws.amazon.com/cloudwatch/).

In the CloudWatch dashboard, select **Logs** in the left pane, to see the CloudFormation stack and the logs. Using this log, you can set up any metrics or alerts.

### Where are Docker/ECS logs?

All logs from the Docker containers in ECS (Elastic Container Services) are also piped to CloudWatch. For details on how to access the logs, see the ["How can I get Elastic Path Commerce application logs?"](#how-can-i-get-elastic-path-commerce-application-logs) section.

### How do I view logs that are not in CloudWatch?

Access the bastion server using SSH, then access one of the EC2 instances using SSH from the [bastion server](../architecture/build-op-env.md#bastion-instance).
