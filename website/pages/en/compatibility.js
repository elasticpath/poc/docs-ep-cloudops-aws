
const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;
const MarkdownBlock = CompLibrary.MarkdownBlock;

function CompatibilityMatrix(props) {
  const {config: siteConfig, language = ''} = props;
  const {baseUrl, docsUrl} = siteConfig;
  const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
  const langPart = `${language ? `${language}/` : ''}`;
  const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

  return (
    <div className="docMainWrapper wrapper">
      <Container className="mainContainer documentContainer postContainer">
        <div className="post">
          <header className="postHeader">
            <h1>Compatibility of {siteConfig.title}</h1>
          </header>
          <div id="compatibilityMatrix">
            <h2>Compatibility Matrix</h2>
            <MarkdownBlock>In the following table, the *Elastic Path Commerce* columns lists versions 6.14 throughout 7.5.1, and the rows show the versions of *Elastic Path CloudOps for AWS* and *Docker* that are compatible (&#9989;), or not (&#10060;)</MarkdownBlock>
            <table>
              <thead>
                <tr>
                  <th colSpan="2">Elastic Path Commerce</th>
                  <th>6.14</th>
                  <th>6.15</th>
                  <th>6.16</th>
                  <th>6.17</th>
                  <th>7.0</th>
                  <th>7.1</th>
                  <th>7.2</th>
                  <th>7.3</th>
                  <th>7.4</th>
                  <th>7.4.1</th>
                  <th>7.5</th>
                  <th>7.5.1</th>
                  <th>7.6</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th rowSpan="6">Elastic Path CloudOps for AWS and Docker</th>
                  <th>3.0.x</th>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                </tr>
                <tr>
                  <th>3.1.x</th>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                </tr>
                <tr>
                  <th>3.2.x</th>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                </tr>
                <tr>
                  <th>3.3.x</th>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#10060;</td>
                </tr>
                <tr>
                  <th>3.4.x</th>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#10060;</td>
                </tr>
                <tr>
                  <th>3.5.x</th>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#10060;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                  <td>&#9989;</td>
                </tr>
              </tbody>
            </table>
            <MarkdownBlock>Elastic Path CloudOps for AWS attempts to maintain compatibility with the previous two major Elastic Path Commerce releases unless there are overriding technical reasons that make this impractical.</MarkdownBlock>
          </div>
        </div>
      </Container>
    </div>
  );
}

module.exports = CompatibilityMatrix;
