
const React = require('react');
const CompLibrary = require('../../core/CompLibrary');
const Container = CompLibrary.Container;
const CWD = process.cwd();
const versions = require(`${CWD}/versions.json`);

class VersionInfo extends React.Component {
  render() {
    const {config: siteConfig} = this.props;

    const ReleaseDetails = props => (
      <tr>
        <th>{props.versionNumber}</th>
        <td><a href={`${props.docBaseUrl}/`}>Documentation</a></td>
        <td><a href={`${props.docBaseUrl}/release-notes.html`}>Release Notes</a></td>        
      </tr>
      
    );

    const latestVersion = versions[0];

    return (
      <div className="docMainWrapper wrapper">
        <Container className="mainContainer versionsContainer">
          <div className="post">
            <h1>{siteConfig.title} Versions</h1>

            <div className = "versionLists">
              <h2 className = "versionHeadline">Current version (Stable)</h2>
              <table>
                <tbody>
                  <ReleaseDetails versionNumber={versions[0]} docBaseUrl={`${siteConfig.baseUrl}${siteConfig.docsUrl}`}/>    
                </tbody>
              </table>
            </div>
            
            {!siteConfig.skipNextRelease ? 
              <div className = "versionLists">
                <h2 className = "versionHeadline">Next version</h2>
                <table>
                  <tbody>
                    <ReleaseDetails versionNumber = "next" docBaseUrl = {`${siteConfig.baseUrl}${siteConfig.docsUrl}/next`}/>
                  </tbody>
                </table>
              </div>
            : <br />}
            
            { versions.length > 1 ?
              <div className = "versionLists">
                <h2 className = "versionHeadline">Past Versions</h2>
                <table>
                  <tbody>
                    {versions.map(version =>
                      version !== latestVersion && (
                        <ReleaseDetails key = {version} versionNumber = {version} docBaseUrl = {`${siteConfig.baseUrl}${siteConfig.docsUrl}/${version}`}/>
                      )
                    )}
                  </tbody>
                </table>
              </div>
              : <br/>
            }
            
          </div>
        </Container>
      </div>
    );
  }
}

module.exports = VersionInfo;
