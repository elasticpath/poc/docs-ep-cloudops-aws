---
id: initialization
title: Initializing CloudTeam
sidebar_label: Initializing CloudTeam
---

This section provides instructions to initialize the CloudTeam component. For generic instructions on initializing CloudOps components, see [Initializing CloudOps Components](../workflow-overview.md#initializing-components).

## Prerequisites

1. Ensure that the requirements listed in the [Requirements](../requirements.md) section are met
2. Ensure that the CloudCore component is successfully initialized. For more information on initializing CloudCore, see [Initializing CloudCore](../cloudCore/initialization.md)

## Procedure

1. Clone the *CloudTeam* repository from the Git repository hosting service as described in the [Requirements](../requirements.md) section.

2. Check out the correct branch for the version of CloudTeam that you want to initialize.

3. Run the following command to copy a private SSH key authorized to clone from the Git repository hosting service to the `Containers/bootstrap/` sub-directory. The private SSH key must not be password protected:

    ```sh
    cp /path/to/${PRIVATE_GIT_KEY} path/to/cloud-team-aws/Containers/bootstrap/
    ```

4. Edit the `runBootstrap.sh` script with the required parameters. For information about the parameters, refer to comments in the script. For additional information on each `runBootstrap.sh` parameter for CloudTeam, [see `runBootstrap.sh` settings below](#runbootstrapsh-settings).

5. Run the `runBootstrap.sh` script.

    > **Tip**: Monitor the progress of the initialization in the terminal window and in the AWS (Amazon Web Services) CloudFormation web console.
    >
    > **Tip**: Save console output to file: ```bash runBootstrap.sh |& tee cloudteam-bootstrap-`date '+%Y-%m-%d-%H-%M-%S'`.txt```

6. Validate the CloudTeam initialization. For instructions, see [Validating CloudTeam Initialization](#validating-initialization)

### Initialization Details

When you initialize the CloudTeam component, it performs the following tasks:

1. Uploads required files to Amazon S3
2. Creates an ECR (Elastic Container Registry) repository for the Maven build container
3. Creates a Nexus instance that hosts a Maven mirror of the Elastic Path public repository
4. Uploads the jobs into the Jenkins instance that is created by CloudCore
5. Starts running a CloudTeam pipeline that creates the deployment package

## Validating Initialization

After running the bootstrap container to initialize CloudTeam, validate the following to ensure that CloudTeam is setup as required.

### AWS Security Groups

Access to resources created by CloudOps is controlled through [AWS Security Groups](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_SecurityGroups.html). This includes the Nexus server. By default, CloudOps only grants access to these resources to the IP address from which CloudCore was initialized. For more information about the security groups, see [Security in Elastic Path CloudOps for AWS](../architecture/infrastructure.md#security).

> **Warning**: Access to the Public security group must be restricted as it grants access to the [Config Store](../architecture/build-op-env.md#configuration-store). The Config Store contains sensitive information including EC2 keys and other credentials.

#### Granting additional access to the Nexus server

1. Identify the external IP address of the users who will need access to the Nexus server
2. Update the **Public** security group as needed

### AWS CloudFormation Stacks

Ensure that the following CloudFormation stacks are completed successfully:

- `EP-CT-ECR`: Creates the Elastic Container Registries (ECRs) required for CloudTeam
- `EP-CT-Nexus`: Creates the Maven Repository Manager

### AWS S3 Bucket

Ensure that the `ep-cloudops-<account_id>` Amazon S3 bucket exists and contains a `CloudTeam` folder with the following structure:

```text
CloudTeam/
|
|---<branch>/
    |
    |---Files/
```

### Elastic Container Registries (ECRs)

Confirm an ECR repository named `maven` is created.

### Jenkins Jobs

Confirm that the following Jenkins jobs are added to the *CloudTeam* view in the master Jenkins server:

- `bootstrap`
- `BuildCommerce`
- `BuildMavenImage`
- `BuildPipeline`

- `RunCortexSystemTests`
- `RunSelectedCommerceTests`
- `CommercePipeline`
- `CommerceScheduledPipeline`

If you access Jenkins immediately after the bootstrap process completes, a Jenkins job named `bootstrap` is running. This `bootstrap` job is responsible for populating the *CloudTeam* view with the other jobs and for triggering the `BuildPipeline` job. You can find the source for the `bootstrap` job in Jenkins at: `cloud-team-aws/Jobs/bootstrap/bootstrap.groovy`.

## Reference Materials

### `runBootstrap.sh` Settings

You must provide the following details to initialize CloudTeam and generate a deployment package from Elastic Path Commerce source code distribution:

|  Field| Description  |
|---|---|
|`epCloudTeamGitRepoUrl`| The SSH URL of the CloudTeam repository.|
|`epCommerceGitRepoUrl`|The SSH URL of the Elastic Path Commerce repository.|
|`epCloudTeamBranch`| The CloudTeam branch and release version to use for CloudTeam initialization. This setting is optional. The default setting for this parameter is master. |
|`epCommerceBranch`| The Elastic Path Commerce branch and release version to build first deployment package with. This setting is optional. The default setting for this parameter is master. |
| `cloudTeamGitSSHKeyFileName` | The name of the private SSH key authorized to clone CloudTeam from the Git repository hosting server. Ensure that the private key is in the same folder as the bootstrap Dockerfile, `Containers/bootstrap/`, and that the key is not password protected. |
| `epCommerceGitSSHKeyFileName` | The name of the private SSH key authorized to clone Elastic Path Commerce from the Git repository hosting server. Ensure that the private key is in the same folder as the bootstrap Dockerfile, `Containers/bootstrap/`, and that the key is not password protected. |
| `cloudTeamGitUsername` | This setting is optional if you don’t use AWS CodeCommit. If you use AWS CodeCommit as a git service for all CloudOps repositories, use the "SSH Key ID" as mentioned in [AWS documentation](https://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html).|
| `epCommerceGitUsername` | This setting is optional if you don’t use AWS CodeCommit. If you use AWS CodeCommit as a git service for all CloudOps repositories, use the "SSH Key ID" as mentioned in [AWS documentation](https://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html).|
|`awsAccessKeyId`  |The access key ID for the same AWS account used to initialize CloudCore. |
|`awsSecretAccessKey`| The secret access key for the same AWS account used to initialize CloudCore. |
| `awsRegion`| The AWS region in which you initialized CloudCore. |
|`epConfigStoreUrl`|The complete URL of the config store created by CloudCore, including `http://` or `https://` prefix. |
|`epNexusUserName`| The username that CloudTeam can use to authenticate to the Elastic Path public Maven repository. **Note:** Maven credentials are required to access the Elastic Path Maven repositories. To learn how to request Maven credentials, see [https://community.elasticpath.com/product-support/p/nexus-maven-repo-access](https://community.elasticpath.com/product-support/p/nexus-maven-repo-access). |
|`epNexusPassword`|The password that CloudTeam can use to authenticate to the Elastic Path public Maven repository. |
|`oracleJdkDownloadUrl`| URL to download a licensed Oracle JDK (Java Development Kit). The file must be a `tar.gz` file. For more information on which version of Java is compatible with a specific version of Elastic Path Commerce, see [Elastic Path documentation](https://developers.elasticpath.com/commerce/7.2/Core-Commerce-Development/Setting-up-your-Developer-Environment/Installing-Build-Tools/1.-Java-JDK). |
|`jdkFolderName`| The name of the folder in the `tar.gz` file that contains the JDK.|

### CloudTeam Pipeline Jenkins Jobs

#### `RunCortexSystemTests` Job

The `RunCortexSystemTests` job runs Cortex Cucumber tests against a deployed Single Instance environment.

| Parameter | Description |
|---|---|
|`SI_DEPLOY_STACK_NAME` | The root stack name of the environment created by the DeploySingleInstance job to run Cortex Cucumber tests against. |
|`EP_COMMERCE_GIT_REPO_BRANCH` | The branch in the Elastic Path Commerce repository that you want to use.  |
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |
|`CLOUDTEAM_GIT_REPO_URL` | The URL of the CloudTeam repository where the source code distribution is available. |
|`EP_CLOUDOPS_ENVNAME` | The name of the CloudOps configuration file used for the environment, not including the `.env` prefix. |

#### `RunSelectedCommerceTests` Job

The `RunSelectedCommerceTests` job runs all Commerce JUnit, integration and system tests that do not depend on a deployed Single Instance environment.

| Parameter | Description |
|---|---|
|`EP_COMMERCE_GIT_REPO_BRANCH` | The branch in the Elastic Path Commerce repository that you want to use. |
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |
|`CLOUDTEAM_GIT_REPO_URL` | The URL of the CloudTeam repository where the source code distribution is available. |
|`SELECTED_TEST_STRING` | A comma separated list of projects to run. Projects must be specified by [groupId]:artifactId or by its relative path. To exclude a project append the entry with `!`. If left empty, all Commerce tests are run.')

#### `CommercePipeline` Jenkins Job

A full pipeline that builds Commerce from source, creates the required Docker images, deploys a Single Instance environment, and runs all tests.

| Parameter | Description |
|---|---|
|`EP_COMMERCE_GIT_REPO_BRANCH` | The branch in the Elastic Path Commerce repository that you want to use. |
|`PIPELINE_TYPE` | The type of pipeline being run. This parameter must match the job name.	|
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |
|`CLOUDTEAM_GIT_REPO_URL` | The URL of the CloudTeam repository where the source code distribution is available. |

#### `CommerceScheduledPipeline` Jenkins Job

A scheduled pipeline that builds Commerce from source, creates the required Docker images, deploys a Single Instance environment, and runs all tests. By default this runs nightly and is disabled.

| Parameter | Description |
|---|---|
|`EP_COMMERCE_GIT_REPO_BRANCH` | The branch in the Elastic Path Commerce repository that you want to use. |
|`PIPELINE_TYPE` | The type of pipeline being run. This parameter must match the job name.	|
|`CLOUDTEAM_BRANCH` | The branch in the CloudTeam repository that you want to use. |
|`CLOUDTEAM_GIT_REPO_URL` | The URL of the CloudTeam repository where the source code distribution is available. |
