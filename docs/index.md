---
id: index
title: Introduction to CloudOps for AWS
sidebar_label: Introduction
---

Elastic Path CloudOps for AWS creates and deploys Elastic Path Commerce environments on AWS (Amazon Web Services) using the Docker images and AWS CloudFormation templates. Other AWS services are also used to optimize stability, reliability, performance, and operations in order to minimize Elastic Path Commerce implementation and operating costs.

## Components

Elastic Path CloudOps for AWS is modularized to three components to make the configuration and deployment flexible. The components are:

- **CloudCore**: Creates and configures the network infrastructure, build server, configuration management server, and storage
- **CloudTeam**: Creates deployment packages from the Elastic Path Commerce source code repository
- **CloudDeploy**: Creates Docker images from the deployment package, and deploys them into an environment

Each component in the Elastic Path CloudOps for AWS is initialized only once. These components set up all required servers and databases to build, deploy and run Elastic Path Commerce.

### CloudCore

CloudCore is the foundation of Elastic Path CloudOps for AWS. CloudCore sets up:

- AWS network configurations
- Jenkins server
- Configuration store
- Bastion server

### CloudTeam

CloudTeam provides the capability to build deployment packages from Elastic Path Commerce source code. The CloudTeam component also includes a Nexus server for storing artifacts created when building Elastic Path Commerce.

### CloudDeploy

CloudDeploy deploys the Elastic Path Commerce deployment package that CloudTeam builds. CloudDeploy has Jenkins jobs that:

- Builds Docker images needed for deployment
- Deploys Elastic Path Commerce
- Updates Docker containers and databases of certain deployment types
- Performs other supporting tasks


## Features

Elastic Path CloudOps for AWS is used to initialize an empty AWS account to run a production-like deployment of Elastic Path Commerce. Features include:

- Infrastructure-as-Code
- Continuous Integration and Continuous Deployment (CI/CD)
    - Streamlines development, testing and delivery to improve code accuracy and eliminate daily build bottlenecks
- A Jenkins management layer that:
    - Builds Docker images quickly
    - Orchestrates deployments and re-deployments with no downtime, easily and consistently using AWS ECS
- Fully automated deployments with ease
- Fault tolerant services that provide redundancy and lower operating costs. For example:
    - Amazon RDS Aurora for the data-tier
    - Elastic Load Balancers (ELB) for load balancing
- Auto-scaling capability
    - Ensures scaling up and down services to meet commerce requirements


## AWS Services used by CloudOps

Elastic Path CloudOps for AWS uses the following AWS services.

### ACM

Amazon Certificate Manager - Manages SSL certificates used with CloudOps infrastructure.

### CloudFormation

This is used for repeatable infrastructure management. It creates and manages a collection of related AWS resources. CloudFormation also provisions and updates the resources in a specific order.

### CloudWatch

Stores logs from Elastic Path Commerce applications.

### EC2

Elastic Cloud Computing - Virtual machines provides computing power in the cloud.

### ECR

Elastic Container Registry - Docker container registry. This stores and provides access to Docker images built and used by CloudOps.

### ECS

Elastic Container Services - Docker Container management service. It runs applications on a managed cluster of Amazon EC2 instances.

### ELB

Elastic Load Balancing - Automatically distributes incoming application traffic across multiple Amazon EC2 instance and provides fault tolerance and load balancing to route traffic for efficiency.

### Lambda

Serverless compute service - Runs code in response to events.

### RDS

Relational Database Service - Manages Aurora database clusters.

### Route53

Connects user requests to infrastructure running in AWS.

### S3

Stores files used by CloudOps infrastructure.

### SES

Simple Email Service - Sends and receives email using verified email addresses and domains.

### SQS

Simple Queue Service - Distributed asynchronous message queue service.

### VPC

Virtual Private Cloud - Controls virtual networking environment.


## Limitations

Elastic Path CloudOps for AWS does not provide a production environment. However, an environment that can be transformed into a production environment is provided.

You can only deploy one Author-Live environment for one CloudOps instance.

You must create different AWS accounts to deploy CloudOps in different regions. For a specific account, you can deploy CloudOps only in the region that is selected for that account.
