---
id: injecting-config
title: Injecting Commerce Configuration at Runtime
sidebar_label: Injecting Commerce Config
---

## Overview

Elastic Path Commerce Docker images support injecting commerce configuration at runtime of the containers. This is useful when:

* Deploying production environments where the configuration must be kept secret and should not be committed into the Elastic Path Commerce codebase
* For overriding configuration set in the Elastic Path Commerce codebase

This method of injecting configuration is secured using Git repositories, GPG keys and [git-crypt](https://www.agwa.name/projects/git-crypt/).

> **Note:** It is expected that you are familiar with Elastic Path Commerce configuration in the deployment package. For more details on configuration in the deployment package, see [Elastic Path Commerce Configuration](commerce-configurations.md).

Additional setup is required in order to take advantage of this functionality. This includes:

* Creating a new Git repository containing configuration that will be injected
* Creating a new SSH key used for cloning the repository
* Creating a new GPG key used for encrypting the repository
* Using [git-crypt](https://www.agwa.name/projects/git-crypt/) to encrypt the Git repository
* Uploading the private SSH and GPG keys to [AWS Secrets Manager](https://aws.amazon.com/secrets-manager/)
* Noting the public host key of the server hosting the new Git repository

This section guides you through all the steps required for the additional setup.

## Pre-requisites

* Ensure the following tools are installed on your local workstation:
    * [Git](https://git-scm.com/)
    * [GPG](https://gnupg.org/index.html)
    * [git-crypt](https://www.agwa.name/projects/git-crypt/)

## Creating the Git repository

1. Create a new Git repository

    > **Note:** Ideally, you would use the same Git repository hosting service used to host the Elastic Path CloudOps for AWS repositories.

2. Create a new SSH key-pair and authorize it under a utility account to clone the newly created repository.

    > **Note:** Password protected SSH keys are **not supported**

## Encrypting the repository

1. Clone the Git repository that you created in the [Creating the Git repository](#creating-the-git-repository) section

1. Run the following command to initialize the repository with git-crypt:

    ```shell
    git-crypt init
    ```

1. Create a `.gitattributes` file in the repository with the following contents:

    ```txt
    * filter=git-crypt diff=git-crypt
    .gitattributes !filter !diff
    ```

    > **Note:** This Git attributes file will configure git-crypt to encrypt everything in the repository. To configure git-crypt differently, see the [git-crypt documentation](https://github.com/AGWA/git-crypt#git-crypt---transparent-file-encryption-in-git).

1. Add and commit the `.gitattributes` file.

1. Run the following command to create a new GPG key pair:

    ```shell
    gpg --gen-key
    ```

    > **Note:** Password protected GPG keys are **not supported**. Additionally, we recommend to use a long-term utility account email instead of a personal email.

1. Run the following command to authorize the GPG key to unlock the repository:

    ```shell
    git-crypt add-gpg-user $ID
    ```

    > **Note:** Replace `$ID` with the ID of the GPG key from step 5.

1. Run the following command to export the private GPG key to a local unencrypted directory:

    ```shell
    gpg --export-secret-keys -a $ID > /path/to/unencrypted/directory/my-private-key.asc
    ```

    > **Note:** Replace `$ID` with the ID of the GPG key from step 5

## Adding Elastic Path Commerce Configuration

1. Add Elastic Path Commerce configuration to the repository in the same format as Commerce configuration in the deployment package.

    > **Note:** For more information about how configuration is stored in the deployment package, see the [Elastic Path Commerce Configuration](./commerce-configurations.md) section.

    You can include multiple configuration sets in the repository. The example below is for production configuration containing certificates and other secret information:

    ```txt
    prod-author
       files
          conf
             secure
                author-certificate-1.p12
                author-certificate-2.p12
       secure-filtering.properties

    prod-live
       files
          conf
             secure
                live-certificate-1.p12
                live-certificate-2.p12
       secure-filtering.properties
    ```

1. Add and commit the configuration files

1. Run `git-crypt status` to validate the files are encrypted correctly

    The output of the command should look similar to:

    ```shell
    $ git-crypt status
    not encrypted: .git-crypt/.gitattributes
    not encrypted: .git-crypt/keys/default/0/6E34CFEF60D34EAD74735515634491886294C9C3.gpg
    not encrypted: .gitattributes
        encrypted: prod-author/files/conf/ep.properties
        encrypted: prod-author/filtering.properties
        encrypted: prod-live/data-population.properties
        encrypted: prod-live/files/conf/cache/ehcache-2-seconds.xml
        encrypted: prod-live/files/conf/cortex/jwtTokenStrategy.config
        encrypted: prod-live/files/conf/ep.properties
        encrypted: prod-live/filtering.properties
    ```

## Creating the AWS Secrets

1. Copy the contents of the private GPG key created in step 7 of section [Encrypting the repository](#encrypting-the-repository)

    The contents should start and end with the following lines:

    ```txt
    ----BEGIN PGP PRIVATE KEY BLOCK-----

    <redacted>

    -----END PGP PRIVATE KEY BLOCK-----
    ```

1. Create an AWS Secret containing the private GPG key following the AWS documentation for [creating a basic secret](https://docs.aws.amazon.com/secretsmanager/latest/userguide/manage_create-basic-secret.html)

    > **Note:** The AWS Secret type must be _Other type of secret_, and the GPG key must be entered using the _Plaintext_ tab, as shown in the screenshot below.

    ![Screenshot of a GPG key in AWS Secrets Manager](assets/aws-secret-gpg-key.png)

1. Note the ARN (Amazon Resource Name) of the AWS Secret

    ![Screenshot of an AWS Secret with the ARN highlighted](assets/aws-secret-gpg-arn.png)

1. Repeat steps 1 to 3 with the private SSH key created in step 2 of section [Creating the Git repository](#creating-the-git-repository)

## Finding the public host key

1. Identify the hostname of the Git repository hosting service that you are using

    Some common hosting services include:
    * `github.com`
    * `gitlab.com`
    * `bitbucket.org`

1. Run `ssh-keyscan` to fetch the public host key of the server

    ```shell
    $ ssh-keyscan github.com 2> /dev/null
    github.com ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
    ```

1. Run the following command to generate the public fingerprint using the public key from step 2

    ```shell
    $ ssh-keyscan github.com 2> /dev/null | ssh-keygen -lf -
    2048 SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8 github.com (RSA)
    ```

1. Validate that you have the correct public host key by comparing the fingerprint with the Git hosting service’s documentation

    Links to documentation containing fingerprints for common hosting services:
    * [Github](https://help.github.com/en/articles/githubs-ssh-key-fingerprints)
    * [Gitlab](https://docs.gitlab.com/ee/user/gitlab_com/index.html#ssh-host-keys-fingerprints)
    * [Bitbucket](https://confluence.atlassian.com/bitbucket/ssh-keys-935365775.html)

## Sample Jenkins parameters

When running the Jenkins job `DeployAuthorAndLive` and following the examples in this section, the values for the Jenkins parameters required for injecting configuration would be:

* `AUTHOR_EP_COMMERCE_ENVNAME` is `prod-author`
* `LIVE_EP_COMMERCE_ENVNAME` is `prod-live`
* `EP_COMMERCE_CONFIG_REPO` is the SSH URL to the newly created Git repository
* `EP_COMMERCE_CONFIG_GPG_AWS_SECRET_ARN` is the ARN of the AWS Secret containing the private GPG key created in the section [Creating the AWS Secrets](#creating-the-aws-secrets)
* `EP_COMMERCE_CONFIG_SSH_AWS_SECRET_ARN` is the ARN of the AWS Secret containing the private SSH key created in the section [Creating the AWS Secrets](#creating-the-aws-secrets)
* `EP_COMMERCE_CONFIG_HOST_KEY` is the validated host key from step 2 in the section [Finding the public host key](#finding-the-public-host-key)

## Configuration Source Priority

Configuration for an environment is constructed in the following steps:

1. Files are copied from the default configuration folder in the Elastic Path Commerce code

1. Files are copied from an environment specific configuration folder in the Elastic
Path Commerce code if one one exists, for example prod-live, overwriting any duplicate files

1. Files are copied from an environment specific configuration folder in an encrypted Git repository, for example prod-live, overwriting any duplicate files

The environment specific configuration folder is set in the Docker container by the environment variable `EP_COMMERCE_ENVNAME`.

For a single-instance deployment you can set this value using the Jenkins parameter `EP_COMMERCE_ENVNAME` in the Jenkins job `DeploySingleInstance`.

For an Author and Live deployment you can set these values using the Jenkins parameters `AUTHOR_EP_COMMERCE_ENVNAME` and `LIVE_EP_COMMERCE_ENVNAME` in the Jenkins job `DeployAuthorAndLive`.
