---
id: index
title: Overview of Deploying Elastic Path Commerce
sidebar_label: Deployment Overview
---

CloudDeploy contains Jenkins jobs that enable building Elastic Path Docker images and deploying and upgrading Elastic Path Commerce. CloudDeploy supports two deployment types; a single-instance environment that you can use during Elastic Path Commerce development, and a more production-like Author and Live environment.

> **Note:** Before using any CloudOps component, ensure that you are familiar with [Jenkins](https://jenkins.io/). For more information on knowledge requirements for using *Elastic Path CloudOps for AWS* (Amazon Web Services), see [CloudOps Requirements](../../requirements.md).

For information on how to access the Jenkins server, see the [Default CloudOps Endpoints and Credentials](../../references.md#default-credentials).

## Jenkins Jobs

You can categorize the CloudDeploy Jenkins jobs into the following categories:

| Job Category | Purpose     |
| ------------ | ----------- |
| Docker Image Builds | Builds Docker images needed to deploy Elastic Path Commerce. The images include a base [Amazon Linux 2](https://aws.amazon.com/amazon-linux-2/) image with Java and Tomcat installed, images for each Elastic Path Commerce web application, an image used for populating databases with Elastic Path data, a MySQL database image, and an ActiveMQ image. |
| Elastic Path Commerce Deployments | Deploys single instance environments for development and test purposes, and pre-production Author and Live environments for a full-fledged production-like environment. |
| Environment Updates | Updates Docker images and databases on existing Author and Live deployments. For more information, see [Updating an Author and Live Environment](../deployment/deploy-auth-live.md#updating-author-and-live-environment) for more details.|
| Maintenance | Keeps the resources, such as AMIs, up-to-date. |

## Docker Image Builds

The following jobs build the Docker images needed to deploy Elastic Path Commerce:

- [BuildActiveMQ](#buildactivemq)
- [BuildBaseImage](#buildbaseimage)
- [BuildEPImage](#buildepimage)
- [BuildMySQL](#buildmysql)

These jobs use the _Elastic Path Docker_ repository to build the Docker images. After the images are successfully built, the Jenkins jobs push the images to their respective repositories in ECR (Elastic Container Registry).

For more information on how to see the parameter values that a job was run with, see the [How to see the parameter values a Jenkins jobs is run with?](../../faq/index.md#how-to-see-the-parameter-values-a-jenkins-job-is-run-with) section.

### BuildActiveMQ

This job builds the ActiveMQ Docker image used in the single instance development environment.

| Parameter |Description |
|---|---|
| `EP_ACTIVEMQ_VERSION` | The version of ActiveMQ that the job uses to build the Docker image. |
| `ACTIVEMQ_IMAGE_TAG` | The name the job tags the image in ECR. |
| `DOCKER_BRANCH` | The branch and release version of the *Docker* Elastic Path repository to use for building the image. |
| `DOCKER_GIT_REPO_URL` | The URL to the Docker Elastic Path repository. |

### BuildBaseImage

This job builds the base Docker image that the *BuildEpImage* job uses to build the Elastic Path application Docker images.

The BuildBaseImage job builds a customized [Amazon Linux 2](https://aws.amazon.com/amazon-linux-2/) image that includes Java 8. The Amazon Linux 2 base image is used to build the final base image that has Tomcat installed.

| Parameter |Description  |
|---|---|
| `EP_TOMCAT_VERSION` | The version of Tomcat to install on the final base Docker image. Must be a full version string like: `x.x.x`. Currently supported Tomcat versions depend on EP Commerce versions: Tomcat `7.0.81` for EP Commerce `7.1` to `7.3`, Tomcat `9.0.10` for EP Commerce `7.4`, Tomcat `9.0.16` for EP Commerce `7.5`. |
| `EP_JAVA_DOWNLOAD_URL` | A link to download an `.rpm` file for installing Java |
| `EP_JAVA_DOWNLOAD_CHECKSUM` | The MD5 checksum for the `.rpm` file downloaded from `EP_JAVA_DOWNLOAD_URL`. |
| `EP_IMAGE_TAG` | This parameter is deprecated and not used. |
| `DOCKER_BRANCH` | The branch and release version of the Docker Elastic Path repository to use for building the image. |
| `DOCKER_GIT_REPO_URL` | The URL to the Docker Elastic Path repository. |

### BuildEPImage

This job builds and uploads to ECR the Elastic Path Commerce web app Docker images such as:

- Cortex server
- Integration server
- Commerce Manager server
- Search server
- Batch server

It also builds and uploads a containerized version of the [Data Population tool](https://developers.elasticpath.com/devtools/7.4/Data-Population/data-population-overview-accelerator) that is used to populate a database with Elastic Path data during deployment of Elastic Path Commerce. All web-apps and data come from the same deployment package.

| Parameter |  Description |
| --------- | ------------ |
| `EP_TOMCAT_VERSION` | Changes which Tomcat files and base image to use. Must be a full version string like: `x.x.x` and match the `EP_TOMCAT_VERSION` used when running the *BuildBaseImage* job. Currently supported Tomcat versions depend on EP Commerce versions: Tomcat `7.0.81` for EP Commerce `7.1` to `7.3`, Tomcat `9.0.10` for EP Commerce `7.4` and Tomcat `9.0.16` for EP Commerce `7.5`. |
| `DEPLOYMENT_PACKAGE_S3_URI` | The S3 location of the Elastic Path deployment package zip file that the job uses when building the Docker images. The deployment package contains the compiled Java code from all of the Elastic Path applications, application data, and other important files. |
| `EP_IMAGE_BUILDER_CONFIG` | The name of the config file the build script uses when building the Elastic Path images. The config file specifies which Elastic Path web apps to build and what Dockerfile template to use when building them. |
| `EP_IMAGE_TAG` | The name the job tags built images in ECR. |
| `DOCKER_BRANCH` | The branch and release version of the Docker Elastic Path repository to use for building the images. |
| `DOCKER_GIT_REPO_URL`| The URL to the Docker Elastic Path repository. |

### BuildMySQL

This job builds the MySQL Docker image used in the single instance development environment. It is based on the MySQL 5.7 stock image and includes some additional features such as health-checks and EP required database config.

| Parameter  |  Description|
|---|---|
| `MYSQL_IMAGE_TAG` | The name the job tags the image in ECR. |
| `DOCKER_BRANCH` | The branch and release version of the Docker Elastic Path repository to use for building the image. |
| `DOCKER_GIT_REPO_URL` | The URL to the Docker Elastic Path repository. |


## Elastic Path Commerce Deployments

The following jobs support deploying and updating Elastic Path Commerce environments:

- [CreateEcsAmi](#createecsami)
- [DeploySingleInstance](#deploysingleinstance)
- [DeployAuthorAndLive](#deployauthorandlive)
- [UpdateAuthorAndLive_1_StageUpdates](#updateauthorandlive_1_stageupdates)
- [UpdateAuthorAndLive_2_ApplyUpdates](#updateauthorandlive_2_applyupdates)
- [UpdateSingleInstance](#updatesingleinstance)
- [Setup-AWS-SES](#setting-up-aws-ses)

For more information on how to see the parameters that a job is run with, see [How to see the parameter values a Jenkins jobs is run with?](../../faq/index.md#how-to-see-the-parameter-values-a-jenkins-job-is-run-with).

### CreateEcsAmi

This job creates an Elastic Path customized version of the AWS ECS (Elastic Container Service) optimized AMI (Amazon Machine Image).

| Parameter  | Description |
|---|---|
| `STACK_NAME` | The name of the stack to create in CloudFormation. |
| `CLOUDCORE_BRANCH` | The CloudCore branch and release version folder in the Amazon S3 bucket from which the job gets CloudFormation templates. |
| `OPT_ECS_AMI_NAME` | The name of the ECS-optimized AMI that AWS publishes. This AMI is used as a base for the new customized AMI. |
| `KEY_NAME` | The name of the SSH key to use when launching any EC2 instances. |
| `INSTANCE_TYPE` | The EC2 instance type to use when launching any EC2 instances. |
| `NETWORK_STACK` | The name of the CloudOps network CloudFormation stack, which is used to source networking information, such as subnets and security groups. |
| `EC2_NAME` | The name of the EC2 that the CloudFormation stack, `STACK_NAME`, creates. |
| `ECS_OPT_AMI_NAME` | The name of the new ECS-optimized AMI to be created. |

### DeploySingleInstance

This job deploys a single instance development environment. For more information on the single instance environment, see [Deploying Single Instance Environment](deploy-single-instance.md).

| Parameter |Description  |
|---|---|
| `STACK_NAME` | The name of the stack to create in CloudFormation. |
| `ECS_AMI_ID` | The ID of the ECS-optimized AMI to use when this job launches any EC2 instances that run containers. |
| `CLOUDDEPLOY_BRANCH` | The CloudDeploy branch and release version folder in Amazon S3 from which the job gets CloudFormation templates. |
| `CLOUDDEPLOY_GIT_REPO_URL` | The Git repository from which the Amazon S3 resources are updated. |
| `EP_IMAGE_TAG` | The Docker image to use for all Elastic Path Commerce related containers when deploying the single instance environment. |
| `MYSQL_IMAGE_TAG` | The Docker image to use for the MySQL container in the single instance environment. |
| `ACTIVEMQ_IMAGE_TAG` | The Docker image to use for the ActiveMQ container in the single instance environment. |
| `KEY_NAME` | The name of the SSH key to use for any launched EC2 instances. |
| `NETWORK_STACK` | The name of the CloudOps network CloudFormation stack, which is used to source networking information, such as subnets and security groups. |
| `ROUTE_53_STACK_NAME` | The name of the CloudOps Route53 related CloudFormation stack. This stack is used to source DNS information like Hosted Zone IDs. |
| `EP_CERTIFICATE_ARN` | The ARN (Amazon Resource Name) for the HTTPS certificate in AWS Certificate Manager used to initialize CloudCore. This setting is required in order to use a custom DNS name with the API gateway. |
| `DNS_NAME` | The DNS name to point to the application load-balancer. |
| `SI_API_GW_NAME` | The name to use when creating the API gateway. |
| `SI_API_GW_CORTEX_STAGE_NAME` | The AWS API gateway stage name of the Cortex resource in the API gateway. |
| `SI_API_GW_STUDIO_STAGE_NAME` | The AWS API gateway stage name of the Studio resource in the API gateway. |
| `SI_CORTEX_API_GW_DNS_NAME` | The DNS name to point to the API gateway. |
| `SI_CORTEX_NLB_DNS_NAME` | The DNS name to point to the internal network load-balancer. |
| `SI_MYSQL_ELB_DNS_NAME` | The DNS name to point to the elastic load-balancer used by the MySQL service. |
| `SI_ACTIVEMQ_ELB_DNS_NAME` | The DNS name to point to the elastic load-balancer used by the ActiveMQ service. |
| `EP_COMMERCE_ENVNAME` | The Elastic Path Commerce environment configuration set to use when deploying the Elastic Path applications. For more information, see the [Application Configuration Files](commerce-configurations.md#elastic-path-commerce-configuration) section. |
| `EP_COMMERCE_CONFIG_REPO` | (Optional) The URL to a git-crypt encrypted Git repository where Elastic Path Commerce configuration is stored. For more information about injecting configuration at runtime, see [Injecting Commerce Configuration at Runtime](./injecting-config.md).  |
| `EP_COMMERCE_CONFIG_GPG_AWS_SECRET_ARN` | The ARN (Amazon Resource Name) of an AWS Secret with a private GPG key authorized to unlock `EP_COMMERCE_CONFIG_REPO`. If the parameter `EP_COMMERCE_CONFIG_REPO` is set, this setting is required. For more information about injecting configuration at runtime, see [Injecting Commerce Configuration at Runtime](./injecting-config.md). |
| `EP_COMMERCE_CONFIG_SSH_AWS_SECRET_ARN` | The ARN of an AWS Secret with a private SSH key authorized to clone from `EP_COMMERCE_CONFIG_REPO`. If the parameter `EP_COMMERCE_CONFIG_REPO` is set, this setting is required. For more information about injecting configuration at runtime, see [Injecting Commerce Configuration at Runtime](./injecting-config.md). |
| `EP_COMMERCE_CONFIG_HOST_KEY` | The public host key of the server where `EP_COMMERCE_CONFIG_REPO` is hosted. If the parameter `EP_COMMERCE_CONFIG_REPO` is set, this setting is required. For more information about injecting configuration at runtime, see [Injecting Commerce Configuration at Runtime](./injecting-config.md). |
| `EP_CLOUDOPS_ENVNAME` | The CloudOps environment configuration file to use when deploying the environment. For more information, see the [CloudOps Default Configuration Sets](../configurations.md#cloudops-configuration-sets) section. |
| `DATABASE_TYPE` | The type of database the environment uses. |
| `TARGET_GROUP_NAME_PREFIX` | The prefix for the names of all the AWS Target Groups created. |
| `LOAD_BALANCER_SCHEME` | The scheme to use for the application load-balancer. Can be either internet-facing or internal. |
| `NAMESPACE` | The namespace where Docker images are pulled from when deploying a single instance environment. The default value is empty. |
| `CONFIG_PREFIX` | Additional prefixes to use when locating keys in the Consul config store. |
| `SETUP_WITH_SES` | Deploys with AWS SES (Simple Email Service) email settings. Can only be run if the Jenkins job `Setup-AWS-SES` was run beforehand. See [Setting up AWS SES SMTP](#setting-up-aws-ses). |

### DeployAuthorAndLive

This job deploys an Author and Live environment. For more information on deploying Author and Live environments, see the [Deploying Author and Live Environments](deploy-auth-live.md) page.

<!-- removed from list until CLOUD-1563 is done -->
<!-- |`ENVIRONMENT_SIZE` | The size of the Author and Live environment. Sets the app nodes, RDS, and ActiveMQ instance sizes. | -->

| Parameter | Description |
|---|---|
| `STACK_NAME` | The name of the stack to create in CloudFormation. |
| `EP_IMAGE_TAG` | The Docker image to use for all Elastic Path Commerce related containers when you deploy the Author and Live environment. |
| `CLOUDDEPLOY_BRANCH` | The CloudDeploy branch and release version folder in Amazon S3 from which the job gets CloudFormation templates. |
| `CLOUDDEPLOY_GIT_REPO_URL` | The Git repository from which the Amazon S3 resources are updated. |
| `ENCRYPT_RDS` | If the RDS data base uses encryption at rest. Default is true. |
| `KMS_CMK_ID` | ARN of a AWS Key Management Service Customer Master Key. If not provided, a CMK (Customer Master Key) will be generated if ENCRYPT_RDS has been enabled. |
| `LIVE_ACTIVEMQ_SUBDOMAIN` | Subdomain for the Live ActiveMQ cluster. |
| `AUTHOR_ACTIVEMQ_SUBDOMAIN` | Subdomain for the Author ActiveMQ cluster. |
| `AUTHOR_SUBDOMAIN` | Subdomain for the Author application load-balancer. |
| `LIVE_CM_SUBDOMAIN` | Subdomain for the Live CM load-balancer. |
| `LIVE_CORTEX_SUBDOMAIN` | Subdomain for the Live Cortex load-balancer. |
| `LIVE_INTEGRATION_SUBDOMAIN` | Subdomain for the Live Integration load-balancer. |
| `LIVE_SEARCHMASTER_SUBDOMAIN` | Subdomain for the Live Search master load-balancer. |
| `AUTHOR_CORTEX_NLB_DNS_NAME` | The DNS name for the internal network load-balancer pointing to Cortex.  |
| `LIVE_API_GW_NAME` | The name to use when creating the Author API gateway. |
| `AUTHOR_API_GW_NAME` | The name to use when creating the Live API gateway. |
| `LIVE_API_GW_CORTEX_STAGE_NAME` | The AWS API gateway stage name of the Cortex resource in the Live API gateway. |
| `AUTHOR_API_GW_CORTEX_STAGE_NAME` | The AWS API gateway stage name of the Cortex resource in the Author API gateway. |
| `LIVE_API_GW_STUDIO_STAGE_NAME` | The AWS API gateway stage name of the Studio resource in the Live API gateway. |
| `AUTHOR_API_GW_STUDIO_STAGE_NAME` | The AWS API gateway stage name of the Studio resource in the Author API gateway. |
| `LIVE_CORTEX_API_GW_DNS_NAME` | The DNS name for the Live API gateway. |
| `AUTHOR_CORTEX_API_GW_DNS_NAME` | The DNS name for the Author API gateway. |
| `BASE_DNSNAME` | Base DNS name for each of the Author and Live services. |
| `AUTHOR_DB_INSTANCE_IDENTIFIER` | The name for identifying the Author DB instance. |
| `LIVE_DB_INSTANCE_IDENTIFIER` | The name for identifying the Live DB instance. |
| `AUTHOR_EP_DB_MASTER_USER` | The username of the Author’s RDS (Relational Database Service) master user. |
| `AUTHOR_EP_DB_MASTER_PASSWORD` | The password of the Author’s RDS master user. |
| `LIVE_EP_DB_MASTER_USER` | The username of the Live’s RDS master user. |
| `LIVE_EP_DB_MASTER_PASSWORD` | The password of the Live’s RDS master user. |
| `LIVE_JMX_USER` | (Optional) The JMX username for the Live deployment. If left empty, the value is auto-generated. The value is stored in the configuration store with other deployment environment variables. |
| `LIVE_JMX_PASSWORD` | (Optional) The JMX password for the Live deployment. If left empty, the value is auto-generated. The value is stored in the configuration store with other deployment environment variables. |
| `AUTHOR_JMX_USER` | (Optional) The JMX username for the Author deployment. If left empty, the value is auto-generated. The value is stored in the configuration store with other deployment environment variables. |
| `AUTHOR_JMX_PASSWORD` | (Optional) The JMX password for the Author deployment. If left empty, the value is auto-generated. The value is stored in the configuration store with other deployment environment variables. |
| `AMAZON_LINUX_AMI_ID` | The ID of the AMI that want to use when you launch any EC2 instance that does not run any container. |
| `ECS_AMI_ID` | The ID of the ECS-optimized AMI that you want to use when this job launches any EC2 instance that runs containers. |
| `AUTHOR_RDS_SNAPSHOT_ID` | Only use for disaster recovery purposes |
| `LIVE_RDS_SNAPSHOT_ID` | Only use for disaster recovery purposes |
| `KEY_NAME` | The name of the SSH key to use for any launched EC2 instances. |
| `OFFICE_IP_ADDRESS` | The public IP address of the office to add to the public security group ingress rules. |
| `NETWORK_STACK` | The name of the CloudOps network CloudFormation stack, which is used to source networking information, such as subnets and security groups. |
| `ROUTE_53_STACK_NAME` | The name of the CloudOps Route53 related CloudFormation stack. This stack is used to source DNS information, such as Hosted Zone IDs. |
| `LAMBDA_STACK_NAME` | The name of the CloudOps Lambda CloudFormation stack, which is the Lambda Function that puts the Author and Live RDS endpoints into the config store. |
| `EP_CERTIFICATE_ARN` | The ARN for the HTTPS certificate in AWS Certificate Manager used to initialize CloudCore. This setting is optional, but note that the Author and Live environment does not use SSL if this field is left blank. |
| `CONFIG_PREFIX` | The key to store the Author and Live RDS endpoint values in the config store. |
| `EP_ACTIVEMQ_VERSION` | The version of ActiveMQ to be used. |
| `EP_RDS_VERSION` | The RDS engine version for both Author and Live Elastic Path databases. |
| `EP_RDS_PARAMETER_GROUP` | The AWS database parameter group for both Author and Live Elastic Path databases. |
| `DEPLOY_HORIZONTAL_DATABASE_SCALING` | Enables read write split in the Live environment with two read replicas. Deploying with EP_RDS_READ_REPLICA will not increase the number of read replicas to three. |
| `EP_RDS_READ_REPLICA` | Create a read replica for Author and Live RDS Aurora databases. |
| `AUTHOR_EP_COMMERCE_ENVNAME` | The Elastic Path Commerce environment configuration folder to use when deploying the Author environment. For more information, see the [Application Configuration Files](commerce-configurations.md#application-configuration-files) section. |
| `LIVE_EP_COMMERCE_ENVNAME` | The Elastic Path Commerce environment configuration folder to use when deploying the Live environment. For more information, see the [Application Configuration Files](commerce-configurations.md#application-configuration-files) section. |
| `EP_COMMERCE_CONFIG_REPO` | (Optional) The URL to a git-crypt encrypted Git repository where Elastic Path Commerce configuration is stored. For more information about injecting configuration at runtime, see [Injecting Commerce Configuration at Runtime](./injecting-config.md).  |
| `EP_COMMERCE_CONFIG_GPG_AWS_SECRET_ARN` | The ARN (Amazon Resource Name) of an AWS Secret with a private GPG key authorized to unlock `EP_COMMERCE_CONFIG_REPO`. If the parameter `EP_COMMERCE_CONFIG_REPO` is set, this setting is required. For more information about injecting configuration at runtime, see [Injecting Commerce Configuration at Runtime](./injecting-config.md). |
| `EP_COMMERCE_CONFIG_SSH_AWS_SECRET_ARN` | The ARN of an AWS Secret with a private SSH key authorized to clone from `EP_COMMERCE_CONFIG_REPO`. If the parameter `EP_COMMERCE_CONFIG_REPO` is set, this setting is required. For more information about injecting configuration at runtime, see [Injecting Commerce Configuration at Runtime](./injecting-config.md). |
| `EP_COMMERCE_CONFIG_HOST_KEY` | The public host key of the server where `EP_COMMERCE_CONFIG_REPO` is hosted. If the parameter `EP_COMMERCE_CONFIG_REPO` is set, this setting is required. For more information about injecting configuration at runtime, see [Injecting Commerce Configuration at Runtime](./injecting-config.md). |
| `AUTHOR_EP_CLOUDOPS_ENVNAME` | Specifies the CloudOps environment file to use with the Author deployment. For more information, see the [CloudOps Default Configuration Sets](../configurations.md#cloudops-default-configuration-sets) section. |
| `LIVE_EP_CLOUDOPS_ENVNAME` | Specifies the CloudOps environment file to use with the Live deployment. For more information, see the [CloudOps Default Configuration Sets](../configurations.md#cloudops-default-configuration-sets) section. |
| `NAMESPACE` | The namespace where Docker images are pulled from when deploying an Author and Live environment. The default value is empty. |
| `SETUP_WITH_SES` | Deploys with AWS SES email settings. Can only be run if the Jenkins job `Setup-AWS-SES` was run beforehand. See [Setting up AWS SES SMTP](#setting-up-aws-ses). |

### TeardownEnvironments

| Parameter | Description |
|---|---|
| `CLOUDDEPLOY_BRANCH` | The CloudDeploy branch and release version folder in Amazon S3 from which the job gets CloudFormation templates. |
| `CLOUDDEPLOY_GIT_REPO_URL` | The Git repository from which the Amazon S3 resources are updated. |
| `STACK_NAME_PREFIX` | The name or prefix of the stack to delete in CloudFormation. This also deletes the parameter values in the config store in the path `deploy/ep/config/v1/envs/<STACK_NAME>`. |

### UpdateAuthorAndLive_1_StageUpdates

This job generates a script that you can use to update an Author and Live environment. For more information about updating an Author and Live environment, see [Updating Environment](deploy-auth-live.md#updating-author-and-live-environment).

> **Note:** This Jenkins job is to update a deployment that is not under active load. Contact [Elastic Path Support](https://community.elasticpath.com/product-support/) for more information about updating a deployment that is under active load.

| Parameter | Description  |
|---|---|
| `APPLICATIONS_STACK_NAME` | Specifies the name of the application stack that you want to update. |
| `NEW_IMAGE_TAG` | Specifies the Docker image tag that you want to update your stack with. |
| `WITH_DB_UPDATES` | Specifies that updating the environment’s database is enabled. If you enable this setting, you must also set the `RDS_STACK_NAME` and `DATA_POP_COMMAND` values. |
| `RDS_STACK_NAME` | Specifies the CloudFormation stack that creates the RDS instance that you want to update. |
| `DATA_POP_COMMAND` | Specifies the data population command to be run when updating the database. The options are `update_db` and `reset_db`. |
| `ROOT_USERNAME` | Specifies the root database username (required when `DATA_POP_COMMAND` is set to reset-db). |
| `ROOT_PASSWORD` | Specifies the root database password (required when `DATA_POP_COMMAND` is set to reset-db). |
| `STAGING_DIRECTORY_S3_URI` |Specifies the folder in Amazon S3 where you can stage the update script that this job generates. |
| `ZERO_DOWN_TIME` |Specifies that the update is run in "Zero Downtime" mode. for more information about zero downtime mode, see [Updating Environment](deploy-auth-live.md#updating-author-and-live-environment). |
| `CLOUDDEPLOY_BRANCH` | Specifies the CloudDeploy branch and release version folder in Amazon S3. This job gets CloudFormation templates and updates related scripts. |
| `CLOUDDEPLOY_GIT_REPO_URL` | Specifies the URL to the CloudDeploy repository to use when sourcing the update related scripts. |

### UpdateAuthorAndLive_2_ApplyUpdates

This job runs the update script created by the UpdateAuthorAndLive_1_StageUpdates job. For more information about updating an Author and Live environment, see [Updating Environment](deploy-auth-live.md#updating-author-and-live-environment).

> **Note:** This Jenkins job is to update a deployment that is not under active load. Contact [Elastic Path Support](https://community.elasticpath.com/product-support/) for more information about updating a deployment that is under active load.

| Parameter | Description |
|---|---|
| `STAGING_DIRECTORY_S3_URI` | The same Amazon S3 folder used in the `UpdateAuthorAndLive_1_StageUpdates` job to stage the update script. |
| `STAGING_FILE` | The name of the update script located in the folder specified by `STAGING_DIRECTORY_S3_URI`. |

### UpdateSingleInstance

This job generates and runs a script that updates a Single Instance environment. For more information about updating a Single Instance environment, see [Updating Environment](deploy-single-instance.md#updating-single-instance-environment).

| Parameter | Description |
|---|---|
| `STACK_NAME` | Specifies the name of the stack that you want to update. |
| `NEW_IMAGE_TAG` | Specifies the Docker image tag that you want to update your stack with. |
| `DATA_POP_COMMAND` | Specifies the data population command to be run when updating the database. The options are `update_db` and `reset_db`. |
| `CLOUDDEPLOY_BRANCH` | Specifies the CloudDeploy branch and release version folder in Amazon S3. This job gets CloudFormation templates and updates related scripts. |
| `CLOUDDEPLOY_GIT_REPO_URL` | Specifies the URL to the CloudDeploy repository to use when sourcing the update related scripts. |

## Setting Up AWS SES

There are two Jenkins jobs for setting up Amazon’s SES, both jobs must be completed before an environment can be deployed to use SES:

- `Setup-AWS-SES`: Run this once in a region. This job does the following:
    - Creates a user with SES permissions and SMTP credentials
    - Pushes the generated tokens, credentials, and SMTP environment parameters into the Config Store

- `Verify-Domain-for-SES`: Run this for every domain which will be used as senders. This job does the following:
    - Creates a user with SES permissions and SMTP credentials
    - Pushes the generated tokens, credentials, and SMTP environment parameters into the Config Store

> **Note:** If the SMTP interface is used to send mails in multiple AWS regions, a unique set of SMTP credentials must be created for each region.

For more information about AWS SES, refer to the AWS documentation [What Is Amazon SES](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/Welcome.html).

> **Known Issues**: Elastic Path Commerce versions before 7.5 do not accept SMTP credential passwords containing a "+". `Setup-AWS-SES` may need to be run several times in order to generate a credential password without a "+".

### `Setup-AWS-SES`

This job creates an IAM (Identity and Access Management) user and SMTP credentials and uses the parameters in the following table.

| Parameter  | Description |
|---|---|
| `STACK_NAME` | The name of the stack to create in CloudFormation. |
| `CLOUDDEPLOY_BRANCH` | The CloudDeploy branch from which the job gets CloudFormation templates. |
| `CLOUDDEPLOY_GIT_REPO_URL` | The Git repository from which the job gets CloudFormation templates. |
| `AWS_REGION` | The region of the SES SMTP endpoint to connect to. |

### `Verify-Domain-for-SES`

This job creates `DKIM` and domain record tokens and uses the parameters in the following table.

| Parameter  | Description |
|---|---|
| `STACK_NAME` | The name of the stack to create in CloudFormation. |
| `CLOUDDEPLOY_BRANCH` | The CloudDeploy branch from which the job gets CloudFormation templates. |
| `CLOUDDEPLOY_GIT_REPO_URL` | The Git repository from which the job gets CloudFormation templates. |
| `DOMAIN_NAME` | The identity domain that you use to send email from using Amazon SES. |
| `UPDATE_AWS_RECORD_SETS` |  Updates AWS Route53 with the record sets generated for `DKIM` and domain verification. If you enable this setting, you must also set the `HOSTED_ZONE_ID` value. |
| `AWS_REGION` | The region of the SES SMTP endpoint to connect to. |
| `HOSTED_ZONE_ID` | The ID of the public hosted zone created in CloudCore. |

### After Setting Up AWS SES

Before sending emails with Amazon SES make sure that email addresses in the Elastic Path Commerce database are within the domain used in `DOMAIN_NAME`. In sandbox mode emails can only be sent to email addresses and domains that have been verified by Amazon SES.

- To verify email addresses, refer to the AWS documentation on [Verifying Identities on Amazon SES](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-addresses-and-domains.html)
- To verify additional domains, rerun the `Verify-Domain-for-SES` job with a unique `STACK_NAME` parameter and the additional domain in `DOMAIN_NAME`

If `Verify-Domain-for-SES` was run without enabling `UPDATE_AWS_RECORD_SETS`, update your domain’s DNS server with the domain verification token and the `DKIM` verification tokens.

1. **Verifying the Domain**

    The domain verification token is found in the Config Store in the path `deploy/ep/ses/domainVerificationToken`. To create a `TXT` record for this token, refer to [Amazon SES documentation](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/dns-txt-records.html)

2. **Setting up `DKIM`**

    The `DKIM` verification tokens are found in the Config Store in the path `deploy/ep/ses/dkimVerificationToken<token_number>`. To create `CNAME` records for these tokens, refer to [Authenticating Email with `DKIM` in Amazon SES](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/dkim.html)

    > **Note:** It can take up to 72 hours for Amazon SES to verify that these record sets have been added.

3. **Email Reputation Management**

    The jobs you just ran set up your AWS account with Amazon SES. To complete the set up of Email Reputation Management refer to [Monitoring Your Amazon SES Sender Reputation](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/monitor-sender-reputation.html).

    For more information on handling bounce and complaint messages, see [Handling Bounces and Complaints](https://aws.amazon.com/blogs/messaging-and-targeting/handling-bounces-and-complaints/)

### Requesting Production Access

New Amazon SES accounts are created in sandbox mode. To transition out of the sandbox, refer to the AWS documentation: [Moving Out of the Amazon SES Sandbox](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/request-production-access.html).

### Deploying an Environment with Amazon SES

After setting your AWS account with Amazon SES, a single instance development environment or an Author and Live environment can be deployed to use Amazon SES.

- Configure your database to use email addresses in the domain you are verified for on AWS SES
- Run the deployment job with the `SETUP_WITH_SES` parameter set to true

For more information on the job parameters of the deployment jobs, see:

- [Deploy Single Instance Dev Environment Jenkins job parameters](#DeploySingleInstance)
- [Deploy Author And Live Jenkins job parameters](#DeployAuthorAndLive)

## Related Topics

- [CloudDeploy Configurations](../configurations.md)

<!-- Hidden until CLOUD-1563 -->
<!-- - [Author and Live Environment Sizing](../../architecture/auth-live-sizing.md) -->
