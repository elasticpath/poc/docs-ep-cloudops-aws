---
id: update-single-instance
title: Updating Single Instance Environments
sidebar_label: Update Single Instance
---

## Overview

You can set up a single instance environment in a short period of time. Elastic Path recommends deploying a new environment instead of updating an existing one.

However, there are three cases where you can update a single instance environment:

1. Preserve previously created test data on a small scale environment
2. Test database updates on a small scale environment
3. Compare the performance of two versions of code on the same infrastructure

Elastic Path provides an automated process with Jenkins jobs for both of these scenarios.

> **Note**: The following scripts work only for the Single Instance environment.


### Redeployment Workflow

#### Procedure

1. Build a new deployment package from your Elastic Path Commerce source code using the `BuildCommerce` job in the CloudOps Jenkins console
2. Build and tag a new set of Docker images using the `BuildEPImage`, `BuildActiveMQ`, and `BuildMySQL` jobs
3. Deploy your new set of images using the `UpdateSingleInstance` job

> **Note**:
>
> When running the DATA_POP_COMMAND Jenkins job parameter set to update-db, the following occurs:
>
> > - The old Elastic Path containers are torn down
> > - The MySQL and ActiveMQ containers from the previous deployment are left up, and a `data-pop-tool` container is run against them in update-db mode
> > - New Elastic Path containers are spun up
>
> When running the DATA_POP_COMMAND Jenkins job parameter set to reset-db, the following occurs:
>
> > - The old Elastic Path, MySQL, and ActiveMQ containers are torn down
> > - New MySQL and ActiveMQ container are spun up, and a `data-pop-tool` container is run against the MySQL container in reset-db mode
> > - New Elastic Path containers are spun up
