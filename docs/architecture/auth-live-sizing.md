---
id: auth-live-sizing
title: Author and Live Environment Sizing
sidebar_label: Author & Live Environment Sizing
---

The Jenkins job `DeployAuthorAndLive` can deploy an Author and Live environment in the following four different environment sizes:

- Micro
- Small
- Medium
- Large

## Environment Options

| Size | Environment | App Node Instance Type | Memory | ActiveMQ Instance Type | Memory | RDS  Instance Type | Memory |
|---|---|---|---|---|---|---|---|
|​ Micro | ​Author | r5.large | 16 |​ t3.medium | 4 |​ db.t3.medium | 4 |
| | Live | t3.large | 8 | t3.medium | 4 | db.t3.medium | 4 |
| Small | Author | r5.large | 16 | c5.large | 4 | db.r5.large | 15 |
| | Live | m5.large | 8 | c5.large | 4 | db.r5.large | 15 |
| Medium | Author | r5.large | 16 | c5.large | 4 | db.r5.large | 15 |
| | Live | c5.xlarge | 8 | c5.large | 4 | db.r5.xlarge | 30 |
| Large | Author | m5.xlarge | 16 | c5.large | 4 | db.r5.large | 15 |
| | Live | c5.xlarge | 8 | c5.xlarge | 8 | db.r5.2xlarge | 61 |

The different sizes do not decrease the available instance memory but affects the processing power in an environment. A `Micro` sized environment is not suited for production environments. The `Small`, `Medium`, and `Large` sized environments are all suitable for production environments.

| Size | Use Case | Live Database Cores | Live Database Memory |
|---|---|---|---|
| Micro | Non-production environments with small catalogs and intermittent loads. Not cost-effective for continuous loads. | 2 | 4 GB |
| Small | Small production environments | 2 | 15 GB |
| Medium | Medium production environments | 4 | 30 GB |
| Large | Large production environments | 8 | 61 GB |