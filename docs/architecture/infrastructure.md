---
id: infrastructure
title: Author and Live Environment Details
sidebar_label: Author & Live Details
---

## Security

### Data in Flight

Elastic Path CloudOps for AWS (Amazon Web Services) uses transport layer security mechanism called Secure Sockets Layer certificate (SSL) to control HTTP traffic securely by encrypting the data in flight.

Amazon’s ELB (Elastic Load Balancer)  are the primary public interface for CloudOps.  The security groups, which the CloudCore component creates, control traffic to these ELBs. By default, these security groups allow both incoming traffic (ingress), which is based on your IP address and which port you are connecting to, and any outgoing traffic (outgress). Any external traffic from and to CloudOps network is encrypted using the SSL certificates. SSL is terminated at ELB and the secure internal CloudOps network uses unencrypted data by default. However, you can use the SSL keys to enable encryption for the data being sent over various components, such as Cortex, Commerce Manager, in CloudOps. Elastic Path uses the [AWS Certificate Manager](https://aws.amazon.com/certificate-manager/) to create SSL certificates.

You must have an SSL certificate to enable HTTPS. Elastic Path recommends using  the AWS [ACM (Amazon Certificate Manager)](https://aws.amazon.com/certificate-manager/) to create SSL certificates. Alternatively, if you already have a certificate, you can import the certificate into ACM. When you initialize CloudCore, specify this certificate details in the `epCertificateArn` option as described in the [CloudCore runBootstrap.sh Settings](../cloudCore/initialization.md#runbootstrapsh-parameters) section.

For more information about CloudOps HTTPS Implementation, see [HTTPS Configuration](../cloudCore/configurations.md#https).

### Data at Rest

CloudOps for AWS uses Amazon RDS (Relational Database Service) Aurora instances to provision databases and by default the Amazon RDS instance uses encryption at rest, via the AWS Key Management Service.

AWS KMS uses the AES-256 encryption algorithm to encrypt data on the server that hosts your Amazon RDS instance. A Customer Master Key is created if one is not provided during the deployment of an Author Live environment.

For more information, see [Encrypting Amazon RDS Resources](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.Encryption.html).

### Author and Live Security Groups

The Author and Live environment uses the [Amazon EC2 security groups](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html) to control network access.

CloudCore creates three security groups, a public security group, a private security group, and a bastion security group. The public security group controls access to infrastructure in the public subnet, the private security group controls access to infrastructure in the private subnet, and the bastion security group controls access to the bastion server.

The Author and Live environment is deployed across the public and private subnets. However, you can only access infrastructure in the public subnet directly, and all important load-balancers(LB) are created in the public subnet.

The following table and diagram illustrate in what direction communication is allowed among resources in a CloudOps environment with an Author and Live deployment. The diagram also represents how the Bastion server and security group are tied to the other resources.

|  | Public Security Group | Private Security Group | Bastion Security Group | VPN IP Address |
|:---:|:------------------:|:----------------------:|:----------------------:|:----------------:|
| **Public Security Group** | Allows all traffic from servers within the public security group. | Allows all traffic from servers in the private security group. | Allows all traffic from the bastion server. | Allows all traffic from the VPN IP address. |
| **Private Security Group** | Allows all traffic from servers in the public security group. | Allows all traffic from servers within the private security group. Also allows all connections from IP addresses starting with `10.20.x.x` in order to correctly enable network load-balancers. | Allows all traffic from servers in the bastion security group. | No access from VPN IP address. |
| **Bastion Security Group** | Allows only TCP traffic on port 22 from servers in the public security group. | Allows only TCP traffic on port 22 from servers in the private security group. | No ingress rules defined, because only one server belongs to the bastion security group. | Allows ICMP access from the VPN IP address and TCP traffic on port 22 from the VPN IP address. |

![Security Groups and Author and Live servers.](assets/cloudops_security_groups.png)

- The public security group and private security groups allow all communication within themselves. All resources in a public security group or private security groups can communicate together with other resources in the same group
- The public and private security groups allow all communication between each other
- The public security group allows all communication from a VPN IP address set at the beginning of CloudOps initialization, but the private security groups doesn’t allow this
- The public and private security groups allows all communication from the bastion server
- The bastion security group allows communication from the public and private security groups only through TCP traffic on port 22
- The bastion security group also allows communication from the VPN IP address on ICMP traffic and on TCP traffic on port 22

## Network

The following diagram provides a detailed network view of the connections among the Elastic Path applications and services. The diagram also illustrates which services are accessible through a VPN:

- Live Cortex server
- Live Integration server
- Live Commerce Manager
- Author EP applications

All other apps and services are inaccessible without using the [Bastion server](build-op-env.md#bastion-instance).

![Author and Live network diagram.](assets/author-live-network.png)

For more information on the components in this diagram, see [Author and Live Architecture](auth-live-env.md).

## ActiveMQ

### ActiveMQ Overview

*Elastic Path CloudOps for AWS* deploys ActiveMQ as a two node cluster in a high-availability configuration without autoscaling. An AWS Elastic Load Balancer (ELB) is used to route traffic to the primary ActiveMQ instance. Only the primary ActiveMQ instance responds on the TCP port. A posix file system lock on the KahaDB is used to designate a primary instance. An AWS Elastic File System (EFS) is used as the shared disk for the KahaDB persistence store. For more information on the shared file system used by ActiveMQ, see the next section below.

The cluster consists of a load-balancer listening on TCP port `61616` and has two static instances connected to it. Only the TCP port of the live primary ActiveMQ server is open, so the load-balancer routes the traffic only to the primary server.

![High Availability ActiveMQ Cluster](assets/activemqclusterdiagram.png)

In addition to a TCP port `61616` for openwire, the ActiveMQ admin interface is available on the load-balancer on either port 80 (http) or 443 (https). The load-balancer redirects to the admin port 8161 on the primary ActiveMQ server.

![ActiveMq Cluster ports Diagram](assets/activemqclusterportsdiagram.png)

### ActiveMQ Shared File System

#### AWS Elastic File System

*Elastic Path CloudOps for AWS* uses an AWS Elastic File System (EFS) for shared storage between ActiveMQ nodes.

#### Disk Speed

>**WARNING:** Elastic Path recommends to determine the required disk performance for ActiveMQ and then add a spacer file to EFS that is large enough to ensure the proper disk throughput.

| Disk Speed (MiB/s) | Disk Space (gb) | Cost (USD$) |
| ------------------:| ---------------:| -----------:|
| 5 | 100 | 30 |
| 10 | 205 | 62 |
| 50 | 1024 | 308 |
| 100 | 2048 | 615 |

#### File System Locks

By default, *Elastic Path CloudOps for AWS* uses the standard posix file system locks used by ActiveMQ to ensure only one primary server exists. From NFSv4, these file system locks function on AWS EFS.

As an alternative, ActiveMQ provides a feature called *Pluggable Storage Lockers*. The *Lease Database Locker* implementation enables the substitution of posix file system locks for a database lock. With the *Lease Database Locker*, the connection to the database is only used as needed and is not held open. You can use this for high volume scenarios.

### Reference ActiveMQ Documentation

- [Shared file system failover documentation](http://activemq.apache.org/shared-file-system-master-slave.html)
- [ActiveMQ’s file system based persistence store](http://activemq.apache.org/kahadb.html)
- [Pluggable Storage Locker documentation (Lease Database Locker)](http://activemq.apache.org/pluggable-storage-lockers.html)

## Batch Server

The batch server has access to an externally persisted directory `/ep/app/`. The file system permissions for this directory allows application processes to create, modify and delete subdirectories and files. The purpose of this directory is to store custom files that must be persisted across application container crashes and resumes. This is done through a mounted volume via the AWS Elastic File System (EFS). For more information on the restrictions on the AWS Elastic File System, see: [Amazon EFS User Guide](https://docs.aws.amazon.com/efs/latest/ug/whatisefs.html).

## Integration Server

The integration server has a same directory `/ep/app/` for storing custom files as the batch server. For more information, see the [Batch Server section](#batch-server).
