---
id: auth-live-env
title: Author and Live Instance Deployment
sidebar_label: Author & Live Deployment
---

The complete Elastic Path Commerce stack includes an Author and a Live environment. The Live environment is configured for performance, reliability, and scalability. The Author environment is used to manage and preview commerce related items and publish changes to the Live environment using the *Data Sync Tool*.

In addition to the production use, you can use these environments for pre-production, performance, and robustness testing. For more information, see [Elastic Path CloudOps for AWS Best Practices](../best-practices.md).

The following architecture diagram highlights the load-balancing and autoscaling for Author and Live environment:

![Load-balancing and autoscaling focused view of an Author and Live environment.](assets/authlive-loadbalancing-architecture.svg)

The Author and Live deployment is able to use AWS (Amazon Web Services) SQS (Simple Queue Service) for message-based integration with external systems. The Author and Live deployment configures SQS queues for each of the Author and Live environments for handling purchases, shipments and inventory.

For more information about AWS SQS, see [What is Amazon SQS](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/welcome.html).

## Author Environment

The Elastic Path Author environment provides business users with the ability to manage and preview products, promotions, and pricing. You can publish changes to the Live environment using the Data Sync Tool.

Since the Author environment is an internally focused environment, the load and reliability requirements are low. All Elastic Path applications are deployed onto a single EC2 instance. Other services used by this single instance, such as the database and the ActiveMQ server, are deployed separately.

### Data Sync Tool

The Data Sync Tool (DST) is deployed as a part of the Elastic Path Author environment. Elastic Path Commerce versions 7.4.1 and earlier distributes DST as a command-line tool. Elastic Path Commerce 7.5 distributes DST as a command-line tool and as a Java application.

For more information about the *Data Sync Tool*, see [Elastic Path Production Tools](https://developers.elasticpath.com/developer-tools).

## Live Environment

The Elastic Path Live environment is composed of the following:

- Cortex API to deliver the shopping services
- Commerce Manager to deliver customer support and order management capabilities
- Integration server to deliver the application and support for back-end integrations
- _Optional_: Read-only RDS replica in a different availability zone

### Horizontal Database Scaling

You can deploy Horizontal Database Scaling (HDS) in the Live environment of an Authoring and Live deployment. To enable this feature, select the `DEPLOY_HORIZONTAL_DATABASE_SCALING` parameter before you run the `DeployAuthorAndLive` job. This will deploy the Live environment with an Amazon RDS (Relational Database Service) Aurora cluster of one writer and two read replicas. The Live environment will still have two read replicas if the `DeployAuthorAndLive` job is run with both job parameters `EP_RDS_READ_REPLICA` and `DEPLOY_HORIZONTAL_DATABASE_SCALING` selected.

<!-- For more information about HDS, see [Horizontal Database Scaling](https://documentation.elasticpath.com/commerce/docs/deployment/performance/horizontal-database-scaling.html). -->

## Elastic Path Commerce Application Nodes

The Elastic Path Commerce application nodes are composed of the Cortex, Commerce Manager, and the Integration servers, and their dependencies, which include a slave search instance and the necessary file assets.

The application nodes provide shopping services to front-end touch points and are scaled horizontally to meet transactional demand and to maintain reliability.

Most applications run on Docker containers on EC2 instances managed by ECS (Elastic Container Services), with the exception of ActiveMQ that runs directly on EC2 instances. The Docker containers are uniquely attributed to maximize the performance and functionality for a particular server.

For more information on how the nodes and services communicate together, see [Author and Live Network Architecture](infrastructure.md#network).

### Cortex Nodes

The Cortex nodes provide the Cortex API service along with Cortex Studio, a web application for easy interaction with the Cortex API. The node also contains a search slave. The Cortex nodes are the only Elastic Path application nodes that automatically scale beyond one node to adjust for load by default for *Elastic Path CloudOps for AWS (Amazon Web Services)*.

### Commerce Manager Node

The Commerce Manager node is for business users to administer Elastic Path Commerce and contains the Commerce Manager server and a search slave.

### Integration Node

Elastic Path Integration node provides a central point for back-end integrations and contains the integration server and a search slave.

### Other Nodes and Services

#### Admin Node

The Elastic Path Admin node is a singleton node that contains the search master server and the batch server.

#### ActiveMQ Cluster

The ActiveMQ cluster provides a redundant JMS (Java Message Service) to the Elastic Path applications through two ActiveMQ nodes, one active node and one fall-back node. The two nodes share storage through an AWS EFS (Elastic File System).

#### MySQL Database

A MySQL database is used for all Elastic Path Commerce data needs. Both the Author and the Live environment use AWS RDS (Relational Database Service) to provision the databases. Amazon RDS handles routine database tasks such as:

- Provisioning
- Patching
- Backup
- Recovery
- Failure detection
- Repair

By default, Amazon Aurora replicates the database into other availability zones for failover scenarios. Optionally, you can deploy a read-only replica for the live master database.

In a high throughput environment, you can improve transaction performance by enabling horizontal database scaling.

### Horizontal database scaling

Scaling the database vertically and adding additional Cortex instances is the simplest and most cost effective way to increase transaction throughput.

Horizontal database scaling allows an additional increase in transaction throughput by directing read requests to a cluster of read-only replica databases rather than to the master database. It should be considered only after the limits of vertical database scalability have been reached.

:::note
Please contact your Elastic Path representative if you are considering using horizontal database scaling. The Elastic Path Performance Lab would like to understand your requirements and provide advice on designing and testing your solution.
:::

To learn how to implement horizontal database scaling, see [Deployment Concepts](https://documentation.elasticpath.com/commerce/docs/deployment/index.html) Elastic Path Commerce documentation.
