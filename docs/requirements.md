---
id: requirements
title: Requirements
sidebar_label: Requirements
---

## Elastic Path Commerce Requirements

Before using Elastic Path CloudOps for AWS (Amazon Web Services), ensure that you have an Elastic Path Commerce source code distribution. This package must be in a repository created in the  source control management system. For more information, see the [Git Repository Hosting Service](#git-repository-hosting-service) section.

Note that the source code distribution and deployment package are not the same. The deployment package is created from the source code distribution.

## Elastic Path CloudOps for AWS Requirements

Before initializing Elastic Path CloudOps for AWS, you must ensure that the following requirements are met.

### Git Repository Hosting Service

Before using Elastic Path CloudOps for AWS, clone all repositories comprising Elastic Path CloudOps for AWS. Host the repositories in a Git repository hosting service, such as GitHub, Bitbucket, or CodeCommit.

Ensure that you have access to the CloudOps repositories hosted at [Elastic Path Source Code Repositories](https://code.elasticpath.com/). The repositories that comprise Elastic Path CloudOps for AWS are:

- `cloud-core-aws`
- `cloud-team-aws`
- `cloud-deploy-aws`
- `docker`

Identify the Git hosting service you will use. The hosting service must be accessible to the CloudOps systems that AWS creates, so it should not be behind a firewall.

Clone the CloudOps repositories and push them to your Git hosting service, so that they are now hosted and are accessible to CloudOps in AWS.

### Elastic Path Commerce Source Code

Ensure that your Elastic Path Commerce source code repository is hosted at a Git hosting service that is accessible to CloudOps systems that are created in AWS.

Identify and make note of the Elastic Path Commerce version that you will use with CloudOps.

### Provisioning and Selecting SSH Keys

Identify or create the Git SSH keys to use with the CloudOps build system, to allow CloudOps to check code out from Git. The Git keys must be long-lived and active for as long as you will use Elastic Path CloudOps. The Git SSH keys provided for initializing the CloudOps components, in the fields named like `<gitRepo>SSHKeyFileName`, will continue to be used by the Jenkins instance after initialization is completed. Selecting the SSH keys is a very important consideration for the longer term stability and functionality of CloudOps.

> **Note:** The CloudOps components do not support password protected SSH keys.

Elastic Path recommendations for Git SSH accounts and keys are as follows:

- Keys are okay for the long term use by the Jenkins jobs to access your Git repository
- Keys must belong to a utility account, and not to an account whose Git access might be revoked
- Keys have read-only access, they do not have write access

For information about read-only deploy keys in Github, see [Read-only deploy keys](https://github.blog/2015-06-16-read-only-deploy-keys/).

### New DNS Sub-Domain for CloudOps

Each AWS account or sub-account used by CloudOps for AWS requires a new and unique DNS sub-domain. This sub-domain is used in the host names of all the assets created by CloudOps, such as Jenkins servers and Elastic Path Commerce servers. The sub-domain must be new and cannot be a sub-domain that is already in use. For more information about how DNS works and is configured in CloudOps, see: [DNS Configuration](cloudCore/configurations.md#dns-configuration).

#### Identify the Parent Domain Name

As previously mentioned, CloudOps for AWS requires a new DNS sub-domain. The new sub-domain will be managed in AWS Route53, and will be a sub-domain of an existing parent domain. Identify the parent domain you will use. It does not need to be hosted or managed in AWS. For example, the parent domain you choose might be `example.com`.

#### Ensure Access For Mapping a Sub-Domain

You must be able to define a new sub-domain for the chosen parent domain name, and be able to map that new sub-domain to AWS name servers. Verify that the DNS hosting provider for your parent domain will allow you to create DNS `NS` records to map the sub-domain to AWS. Most DNS hosting providers will allow and suport this, but there could be exceptions. Ensure that you or your network team have the access required to add the required `NS` record to the parent domain.

#### Identify the Sub-Domain

Identify the new and unique sub-domain you will use. Ideally, it will be something meaningful that describes the use of the Elastic Path Commerce systems in the AWS account. For example, if you use the parent domain `example.com` then you might choose sub-domain `cloudops.example.com`. That sub-domain is the domain name to specify as `epCloudOpsDomain` when initializing CloudCore. It is also the domain name referred to as `$DOMAIN_NAME.com` in the following _Securing Internal Elastic Path Services with HTTPS_ section.

### Securing Internal Elastic Path Services with HTTPS

If you want to use CloudOps with SSL, an SSL certificate for your domain name, `$DOMAIN_NAME`, and the following sub-domains are also required:

- `*.$DOMAIN_NAME.com`
- `*.ep-build.$DOMAIN_NAME.com`
- `*.al.$DOMAIN_NAME.com`
- `*.ep-dev.$DOMAIN_NAME.com`

For example, if you want to use the domain name `cloudops.example.com`, you need an SSL certificate including the following names:

- `*.cloudops.example.com`
- `*.ep-build.cloudops.example.com`
- `*.al.cloudops.example.com`
- `*.ep-dev.cloudops.example.com`

If you are using multiple Author and Live environments, you will need SSL certificates for each of of your environments. Following the previous example, if you have an Author and Live environments for qa and staging with the following base domain names

- `al-qa`
- `al-staging`

You need an SSL certificate including the following names:

- `*.al-qa.cloudops.example.com`
- `*.al-staging.cloudops.example.com`

### AWS Requirements

For security and isolation purposes CloudOps for AWS requires an empty AWS account (or sub-account). We also recommend using separate AWS accounts for development and production. For more information on recommendations and best practices, see [Best Practices](best-practices.md).

Before getting started with CloudOps, ensure that the following AWS Account requirements are met:

- An empty AWS account, created with credit card details included, to use for CloudOps. Note that you must provide credit card details to use AWS services required by CloudOps
- Administrator access to the AWS Account. The Elastic Path CloudOps initialization process must be run as a user with administration access in the AWS account
- An [AWS region supported by ECR (Elastic Container Registry)](https://aws.amazon.com/about-aws/global-infrastructure/regional-product-services/)
- An AWS EC2 vCPU quota of at least 120. The limit default is 1152. For new accounts, the limit may be lower. To increase the limit, request an increase from AWS. For more information about requesting an increase, see [Amazon EC2 Service Limits](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-resource-limits.html)

Ensure that you are proficient in the following services that CloudOps uses:

- Amazon CloudFormation
- Amazon VPC (Virtual Private Cloud)
- Amazon Route53
- Amazon EC2
- Amazon ECS (Elastic Container Service)
- Amazon RDS (Relational Database Service)
- Amazon S3
- Amazon CloudWatch
- AWS Certificate Manager
- AWS Lambda

### Software and Other Requirements

The CloudOps initialization process requires that you clone the CloudOps repositories locally and then update and run specific shell scripts. The following are the requirements for the system where you run the steps to initialize CloudOps components:

- An SSH key authorized to the Git service hosting your copy of CloudOps repositories. For more information on creating SSH keys, see [this github documentation page](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)
- URL to download licensed JDK (Java Development Kit)
- Username and password to Elastic Path Nexus to download source code and create a mirror of the source code
- The AWS CLI (Command Line Interface) installed and configured correctly. For more information on configuring the AWS CLI, see [this AWS documentation page](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
- A Linux or Mac machine with the following programs installed:
    - Bash
    - Git
    - [Docker](https://docs.docker.com/install/) version 17 or higher
