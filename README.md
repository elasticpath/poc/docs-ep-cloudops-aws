## Build & Test

From your terminal/command prompt, you will need to be in the `website` directory to execute any commands. It's recommended to use Gradle but if you don't want to use Gradle, please make sure:

- Use `yarn` instead of `npm`
- From your terminal, go to the `website` directory of this repo: `cd website`
- Run `yarn install` to install necessary dependent packages
- Run `yarn clean` to remove build artifacts
- Run `yarn build` to build the site
    - Built artifats (static site html) will be available in `website/build` directory of this repository
- Run `yarn test` to execute all the tests setup for validating documentation guidelines and styles


### List of all relevant commands

- `startDevServer`: Starts the dev server available with *Docusaurus*. This can be used for checking documentation presentation/rendering before submitting for review
- `clean`: Deletes `build` directory and any other temporary directory created during build
- `build`: Builds a static html site
- `test`: Executes configured tests/linters
- `markdownlint`: Runs [markdownlint-cli][markdownlint-cli] to validate docs are written using standard markdown format
- `textlint`: Runs [textlint][textlint] to validate docs are confirming to a set of good writing standard such as puncuation, grammer, valid links, etc.
- `textlintWithAutofix`: Runs the same validation check as `textlint` but applies fixes where possible
- `textlintWriteGood`: Scans documentation for good writing practices
- `mdspell`: Checks for spelling mistakes in documents
- `lint`: Executes all of the linters available in this repository


[doc-site]: https://documentation.elasticpath.com/cloudops-aws/
[docusaurus-site]: https://docusaurus.io
[markdownlint-cli]: https://github.com/igorshubovych/markdownlint-cli
[textlint]: https://textlint.github.io
